## About Arcana Project

Dibuat dengan penuh cinta dan harapan, dikembangkan seperti anak sendiri.

## Technology

- Using Laravel 5.6 , PHP 7.1
- Add PHPImageWorkshop (check composer.json) . copy folder on vendor, then Run : composer dump-autoload

## Arcana Project Sponsors

We would like to extend our thanks to the following sponsors for helping fund on-going Arcana development. If you are interested in becoming a sponsor, please contact me (hendraprasetio1995@gmail.com) :

- ijejo.com

## Contributing

- Inspinia v2.5
- Jefry Bagus (as Logo Designer)
