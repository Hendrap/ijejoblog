<?php 

if(count(\Config::get('arcana.application.locales')) > 1){
		if(!isset($_SESSION['lang'])){
			$_SESSION['lang'] = \Config::get('arcana.application.default_locale');
		}
		app('config')->set('arcana.application.default_locale',$_SESSION['lang']);
}
