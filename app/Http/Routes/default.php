<?php 

	// Auto Generate Routes
	//entries
	if(!empty(app('ArcanaSetting')->entries)){
		foreach (app('ArcanaSetting')->entries as $key => $value) {
			
			//index
			Route::get('/'.$value['slug'],['as'=>'arcana_front_entry_'.$value['slug'],'uses'=>'FrontController@entries']);

			//detail
			Route::get('/'.$value['slug'].'/{slug}',['as'=>'arcana_front_entry_'.$value['slug'].'_detail','uses'=>'FrontController@entriesDetail']);

		}
	}

	//taxonomies
	if(!empty(app('ArcanaSetting')->taxonomies)){
		foreach (app('ArcanaSetting')->taxonomies as $key => $value) {

			//taxonomies
			Route::get('/categories/'.$value['slug'],['as'=>'arcana_front_taxonomy_'.$value['slug'],'uses'=>'FrontController@taxonomies']);

			//taxonomies - entries
			Route::get('/categories/'.$value['slug'].'/{slug}',['as'=>'arcana_front_taxonomy_'.$value['slug'].'_entries','uses'=>'FrontController@taxonomyEntries']);

		}
	}

	//pages
	$pages = json_decode(app('ArcanaSetting')->getSetting('arcana_pages_slug'));
	if(!empty($pages))
	{
		$blackList = Config::get('arcana.application.blacklist_pages');
		foreach ($pages as $key => $value) {
			if(!empty($value))
			{
				if(!in_array($value, $blackList)){
					Route::get('/'.$value,'FrontController@defaultPage');									
				}
			}
		}
	}

	//sample blacklist,custom route
	Route::get('/black-page',function(){
		echo "Custom Route!";
	});