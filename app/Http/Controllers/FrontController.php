<?php
namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use Arcana\Core\CoreFrontController as BaseController;

class FrontController extends BaseController
{
  	public $template = 'basic';
  	public $with;
    
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        $this->layout = view('app::'.$this->template.'.layout');
        $this->layout->title = setHomeTitle();
        $this->with = ['metas','user','taxonomies','medias'];
    }
    private function getEntryView($entry)
    {
        $defaultView = 'app::'.$this->template.'.pages.detail';
        $view = $defaultView;

        //entry type
        if(\View::exists($defaultView.'-'.$entry->entry_type)){
          $view = $defaultView.'-'.$entry->entry_type;
        }

        //slug
        if(\View::exists($defaultView.'-'.$entry->slug)){
          $view = $defaultView.'-'.$entry->slug;
        }

        //entry id
        if(\View::exists($defaultView.'-'.$entry->id)){
          $view = $defaultView.'-'.$entry->id;
        }
        return $view;

    }
    public function index()
    {
      $entries = \Entry::with($this->with)->whereEntryType('blog')->where('status','=','published')->where('published_at','<=',date('Y-m-d H:i:s'))->paginate(30);
    	$this->layout->content = view('app::'.$this->template.'.pages.index',[
            'postTitle' => 'Hi,Everyone!',
            'subPostTitle' => 'Lorem Ipsum Sub Dolor',
            'blog' => $entries,
            'entryType' => 'blog'
            ]);
    }

    public function entries()
    {
       $segments = \Request::segments();
       
       if(array_key_exists($segments[0], \Config::get('arcana.application.locales'))){
            unset($segments[0]);
       }    
       sort($segments);

       if(empty($segments[0])) abort(404);

       $entryType = $segments[0];
       
       $entries = \Entry::with($this->with)->whereEntryType($entryType)->where('status','=','published')->where('published_at','<=',date('Y-m-d H:i:s'))->paginate(30);
      
       $config = getEntryConfig($entryType);

       $this->layout->title = setPageTitle($config['plural']);

       $view = 'app::'.$this->template.'.pages.pages';

       //entry type
       if(\View::exists($view.'-'.$entryType)){
            $view = $view.'-'.$entryType;
       }
       
       $this->layout->content = view($view,[
        'entries' => $entries,
        'pageTitle' => $config['plural'],
        'entryType' => $entryType
        ]);

    }
    public function defaultPage()
    {

        $segments = \Request::segments();
         
        if(array_key_exists($segments[0], \Config::get('arcana.application.locales'))){
             unset($segments[0]);
        }    
        sort($segments);

        if(empty($segments[0])) abort(404);

        $slug = $segments[0];

        $entry = \Entry::with($this->with)->whereEntryType('page')->whereSlug($slug)->whereStatus('published')->where('published_at','<=',date('Y-m-d H:i:s'))->first();

        if(empty($entry)) abort(404);
        
        $view = $this->getEntryView($entry);

        $this->layout->title = setPageTitle(parseMultiLang($entry->title));
        $this->layout->content = view($view,[
          'entry' => $entry
          ]);

    }

    public function entriesDetail($slug)
    {
        $entry = \Entry::with($this->with)->whereSlug($slug)->whereStatus('published')->where('published_at','<=',date('Y-m-d H:i:s'))->first();
        if(empty($entry)) abort(404);

        $view = $this->getEntryView($entry);

        $this->layout->title = setPageTitle(parseMultiLang($entry->title));
        $this->layout->content = view($view,[
          'entry' => $entry
          ]);
    }

    public function taxonomies()
    {
      $segments = \Request::segments();
      $taxonomyType = $segments[count($segments) - 1];
      $info = getTaxonomyConfig($taxonomyType);
      if(empty($info)) abort(404);

      $taxonomies = \Taxonomy::whereTaxonomyType($taxonomyType)->paginate(30);
      $view = 'app::'.$this->template.'.pages.taxonomies';

      if(\View::exists($view.'-'.$taxonomyType)){
        $view = $view.'-'.$taxonomyType;
      }

      $this->layout->title = setPageTitle($info['plural']);
      $this->layout->content = view($view,[
        'taxonomies' => $taxonomies,
        'pageTitle' => $info['plural']
        ]);
    }

    public function taxonomyEntries($taxonomySlug)
    {
        $taxonomy = \Taxonomy::whereTaxonomySlug($taxonomySlug)->first();

        if(empty($taxonomy)) abort(404);

        $entries = $taxonomy->entries()->with($this->with)->whereStatus('published')->where('published_at','<=',date('Y-m-d H:i:s'))->paginate(30);
        

        $defaultView = 'app::'.$this->template.'.pages.taxonomy';
        $view = $defaultView;

        //taxo type
        if(\View::exists($defaultView.'-'.$taxonomy->taxonomy_type)){
          $view = $defaultView.'-'.$taxonomy->entry_type;
        }

        //slug
        if(\View::exists($defaultView.'-'.$taxonomy->taxonomy_slug)){
          $view = $defaultView.'-'.$taxonomy->slug;
        }

        //taxo id
        if(\View::exists($defaultView.'-'.$taxonomy->id)){
          $view = $defaultView.'-'.$taxonomy->id;
        }

        $this->layout->title = setPageTitle($taxonomy->name);
        $this->layout->content = view($view,[
          'entries' => $entries,
          'pageTitle' => $taxonomy->name
          ]);

    }
}
