<section class="features" id="blog">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <div class="navy-line"></div>
                <h1>{{app('ArcanaSetting')->getSetting('site_tagline')}}</h1>
                <p>{{app('ArcanaSetting')->getSetting('site_desc')}}</p>
            </div>
        </div>
        <div class="row features-block">
        	@foreach($blog as $entry)
            <div class="col-lg-3 features-text wow fadeInLeft">
                <small>{{date_format(date_create($entry->created_at),app('ArcanaSetting')->getSetting('date_format'))}}</small>
                <h2>{{ parseMultiLang($entry->title) }}</h2>
                <p>{{ str_limit(strip_tags(parseMultiLang($entry->content),50)) }}</p>
                <a href="{{ route('arcana_front_entry_'.$entryType.'_detail',$entry->slug) }}" class="btn btn-primary">Learn more</a>
            </div>
            @endforeach
        </div>
    </div>

</section>

<section id="contact" class="gray-section contact">
    <div class="container">
        <div class="row m-b-lg">
            <div class="col-lg-12 text-center">
                <div class="navy-line"></div>
                <h1>Contact Us</h1>
                <p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod.</p>
            </div>
        </div>
        <div class="row m-b-lg">
            <div class="col-lg-3 col-lg-offset-3">
                <address>
                    <strong><span class="navy">Arcana Studio</span></strong><br/>
                    795 Folsom Ave, Suite 600<br/>
                    San Francisco, CA 94107<br/>
                    <abbr title="Phone">P:</abbr> (123) 456-7890
                </address>
            </div>
            <div class="col-lg-4">
                <p class="text-color">
                    Consectetur adipisicing elit. Aut eaque, totam corporis laboriosam veritatis quis ad perspiciatis, totam corporis laboriosam veritatis, consectetur adipisicing elit quos non quis ad perspiciatis, totam corporis ea,
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 text-center">
                <a href="mailto:test@email.com" class="btn btn-primary">Send us mail</a>
                <p class="m-t-sm">
                    Or follow us on social platform
                </p>
                <ul class="list-inline social-icon">
                    <li><a href="#"><i class="fa fa-twitter"></i></a>
                    </li>
                    <li><a href="#"><i class="fa fa-facebook"></i></a>
                    </li>
                    <li><a href="#"><i class="fa fa-linkedin"></i></a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 text-center m-t-lg m-b-lg">
                <p><strong>&copy; 2018 Arcana Project</strong><br/> {{app('ArcanaSetting')->getSetting('site_desc')}}</p>
            </div>
        </div>
    </div>
</section>