<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Http\Middleware\MultiLanguage;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // 
        $req = app('Illuminate\Http\Request');
        $this->app->register('App\Providers\AuthServiceProvider');
        $this->app->register('App\Providers\EventServiceProvider');
        $this->loadViewsFrom(base_path('/app/Http/Views'), 'app');

        $this->app->register('App\Providers\RouteServiceProvider');

        // register class multilang
        $this->app['router']->middlewareGroup('multilang',[
            \App\Http\Middleware\MultiLanguage::class,
        ]);
        // $this->app['router']->middleware('multilang', \App\Http\Middleware\MultiLanguage::class);    
    }
}
