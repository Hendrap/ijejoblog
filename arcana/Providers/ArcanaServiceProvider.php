<?php

/*
 * Arcana Services Provider
 *
 * @author HendraP <hendraprasetio1995@gmail.com>
 * Ini adalah file kunci, dimana semua helper, config, views, dsb yang ada di folder arcana di load
 * Ini untuk loader semua keperluan untuk folder arcana
 * Diregister lewat config/app.php -> providers
 */


namespace Arcana\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Filesystem\Filesystem;
class ArcanaServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        app('Arcana\Core\ArcanaSetup')->run();
        // $this->loadGAConfig();
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        
        $this->loadConfigs();
        $this->setAliases();
        $this->registerMiddlewares();
        $this->registerServiceProviders();
        $this->registerViews();
        $this->loadHelpers();
        // $this->loadArcanaConfig();
        
    }

     /**
     * Load semua service yg berhubungan sama arcana
     * 
     * @return void
     */
    public function loadArcanaConfig()
    {

        //ini profiler/debug bar & console

        if(\Config::get('arcana.application.profiler')){
            $this->app->register(\Barryvdh\Debugbar\ServiceProvider::class);
        }
        $this->app->register(\Darsain\Console\ConsoleServiceProvider::class);

    }

    /**
     * Load semua helpers
     * 
     * @return void
     */
    public function loadHelpers()
    {
        $helpers = $this->loadFilesFromDir(base_path().'/arcana/Helpers');
        foreach ($helpers as $key => $value) {
            include $value;
        }
    }

    /**
     * Register semua middleware
     * 
     * @return void
     */
    public function registerMiddlewares()
    {
        $middlewares = $this->loadFilesFromDir(base_path().'/arcana/Middleware');
        foreach ($middlewares as $key => $value) {
            $className = str_replace('.php', '', $value->getRelativePathname());
            $this->app['router']->middleware($className,'Arcana\Middleware\\'.$className);            
        }

        $this->app['router']->middlewareGroup('ArcanaAdminGroup',[
            \Arcana\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            \Arcana\Middleware\VerifyCsrfToken::class,
        ]);
    }

     /**
     * Register semua views
     * 
     * @return void
     */

    public function registerViews()
    {
        // menambahkan load package arcana agar bisa di load (arcana::*)
         $this->loadViewsFrom(base_path() . '/arcana/Views', 'arcana');
         
    }

    /**
     * Register semua providers yg ada di file config arcana
     * 
     * @return array
     */
    public function registerServiceProviders()
    {
        $providers = \Config::get('arcana.providers');
        foreach ($providers as $key => $value) {
            $this->app->register($value);
        }
        
    }

    /**
     * Ini cuma ambil informasi files php yg ada dalam suatu folder/path
     * 
     * @return array
     */
    public function loadFilesFromDir($path = '')
    {
        $files = [];
        if(!empty($path)) $files = \File::allFiles($path); 
        return $files;
    }

    /**
     * Masukin semua config yg ada di folder Config
     * bisa create sendiri untuk developer
     * @return void
     */
    public function loadConfigs()
    {
        $configFiles = $this->loadFilesFromDir(base_path().'/arcana/Config');
        foreach ($configFiles as $key => $value) {
            $this->mergeConfigFrom((string)$value, 'arcana.'.str_replace('.php', '', $value->getRelativePathname()));
        }
    }

    /**
     * Set alias untuk core,controllers,dll arcana punya
     * nantinya biar gak perlu nambahin namespace
     * @return void
     */
    public function setAliases()
    {

        $loader = \Illuminate\Foundation\AliasLoader::getInstance();
        $aliases = \Config::get('arcana.aliases');
        $models = $this->loadFilesFromDir(base_path('arcana/Models'));
        foreach ($models as $key => $value) {
            $className = str_replace('.php', '', $value->getrelativePathname());
            $aliases[$className] = 'Arcana\Models\\'.$className;
        }
        foreach ($aliases as $key => $value) {
          $loader->alias($key, $value);
        }

    }

    public function loadGAConfig(){
        if(file_exists(storage_path('app/ga_account.p12')) && !empty(app('ArcanaSetting')->getSetting('ga_view_id')) && !empty(app('ArcanaSetting')->getSetting('ga_client_email')) && !empty(app('ArcanaSetting')->getSetting('ga_client_id'))){
            \Config::set('laravel-analytics.siteId','ga:'.app('ArcanaSetting')->getSetting('ga_view_id'));
            \Config::set('laravel-analytics.serviceEmail',app('ArcanaSetting')->getSetting('ga_client_email'));
            \Config::set('laravel-analytics.clientId',app('ArcanaSetting')->getSetting('ga_client_id'));
            \Config::set('laravel-analytics.certificatePath',storage_path('app/ga_account.p12'));
            $this->app->register('Spatie\LaravelAnalytics\LaravelAnalyticsServiceProvider');
        }
    }

}
