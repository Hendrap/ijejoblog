<?php

namespace Arcana\Providers;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class ArcanaRoutesServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'Arcana\Core';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function map()
    {
        $router->group(['namespace' => $this->namespace], function () {
            $this->loadRoutes();
        });
    }

    /**
     * Register all routes
     * Nanti per file bisa define sendiri
     * @return void
     */
    public function loadRoutes()
    {
        $routeFiles = \File::allFiles(base_path().'\arcana\Routes');
        foreach ($routeFiles as $key => $value) {
            include $value;
        }
    }

}
