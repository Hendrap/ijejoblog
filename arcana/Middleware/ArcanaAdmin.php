<?php 
namespace Arcana\Middleware;
use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;
use Closure;

class ArcanaAdmin
{

    public function handle( $request, Closure $next ) {
     	return $next($request);
    }

}