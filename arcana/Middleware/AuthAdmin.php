<?php 
namespace Arcana\Middleware;
use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;
use Closure;

class AuthAdmin
{

    public function handle( $request, Closure $next ) {
    	if(\Auth::guest()){
            $redirect = urlencode(\Request::url());
    		return redirect()->to(route('arcana_get_login')."?redirect=".$redirect);
    	}
    	if(\Auth::user()){
    		if(!app('Arcana\Core\ArcanaAcl')->multiAccess([32])){
    			abort(404, 'Unauthorized action.');
    		}
    	}
    	return $next($request);
    }

}