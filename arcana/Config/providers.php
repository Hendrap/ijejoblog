<?php 
return [
	'Arcana\Providers\ArcanaRoutesServiceProvider',
	// https://github.com/spekkionu/laravel-zend-acl ver 4. 
	// usage : pengecekan autentikasi lebih mudah
	'Spekkionu\ZendAcl\ZendAclServiceProvider',
	// https://laravelcollective.com/docs/5.4/html
	'Collective\Html\HtmlServiceProvider',
];