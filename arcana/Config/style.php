<?php 
return [
	'grids' => [
		'Mini' => 'col-lg-1',
		'Small' => 'col-lg-2',
		'Medium' => 'col-lg-4',
		'Large' => 'col-lg-7',
		'Full' => 'col-lg-9'
	]	
];
