<?php 
return [
	'ArcanaController' => 'Arcana\Core\ArcanaController',
	'Acl' => 'Spekkionu\ZendAcl\Facades\Acl',
    'Form' => 'Collective\Html\FormFacade',
    'Html' => 'Collective\Html\HtmlFacade',
    'LaravelAnalytics' => 'Spatie\LaravelAnalytics\LaravelAnalyticsFacade'
];