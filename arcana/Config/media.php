<?php 
return [
	
	'image' => ['jpg','png','jpeg','gif'],

	'audio' => ['mp3','midi','mpeg','mpg','mpeg3','mp3','x-wav'],

	'video' => ['mp4','avi','3gp','mkv'],

];