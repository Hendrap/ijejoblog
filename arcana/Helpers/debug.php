<?php 
function dpr() {
		$args = func_get_args();
		echo "<pre>";
		foreach ($args as $k => $v) {
			echo "dpr".($k + 1).":\n";
			print_r($v);
			echo "\n";
		}
		echo "</pre>";
	}
