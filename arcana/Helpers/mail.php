<?php


 function parseEmail($id, $array) {

 

     $type = 'email_template';
     $email = Entry::where('slug','=',$id)->where('entry_type', '=', $type)->first();
     if(empty($email)) return false;

     $msg =  parseMultiLang($email->content);

     foreach ($array as $key => $value) {

         $msg = str_replace($key, $value,$msg);

     }

     $subject = parseMultiLang($email->title);

     foreach ($array as $key => $value) {

         $subject = str_replace($key, $value,$subject);

     }

     $res = (object)array('subject' => $subject,'msg'=>$msg);

     return $res;

 

 }
