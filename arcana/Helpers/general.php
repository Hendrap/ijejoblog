<?php 
function getConfigBySlug($attr = '',$slug = ''){
	foreach (app('ArcanaSetting')->{$attr} as $key => $value) {
		if($value['slug'] == $slug) return $value;
	}
	return [];
}
function setPageTitle($title = 'Page'){
	return $title.' | '.app('ArcanaSetting')->getSetting('site_name');
}
function setHomeTitle(){
	return app('ArcanaSetting')->getSetting('site_name').' | '.app('ArcanaSetting')->getSetting('site_slogan');
}
function formatBytes($size, $precision = 2)
{
    $base = log($size, 1024);
    $suffixes = array('Bytes', 'K', 'M', 'G', 'T');   

    return round(pow(1024, $base - floor($base)), $precision) .' '. $suffixes[floor($base)];
}
function newLine($string = '')
{
	$string = trim(preg_replace('/\s+/', ' ', $string));

	return $string;
}
function requiredMessage()
{
	$messages = [
    	'required' => 'The :attribute field is required.',
	];

	return $messages;

}

function generateName($user)
{
	$tmp = array();
	$name = "";
	
	if(!empty($user->metas))
	{
		$tmpFirst = getEntryMetaFromArray($user->metas,'first_name');
		if(!empty($tmpFirst)){
			$tmp[] = $tmpFirst;
		}

		$tmpLast = getEntryMetaFromArray($user->metas,'last_name');
		if(!empty($tmpLast)){
			$tmp[] = $tmpLast;
		}
		$name = implode(" ", $tmp);
	}

	if(!empty($user->email))
	{
		if(empty($name)) $name = $user->email;		
	}

	if(empty($name)) $name = 'Arcanas';
	return $name;
}