<?php
namespace Arcana\Models;
/**
* 
*/
class Logger extends ArcanaModels
{
	
	function __construct()
	{
		# code...
	}
	
	public function user()
	{
		return $this->belongsTo('User');
	}

	public static function info($desc = '')
	{
		if(empty($desc)) return false;
		$user = \Auth::user();
		$user_id = 0;
		if(!empty($user)) $user_id = $user->id;

		$log = new Logger();
		$log->user_id = $user_id;
		$log->description = $desc;
		$log->server = json_encode($_SERVER);
		$log->save();
	}
}