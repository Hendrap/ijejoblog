<?php 
namespace Arcana\Models;
/**
* 
*/
class Entrymeta extends ArcanaModels
{
	
	protected $fillabel = ['entry_id'];
	
	function __construct()
	{
		# code...
	}
	
	public function entry(){
		return $this->belongsTo('Entry');
	}

	public function media()
	{
		return $this->belongsTo('Media');
	}
}