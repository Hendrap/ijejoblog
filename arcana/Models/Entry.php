<?php 
namespace Arcana\Models;
use Illuminate\Support\Str;
/**
* 
*/
class Entry extends ArcanaModels
{
	protected $fillable = ['title','content','excerpt'];

	public function media()
	{
		return $this->belongsTo('Media','media_id');
	}

	public static function getLastUpdate($type = ''){
        return static::with('modifiedBy')->whereEntryType($type)->orderBy('updated_at','desc')->first();
    }
    public function brand(){
    	return $this->belongsTo('Entry','brand_id');
    }

	public function saveSEO($input = array()){
		if(empty($input)) return false;
		foreach ($input as $key => $value) {
			 $meta = \Entrymeta::whereMetaKey('seo_'.$key)->whereEntryId($this->id)->first();
			 if(empty($meta)){
                $meta = new \Entrymeta();
                $meta->meta_key = 'seo_'.$key;
                $meta->entry_id = $this->id;
             }
             $meta->meta_value_text = $value;
             $meta->save();

		}
	}
	public function saveEntryMetaFromInput($metas = array(),$input = array()){
		$metas[] = (object)[
			'meta_key' => 'arcana_keyword_tags',
			'meta_data_type' => 'text',
			'meta_name' => 'Tags'
		];
		
		foreach ($metas as $key => $value) {
			if(empty($value->meta_type) || $value->meta_type == 'default'){
	            $meta = \Entrymeta::whereMetaKey($value->meta_key)->whereEntryId($this->id)->first();
	            if(empty($meta)){
	                $meta = new \Entrymeta();
	                $meta->meta_key = $value->meta_key;
	                // $meta->meta_name = $value->meta_name;
	                $meta->entry_id = $this->id;
	            }
	            $val = '';
	            switch ($value->meta_data_type) {
	                case 'text':
	                    $val = @$input['metas'][$value->meta_key];
	                    break;
	                case 'int':
	                    $val = (int)@$input['metas'][$value->meta_key];
	                    break;
	                case 'date':
	                    $val = date('Y-m-d H:i:s',strtotime(@$input['metas'][$value->meta_key]));
	                    break;
	            }
	            if(!empty($val)){
	                $meta->{'meta_value_'.$value->meta_data_type} = $val;                
	            }else{
	            	 $meta->{'meta_value_'.$value->meta_data_type} = "";        
	            }
	            $meta->save();
	         }
        }
	}
	public function modifiedBy(){
		return $this->belongsTo('User','modified_by');
	}
	
	public function user(){
		return $this->belongsTo('User','author');
	}

	public function parent(){
		return $this->belongsTo('Entry','entry_parent');
	}

	public function childs(){
		return $this->hasMany('Entry','entry_parent');
	}

	public function metas(){
		return $this->hasMany('Entrymeta');
	}

	public function taxonomies(){
		return $this->BelongsToMany('Taxonomy');
	}

	public function medias(){
		return $this->BelongsToMany('Media')->orderBy('entry_media.id','desc');
	}

	public function items(){
        return $this->hasMany('Morra\Catalog\Models\Item','product_id');
    }

    public function activeItems(){
        return $this->hasMany('Morra\Catalog\Models\Item','product_id')->where('catalog_items.status','active');
    }

	public function getMediaMeta($key = "")
	{
			$attr = strtolower(str_replace("arcanaMedia", "", $key));
			if(!empty($this->relations['metas'])){
				foreach ($this->relations['metas'] as $k => $value) {
					if($attr == $value->meta_key && $value->media_id > 0)
					{
						return $value->media;
					}
				}
			}
			return [];
	}



	public function __get($key)
	{
		if(Str::startsWith($key,"arcanaMedia")){
			return $this->getMediaMeta($key);
		}
		
		return parent::__get($key); 
		

	}


}