<?php 
namespace Arcana\Models;
/**
* 
*/
class Role extends ArcanaModels
{
	protected $fillable = ['name'];
	function __construct()
	{
		# code...
	}

	public function rules(){
		return $this->belongsToMany('Rule');
	}
	public function user()
	{
		return $this->belongsTo('User','modified_by');

	}
	public function users(){
		return $this->belongsToMany('User');
	}
}