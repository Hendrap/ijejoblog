<?php 
namespace Arcana\Models;
/**
* 
*/
class Media extends ArcanaModels
{
	protected $table = 'medias';

	function __construct()
	{
		# code...
	}

	public function entries()
	{
		return $this->belongsToMany('Entry');
	}
	public function items()
	{
		return $this->belongsToMany('Morra\Catalog\Models\Item');
	}
	public function user()
	{
		return $this->belongsTo('User','author');
	}
}