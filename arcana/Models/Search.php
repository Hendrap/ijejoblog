<?php
namespace Arcana\Models;
/**
* 
*/
class Search extends ArcanaModels
{
	
	function __construct()
	{
		
	}
	public static function rebuild($entry = array()){
		if($entry->entry_type == 'page'){
			$data = [];	
			$entries = Entry::select(['slug'])->whereEntryType('page')->get();
			foreach ($entries as $key => $value) {
				$data[] = $value->slug;
			}

			$data = json_encode($data);
			$setting = Setting::whereSettingKey('arcana_pages_slug')->first();
			if(empty($setting)) $setting = new Setting();

			$setting->setting_key = 'arcana_pages_slug';
			$setting->setting_value = $data;
			$setting->bundle = 'arcana';
			$setting->autoload = 'yes';
			$setting->save();
		}	
	}
}