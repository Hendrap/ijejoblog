<?php

namespace Arcana\Controllers;

use Arcana\Core\ArcanaController;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class General extends ArcanaController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
	
	public $template = 'arcana::';
    public $with;

    function __construct()
    {
        parent::__construct();
        $this->layout->activeParent = 'general';
        $this->layout->active = '';
    }

    public function index()
    {
    	$this->layout->content = view($this->template.'admin.page.dashboard',[
    		'title' => ''
    	]);

    }

    public function siteDescription()
    {
        $this->layout->active = 'site_description';
        $this->layout->title = setPageTitle("Site Description");    
        $this->layout->content = view($this->template.'admin.setting.site_description',[
        ]);

    }

    

}
