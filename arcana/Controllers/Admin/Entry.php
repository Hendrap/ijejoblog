<?php

namespace Arcana\Controllers\Admin;

use Illuminate\Http\Request;
use Arcana\Requests;
use Arcana\Core\ArcanaController;
use \Taxonomy as Taxonomies;
use \Entry as Entries;
use \Entrymeta as Entrymetas;
use \Logger;

class Entry extends ArcanaController
{
    public function sequence($type)
    {
        if(!app('Arcana\Core\ArcanaAcl')->multiAccess([2])){
            abort(403, 'Unauthorized action.');
        }

        $info = getEntryConfig($type);
 
        $entries = Entries::whereEntryType($type)->where('status','published')->orderBy('sequence','asc')->get();
        $this->layout->title = setPageTitle("Sequence");
        $this->layout->content = view('arcana::admin.entry.sequence',[
                'entries' => $entries,
                'info' => $info,
                'type' => $type
            ]);
    }

    public function saveSequence($type,Request $req)
    {
        if(!app('Arcana\Core\ArcanaAcl')->multiAccess([2])){
            abort(403, 'Unauthorized action.');
        }

        $items = $req->input('data');
        $max = count($items);
        $pageLength = $req->pageLength;
        $currentPage = $req->currentPage;
        
        foreach ($items as $key => $value) {
            if($value['sequence'] == 1 && $value['before'] != 0 && $currentPage > 1)
            {
                //kalau dia pindah page ke sebelum'e'
                //dpr($value);
                $entry = Entries::whereId($value['entry_id'])->update(['sequence'=>($currentPage * $pageLength)]);
            }elseif ($value['sequence'] == $max && $value['after'] != 0) {
                //dpr($value);
               //kalau dia pindah page ke berikut'e
                $entry = Entries::whereId($value['entry_id'])->update(['sequence'=>(($currentPage + 1) * $pageLength) + 1]);
            }else{
                //lain2
                $entry = Entries::whereId($value['entry_id'])->update(['sequence'=>$value['sequence']]);
            }

        }

        return json_encode(array('status'=>1));
    }

    public function status($entry_id = 0,$status = ''){
        $entry = Entries::find($entry_id);
        if(empty($entry)) abort(404);

        $entry->status = $status;
        $entry->save();

        Logger::info("Change Entry's status to ".$status);
        return redirect()->back()->with('msg','Status Changed!');
    }

    public function changeSlug(Request $req)
    {
        $id = $req->entry_id;

        $defaultTitle = "page";

        $entry = Entries::find($id);
        if(empty($entry)) return json_encode(array('status'=>0));

        $slug = \Request::input('slug',str_slug($defaultTitle));
        if(empty($slug)) $slug = str_slug($defaultTitle);

        $originalSlug = $slug;
        $check = Entries::where('id','!=',$id)->where('slug','like',$slug.'%')->count();
        if($check > 0){
            $slug .= '-'.$check;
        }

        if($entry->slug != $slug && $entry->type == 'page'){
            $routeList = getRouteList();
            $locale = \Config::get('arcana.application.default_locale').'/';
            $testRoute = $slug;
            $isMultiLang = false;
            if(count(\Config::get('arcana.application.locales')) > 1)
            {
                $isMultiLang = true;
                $testRoute = $locale.$slug;
            }

            while (in_array($testRoute, $routeList)) {
                $check++;
                $slug = $originalSlug.'-'.$check;
                $testRoute = $slug;
                if($isMultiLang) $testRoute = $locale.$slug;
            }
        }

        $entry->slug = $slug;
        $entry->save();
        
        return json_encode(array('status'=>1,'slug'=>$slug));
    }

    public function castingTaxonomies($infoTaxo,$taxonomies)
    {
        $data = [];
        foreach ($infoTaxo as $info) {
            foreach ($taxonomies as $key => $value) {
               if($value->taxonomy_type == $info['slug']){
                   if(empty($data[$info['slug']])) $data[$info['slug']] = [];
                   $data[$info['slug']][] = $value;                 
               }
            }
        }

        $triTaxo = [];
        $fiveTaxo = [];
        foreach ($data as $key => $value) {
            // if(count($value) < 5)
            // {
            //     $triTaxo[$key] = $value;
            // }else{
                $fiveTaxo[$key] = $value;
//            }
        }

        return ['triTaxo'=>$triTaxo,'fiveTaxo'=>$fiveTaxo];
    }

    public function getData($type = 'type',Request $req)
    {
        //container data
        $data = [];
        //pagination
        $start = $req->input('start',0);
        $take = $req->input('length',10);
        //search field
        $search = $req->input('search');
        //order
        $order = $req->input('order');
        //categgory
        $isSearch = false;
        $selectedCategory = '';
        $category = $req->input('columns');
        if(!empty($category[7]['search']['value']))
        {
            $selectedCategory = $category[7]['search']['value'];
        }
      

       
        //total records
        if(!empty($selectedCategory))
        {
            $taxonomy = Taxonomies::find($selectedCategory);
            if(empty($taxonomy)) return json_encode(['data'=>[],'draw'=>$req->input('draw',1),'recordsTotal'=>0,'recordsFiltered'=>0]);
            $base = $taxonomy->entries()->select('entries.*');
        }else{
            $base = new Entries();            
        }

        $recordsTotal = $base->where('status','!=','deleted')->whereEntryType($type);
        if(empty($search['value'])){
            $recordsTotal = $recordsTotal->whereEntryParent(0);
        }
        $recordsTotal = $recordsTotal->count();

        //filter entry
        $entries = $base->whereEntryType($type)->where('entries.status','!=','deleted');

        $reOrderCol = @$order[0]['column'];
        $isReorder = false;
      
        if($reOrderCol == 0) $isReorder = true;
       
        if($reOrderCol == null) $isReorder = false;

        if(!empty($search['value']) && $isReorder == false)
        {
            $isSearch = true;
            $entries = $entries->where(function($q)use($search){
                $q->where('entries.title','like','%'.$search['value'].'%');
                $q->orWhere('entries.content','like','%'.$search['value'].'%');
                $q->orWhere('entries.excerpt','like','%'.$search['value'].'%');
            });
        }else{
            $entries = $entries->whereEntryParent(0);
        }       
        //ambil data

        //entry status
        $isStatus = false;
        $status = $req->input('columns');
        if(!empty($status[5]['search']['value']))
        {
            $status = $status[5]['search']['value'];
            if($status == 'scheduled_publish')
            {
                $entries = $entries->whereStatus('published');
                $entries = $entries->where('published_at','>',date('Y-m-d H:i:s'));
            }else{
                $entries = $entries->whereStatus($status);
            }
            $isStatus = true;
        }

         //total entry yg kefilter
        $recordsFiltered = $entries->count();
        

        $with = ['medias','user',
                'childs'=>function($q)use($search,$status,$isStatus){
                    $q->where('entries.status','!=','deleted');
                    if(!empty($search['value']))
                    {
                        $q->where('entries.title','like','%'.$search['value'].'%');
                        $q->orWhere('entries.content','like','%'.$search['value'].'%');
                        $q->orWhere('entries.excerpt','like','%'.$search['value'].'%');
                    }
                    if($isStatus)
                    {
                        if($status == 'scheduled_publish')
                        {
                            $q->whereStatus('published');
                            $q->where('published_at','>',date('Y-m-d H:i:s'));
                        }else{
                             $q->whereStatus($status);
                        }
                    }


                },
                'childs.user','childs.childs','childs.childs.user'];
       
        if(!empty($search['value'])){
            $with[] = 'parent';
        }

        $entries = $entries->take($take)->skip($start)->with($with);
        //sorting
        $cols = ['sequence','title','author','published_at','created_at','updated_at','status'];
        
        if(!empty($order[0]))
        {
            $col = @$cols[$order[0]['column']];
            if(!empty($col))
            {
                $entries = $entries->orderBy($col,$order[0]['dir']);
            }else{
                $entries = $entries->orderBy('entries.created_at','desc');
            }
        }else{
            $entries = $entries->orderBy('entries.created_at','desc');
        }
        

        $entries = $entries->get();


        if(@$order[0]['column'] == 0){
            $page =  ($start/$take) + 1;
            if($page > 1){
                $before = Entries::whereEntryParent(0)->whereEntryType($type)->skip($start - $take)->where('status','!=','deleted')->orderBy('sequence','desc')->first();
                
                if(!empty($before))
                {
                    $before->before = true;
                    $entries->prepend($before);
                }
            }

            $after = Entries::whereEntryParent(0)->whereEntryType($type)->skip($start + $take)->where('status','!=','deleted')->orderBy('sequence','asc')->first();
            if(!empty($after))
            {
                $after->after = true;
                $entries->push($after);           
            }
        }

        $cols = ['sequence','title','user','published_at','created_at','updated_at','parent','action'];
        $view = 'arcana::admin.'.$type.'.grid.entry';
        $base = [];
        foreach ($cols as $col) {
            $base[$col] = '';
        }

        if(!\View::exists($view)){
            $view = 'arcana::admin.entry.grid.entry';
        }

        foreach ($entries as $key => $value) {
            $item = $base;
            $item['entry_parent'] = $value->entry_parent;
            $html = '';
            $html .= view($view,['entry'=>$value,'type'=>$type,'isSearch'=>$isSearch]);
            $item['html'] = $html;

            $data[] = $item;
            
            if(empty($search['value']) && $isReorder == false){
                foreach ($value->childs as $v) {
                  $item = $base;
                  $item['entry_parent'] = $v->entry_parent;

                  $html = '';
                  $html .= view($view,['entry'=>$v,'type'=>$type,'isSearch'=>$isSearch]);
                  $item['html'] = $html;
                  $data[] = $item;
                    
                  foreach($v->childs as $c){
                    $item = $base;
                    $item['entry_parent'] = $c->entry_parent;
                    $html = '';
                    $html .= view($view,['entry'=>$c,'type'=>$type,'isSearch'=>$isSearch,'lastChild'=>true]);
                    $item['html'] = $html;
                    $data[] = $item;
                  }


                }
            }
        }


        Logger::info("View Entry : ".$type);

        return json_encode(['data'=>$data,'draw'=>$req->input('draw',1),'recordsTotal'=>$recordsTotal,'recordsFiltered'=>$recordsFiltered]);
    }

    public function index($type = 'type')
    {
        if(!app('Arcana\Core\ArcanaAcl')->multiAccess([2,6])){
            abort(404, 'Unauthorized action.');
        }

        $req = app('request');

        $info = getEntryConfig($type);
        $last = Entries::getLastUpdate($type);
        $theLastName = 'arcana';
        $theLastDate = date('d F Y H:i:s');
        if(!empty($last->modifiedBy)){
            $theLastName = $last->modifiedBy->username;
            $theLastDate = date('d F Y H:i:s',strtotime($last->updated_at));
        }

        $view = 'arcana::admin.'.$type.'.index';
        if(!\View::exists($view)){
            $view = 'arcana::admin.entry.index';
        }

        $taxoInfo = getTaxonomyFromEntry($type);
        $taxonomies = getTaxonomiesForEntry($taxoInfo->taxoTypes);
        

        $this->layout->activeParent = $type;
        $this->layout->active = $type;
        $this->layout->title = setPageTitle("All ".$info['plural']);
        $this->layout->content = view($view,[
                'type' => $type,
                'info'  => $info,
                'theLastName'=> $theLastName,
                'theLastDate'=> $theLastDate,
                'taxoInfo' => $taxoInfo,
                'taxonomies' => $taxonomies
            ]);
    }
    public function getAdditionalInfo($type = '',$idsBlackList = array()){

        $metas = getMetaFromEntry($type);

        $taxoInfo = getTaxonomyFromEntry($type);
        $taxoTypes = $taxoInfo->taxoTypes;
        $taxo = $taxoInfo->taxo;
        $taxonomies = getTaxonomiesForEntry($taxoTypes);
        
        $parents = getEntryParents($type,0,$idsBlackList);

        return (object)['parents'=>$parents,'taxonomies' => $taxonomies,'metas'=>$metas,'taxo'=>$taxo];
    }
    public function create($type = 'type')
    {
        if(!app('Arcana\Core\ArcanaAcl')->multiAccess([2])){
            abort(404, 'Unauthorized action.');
        }
        $config = getEntryConfig($type);
        $info = $this->getAdditionalInfo($type);
        $view = 'arcana::admin.'.$type.'.create';
        if(!\View::exists($view)){
            $view = 'arcana::admin.entry.create';
        }

        $parsedTaxonomies = $this->castingTaxonomies($info->taxo,$info->taxonomies);
        $infoTaxoContainer = [];
        foreach ($info->taxo as $key => $value) {
            $infoTaxoContainer[$value['slug']] = $value;
        }
        
        Logger::info("Create Entry : ".$type);

        
        $this->layout->activeParent = $type;
        $this->layout->active = $type;
        $this->layout->title = setPageTitle("Create ".$config['single']);
        $this->layout->content = view($view,[
            'type' => $type,
            'parents' => $info->parents,
            'metas' => $info->metas,
            'taxo' => $info->taxo,
            'taxonomies' => $info->taxonomies,
            'config'    => $config,
            'parsedTaxonomies' => $parsedTaxonomies,
            'infoTaxoContainer' => $infoTaxoContainer
        ]);
    }

    public function edit($type = 'type',$id = 0)
    {
        if(!app('Arcana\Core\ArcanaAcl')->multiAccess([2,6])){
            abort(404, 'Unauthorized action.');
        }

        $this->layout->active = 'entry_'.$type;

        $entry = Entries::with(['taxonomies','metas','user','user.metas'])->find($id);
        if(empty($entry)) abort('404');

        $info = $this->getAdditionalInfo($type,[$id]);
        $config = getEntryConfig($type);

        $selectedTaxo = [];
        $view = 'arcana::admin.'.$type.'.edit';
        if(!\View::exists($view)){
            $view = 'arcana::admin.entry.edit';
        }
        if(!empty($entry->taxonomies)){
            foreach ($entry->taxonomies as $key => $value) {
                $selectedTaxo[] = $value->id;
            }
        }
        $parsedTaxonomies = $this->castingTaxonomies($info->taxo,$info->taxonomies);
        $infoTaxoContainer = [];
        foreach ($info->taxo as $key => $value) {
            $infoTaxoContainer[$value['slug']] = $value;
        }

        Logger::info("Edit Entry : ".$type);
        
        $this->layout->activeParent = $type;
        $this->layout->active = $type;
        $this->layout->title = setPageTitle("Edit ".$config['single']);
        $this->layout->content = view($view,[
            'parsedTaxonomies' => $parsedTaxonomies,
            'infoTaxoContainer' => $infoTaxoContainer,
            'type' => $type,
            'entry' => $entry,
            'parents' => $info->parents,
            'metas' => $info->metas,
            'taxo' => $info->taxo,
            'taxonomies' => $info->taxonomies,
            'selectedTaxo' => $selectedTaxo,
            'config' => $config,
        ]);

    }

    public function fileUploadHandler(Request $req,$entryId = 0)
    {

        if($req->input('file_upload')){
           foreach ($req->input('file_upload') as $key => $value) {
                $meta = Entrymetas::whereEntryId($entryId)->whereMetaKey($key)->first();
                if(empty($meta)) $meta = new Entrymetas();
                $meta->meta_key = $key;
                $meta->meta_value_text = $value;
                $meta->entry_id = $entryId;
                if(empty($value))
                {
                    $meta->media_id = 0;
                }

                if(!empty($req->file('uploader.'.$key))){
                    $media = mediaUploader('uploader.'.$key,$req,"","other");       
                    $meta->media_id = $media->id;
                    $meta->meta_value_text = $media->path;
                }

                $meta->save();
           }
        }

        if($req->input('media_container'))
        {
           foreach ($req->input('media_container') as $key => $value) {
                $meta = Entrymetas::whereEntryId($entryId)->whereMetaKey($key)->first();
                if(empty($meta)) $meta = new Entrymetas();
                $meta->meta_key = $key;
                $meta->meta_value_text = $value;
                $meta->entry_id = $entryId;
                
                if(empty($value))
                {
                    $meta->media_id = 0;
                }

                if(!empty($req->input('media_ids.'.$key))){
                    $meta->media_id = $req->input('media_ids.'.$key);
                }

                $meta->save();

           }        
        }
    
    }

    function saveMultiLangMeta(Request $req,$entryId = 0){
        if($req->input('multilangmetas')){
            foreach ($req->input('multilangmetas') as $key => $value) {
                $metaValue = "";
                $meta = Entrymetas::whereEntryId($entryId)->whereMetaKey($key)->first();
                if(empty($meta)) $meta = new Entrymetas();
                $meta->meta_key = $key;
                $meta->entry_id = $entryId;
                foreach ($value as $lang => $val) {
                    $metaValue .= '[:'.$lang.']'.$val;
                }
                $meta->meta_value_text = $metaValue;
                $meta->save();
            }
        }
    }

    public function save($type = 'type',$id = 0,Request $requests)
    {
        if(!app('Arcana\Core\ArcanaAcl')->multiAccess([2])){
            abort(404, 'Unauthorized action.');
        }

        

        $defaultLang = \Config::get('arcana.application.default_locale');
        
        $this->validate($requests,[
            'title.'.$defaultLang => 'required',
            'status' => 'required'
            ]);

        $input = $requests->input();
       
        $vars = ['title','content','excerpt'];

        foreach ($vars as $key => $value) {
            ${'default'.ucfirst($value)} = @$input[$value][$defaultLang];
            ${$value} = '';
            foreach ($input[$value] as $k => $v) {
                $tmp = '';
                if($id == 0){
                    $tmp = ${'default'.ucfirst($value)};                    
                }
                if(!empty($v)) $tmp = $v;                    

                ${$value} .= '[:'.$k.']'.$tmp;
            }
        }
        
        $entry = Entries::find($id);
        if(empty($entry)){
            $entry = new Entries();
            $entry->author = app('AdminUser')->user->id;
            $last = Entries::whereEntryType($type)->where('status','!=','deleted')->orderBy('sequence','desc')->first();
            if(!empty($last))
            {
                $entry->sequence = $last->sequence + 1;
            }
        }

        $slug = \Request::input('slug',str_slug($defaultTitle));
        if(empty($slug)) $slug = str_slug($defaultTitle);

        $originalSlug = $slug;
        if($entry->slug != $slug)
        {
            $check = Entries::where('id','!=',$id)->where('slug','like',$slug.'%')->count();
            if($check > 0){
                $slug .= '-'.$check;
            }
        }

        if($entry->slug != $slug && $type == 'page'){
            $routeList = getRouteList();
            $locale = \Config::get('arcana.application.default_locale').'/';
            $testRoute = $slug;
            $isMultiLang = false;
            if(count(\Config::get('arcana.application.locales')) > 1)
            {
                $isMultiLang = true;
                $testRoute = $locale.$slug;
            }

            while (in_array($testRoute, $routeList)) {
                $check++;
                $slug = $originalSlug.'-'.$check;
                $testRoute = $slug;
                if($isMultiLang) $testRoute = $locale.$slug;
            }
        }

        $metas = array();
        $tmpMetas = app('ArcanaSetting')->entries;
        foreach ($tmpMetas as $key => $value) {
            if($value['slug'] == $type){
                $metas = $value['metas'];
            }
        }

        $status = $input['status'];
        $published_at = \Request::input('published_at',date('Y-m-d H:i:s'));
        $published_at = date('Y-m-d H:i:s',strtotime($published_at));
        
        if($status == 'scheduled_publish')
        {
            $status = 'published';
            $published_at = date('Y-m-d H:i:s',strtotime($input['published_at']));
        }

        $entry->title = $title;
        $entry->slug = $slug;
        $entry->content = $content;
        $entry->excerpt = $excerpt;
        $entry->entry_type = $type;
        $entry->entry_parent = \Request::input('parent',0);
        $entry->modified_by = app('AdminUser')->user->id;
        $entry->status = $status;
        $entry->published_at = $published_at;
        $entry->touch();
        $entry->save();
        $entry->saveSEO(@$input['seo']);
        $entry->saveEntryMetaFromInput($metas,$input); 
        $this->saveMultiLangMeta($requests,$entry->id);
        $entry->taxonomies()->sync([]);  
        $entry->taxonomies()->sync(\Request::input('taxonomies',[]));
        
        if(!empty($input['taxonomies'])){
            foreach ($input['taxonomies'] as $key => $value) {
                $tmpTaxo = Taxonomies::find($value);
                $tmpTaxo->count = $tmpTaxo->count + 1;
                $tmpTaxo->save();
            }
        }
        
        \DB::table('entry_media')->whereEntryId($entry->id)->delete();

        $medias = \Request::input('media',[]);
        $medias = array_reverse(\Request::input('media',[]));            
        
              
       if(!empty($medias)){
            foreach ($medias as $key => $value) {
                $entry->medias()->attach($value);
            }
       }


        $this->fileUploadHandler($requests,$entry->id);
        Logger::info("Save Entry : ".$type);
        \Search::rebuild($entry);
        

        return redirect()->route('arcana_admin_entry_edit',[$type,$entry->id])->with('msg','Data Saved!');

    }

    public function delete($id)
    {
        if(!app('Arcana\Core\ArcanaAcl')->multiAccess([2])){
                abort(404, 'Unauthorized action.');
        }
       $entry = Entries::find($id);
      
       if(empty($entry)) return redirect()->back()->with('msg','Data Deleted!');
      
       $update = Entries::whereEntryParent($entry->id)->update(['entry_parent'=>0]);
      
       $entry->status = 'deleted';
       $entry->save();

       Logger::info("Delete Entry : ".parseMultiLang($entry->title));
      
       return redirect()->route('arcana_admin_entry_index',[$entry->entry_type])->with('msg','Data Deleted!');
    }
}
