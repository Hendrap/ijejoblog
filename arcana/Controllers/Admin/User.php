<?php

namespace Arcana\Controllers\Admin;

use Illuminate\Http\Request;
use Arcana\Requests;
use Arcana\Core\ArcanaController;
use \Setting as Settings;
use \Logger;

class User extends ArcanaController
{
    public $metas;
    public function __construct(){
        parent::__construct();
        $metas = unserialize(app('ArcanaSetting')->getSetting('user_default'));
        $this->metas  = @$metas['metas'];
        $this->layout->activeParent = 'main_account';
        $this->layout->active = 'all_account';
    }
    public function myAccount(){
        $user = app('AdminUser')->user;
        if($user->id == 1){
            abort(404);
        }
        if(empty($user)) abort(404);

        $this->layout->title = setPageTitle("My Account");
        $this->layout->content = view('arcana::admin.user.account',[
            'user' => $user,
            ]);

    }
    public function saveMyAccount(Request $request){
        $input = \Request::all();
        
        $this->validate($request,[
            'first_name' => 'required|alpha_dash',
            'last_name' => 'required|alpha_dash',
            'email' => 'required|email|unique',
            'password' => 'required|confirmed'
        ]);

        //validasi edit-create
        unset($input['_token']);
        $user = \User::find($input['id']);
        if($input['id'] == 0 || empty($user)){
            $user = new \User();
        }
        //email validation
        $check = \User::whereEmail($input['email'])->where('id','!=',$input['id'])->count();
        if($check > 0){
             return redirect()->back()->with('msg','Email has been taken before !');
        }
        //username
       
        //user 
        $user->email = $input['email'];
        $user->username = $input['email'];
     
        if(!empty($input['password'])){
            $user->password = bcrypt($input['password']);
        }
        $user->modified_by = app('AdminUser')->user->id;
        $user->touch();
        $user->save();
        return redirect()->back()->with('msg','Data Saved!');
    }
    public function status($id = 0,$status = ''){
        
        if(!app('Arcana\Core\ArcanAacl')->multiAccess([22])){
            abort(404, 'Unauthorized action.');
        }


        $user = \User::find($id);
        if(empty($user)) abort(404);

        Logger::info("Change ".$user->email."'s Status to ".$status);

        $user->status = $status;
        $user->save();
        return redirect()->back()->with('msg','Status Changed!');
    }

    public function getData(Request $req)
    {
                //container data
        $data = [];
        //pagination
        $start = $req->input('start',0);
        $take = $req->input('length',10);
        //search field
        $search = $req->input('search');
        //categgory
        $selectedRole = '';
        $role = $req->input('columns');
        //dd($role[1]);
        if(!empty($role[1]['search']['value']))
        {
            $selectedRole = $role[1]['search']['value'];
        }

        if(!empty($selectedRole))
        {
            $role = \Role::find($selectedRole);
            if(empty($role)) return json_encode(['data'=>[],'draw'=>$req->input('draw',1),'recordsTotal'=>0,'recordsFiltered'=>0]);
            $base = $role->users()->select('users.*')->where('users.id','!=',1);
        }else{
            $base = \User::where('users.id','!=',1);            
        }

        $recordsTotal = $base->where('status','!=','deleted')->count();

        //filter entry
        $users = $base->where('users.status','!=','deleted');
        if(!empty($search['value']))
        {
            $users = $users->join('usermetas',function($j){
                $j->on('users.id','=','usermetas.user_id');
            })->where(function($w)use($search){
                $w->where('meta_key','=','first_name');
                $w->orWhere('meta_key','=','last_name');
            })->groupBy('users.id')->select(['users.*']);
        }


        $users = $users->where(function($q)use($search){
             if(!empty($search['value'])){
                $q->where('email','like','%'.$search['value'].'%');
                $q->orWhere('username','like','%'.$search['value'].'%');
                $q->orWhere('meta_value_text','like','%'.$search['value'].'%');
            }
        });
        

        $recordsFiltered = $users->count();

        $status = $req->input('columns');
        if(!empty($status[4]['search']['value'])){
            $users = $users->whereStatus($status[4]['search']['value']);
        }

        $with = ['roles','metas'];
        $users = $users->take($take)->skip($start)->with($with);
        // $cols = [0=>'username',2=>'created_at',3=>'last_login',4=>'status'];
        $cols = [0=>'username',2=>'created_at',3=>'last_login',4=>'status'];
        $order = $req->input('order');
        if(!empty($order[0]))
        {
            $col = @$cols[$order[0]['column']];
            if(!empty($col))
            {
                $users = $users->orderBy($col,$order[0]['dir']);
            }else{
                $users = $users->orderBy('users.created_at','desc');
            }
        }else{
            $users = $users->orderBy('users.created_at','desc');
        }
        $users = $users->groupBy('users.id')->get();

        $base = [];
        $cols = ['username','role','created_at','last_login','status','action'];
        foreach ($cols as $col) {
           $base[$col] = "";
        }

        foreach ($users as $key => $value) {
            $html = '';
            $html .= view('arcana::admin.user.grid.user',['user'=>$value]);
            $item['html'] = $html;
             
            $data[] = $item;
        }

        return json_encode(['data'=>$data,'draw'=>$req->input('draw',1),'recordsTotal'=>$recordsTotal,'recordsFiltered'=>$recordsFiltered]);


    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        if(!app('Arcana\Core\ArcanaAcl')->multiAccess([22,26])){
            abort(404, 'Unauthorized action.');
        }
        
        $req = app('request');
        Logger::info("View User");
        $roles = \Role::where('id','!=',1)->get();
        $this->layout->title = setPageTitle("Accounts");   
        $this->layout->content = view('arcana::admin.user.index',['roles'=>$roles]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
         if(!app('Arcana\Core\ArcanaAcl')->multiAccess([22])){
            abort(404, 'Unauthorized action.');
         }
         $roles = \Role::where('id','!=',1)->get();

         Logger::info("Create User");


         $this->layout->title = setPageTitle("Create Account");  
         $this->layout->content = view('arcana::admin.user.createEdit',[
            'metas' => $this->metas,
            'roles' => $roles,
         ]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         if(!app('Arcana\Core\ArcanaAcl')->multiAccess([22])){
            abort(404, 'Unauthorized action.');
        }
        $input = \Request::all();
        $rules = [
            'metas.first_name' => 'required|alpha_dash',
            'metas.last_name' => 'required|alpha_dash',
            'metas.day' => 'numeric',
            'metas.month' => 'numeric',
            'metas.year' => 'numeric',
            'email' => 'required|email',
            
        ];
         if($request->file('avatar')){
            $rules['avatar'] = 'image|max:2000';
        }      
        if ($input['id'] == 0) {
              $rules['password'] = 'required|confirmed';
              $rules['email'] = 'required|email|unique:users';
          }  
        $this->validate($request,$rules);

        //validasi edit-create
        unset($input['_token']);
        $user = \User::find($input['id']);
        if($input['id'] == 0 || empty($user)){
            $user = new \User();
        }
        //email validation
        $check = \User::whereEmail($input['email'])->where('id','!=',$input['id'])->count();
        if($check > 0){
             return redirect()->back()->with('err','Email has been taken before !');
        }
        
        //user 
        $user->email = $input['email'];
        $user->username = $input['email'];
        $user->status = $input['status'];
        if(!empty($input['password'])){
            $user->password = bcrypt($input['password']);
        }
        $user->modified_by = app('AdminUser')->user->id;
        $user->touch();
        $user->save();
        //user metas
        $user->saveUserMetaFromInput($this->metas,$input);
        if($request->file('avatar')){
            $media = mediaUploader('avatar',$request);
            \Usermeta::whereMetaKey('avatar')->whereUserId($user->id)->delete();
            $meta = new \Usermeta();
            $meta->meta_key = 'avatar';
            $meta->meta_value_text = $media->path;
            $meta->user_id = $user->id;
            $meta->save();
        }
        //role
        $user->roles()->sync([$input['role']]);
        Logger::info("Save User ".$user->email);

        return redirect()->route('accounts.edit',[$user->id])->with('msg','Data Saved!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        if(!app('Arcana\Core\ArcanAacl')->multiAccess([22,26])){
            abort(404, 'Unauthorized action.');
        }


        if($id == 1){
            abort(404);
        }
        $data = array();
        $roles = \Role::where('id','!=',1)->get();
        $user = \User::with(['metas','roles','modifiedBy','modifiedBy.metas'])->find($id);
        foreach ($user->metas as $key => $value) {
            $data[$value->meta_key] = getEntryMetaFromArray($user->metas,$value->meta_key); 
        }
        if(empty($user)) abort(404);

        Logger::info("Edit User ".$user->email);

        $this->layout->title = setPageTitle("Edit Account");   
        $this->layout->active = 'user';
        $this->layout->content = view('arcana::admin.user.createEdit',[
            'user'=>$user,
            'roles' => $roles,
            'metas' => $this->metas,
            'dataMetas' => $data
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if(!app('Arcana\Core\ArcanAacl')->multiAccess([22])){
            abort(404, 'Unauthorized action.');
        }

        if($id == 1){
            return redirect()->back()->with('msg','Data Cannot Deleted!');
        }
        $user = \User::find($id);
        if(!empty($user)){
            Logger::info("Delete User ".$user->email);
            $user->status = 'deleted';
            $user->save();
        }
        return redirect()->back()->with('msg','Data Deleted!');
    }
}
