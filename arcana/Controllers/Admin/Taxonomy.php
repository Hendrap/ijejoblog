<?php

namespace Arcana\Controllers\Admin;

use Illuminate\Http\Request;
use Arcana\Requests;
use Arcana\Core\ArcanaController;
use \Taxonomy as Taxonomies;
use \Logger;

class Taxonomy extends ArcanaController
{

    public function getData($type = '',Request $req)
    {
        Logger::info("View Taxonomy : ".$type);

        //container data
        $data = [];
        //pagination
        $start = $req->input('start',0);
        $take = $req->input('length',10);
        //search field
        $search = $req->input('search');
        $isSearch = false;

        $cols = ['name','created_by','created_at','updated_at','action'];
        $base = [];
        foreach ($cols as $key => $value) {
            $base[$value] = "";
        }

        $taxonomies = Taxonomies::whereTaxonomyType($type);

        $recordsTotal = $taxonomies;
        if(empty($search['value'])){
            $recordsTotal = $recordsTotal->whereParent(0);
        }
        $recordsTotal = $recordsTotal->count();

        if(!empty($search['value']))
        {
            $isSearch = true;
            $taxonomies = $taxonomies->where(function($q)use($search){
                $q->where('name','like','%'.$search['value'].'%');
                $q->orWhere('description','like','%'.$search['value'].'%');
            });
        }else{
            $taxonomies = $taxonomies->whereParent(0);
        }

        $recordsFiltered = $taxonomies->count();

        $with = [
            'user',
            'childs'=>function($q)use($search){
                if(!empty($search['value']))
                {
                     $q->where('name','like','%'.$search['value'].'%');
                     $q->orWhere('description','like','%'.$search['value'].'%');
                }
            },
            'childs.user','childs.childs','childs.childs.user'
        ];

        if(!empty($search['value'])){
            $with[] = 'parents';
        }

        $cols = ['name','modified_by','created_at','updated_at'];
        $order = $req->input('order');
        if(!empty($order[0]))
        {
            $col = @$cols[$order[0]['column']];
            if(!empty($col))
            {
                $taxonomies = $taxonomies->orderBy($col,$order[0]['dir']);
            }else{
                $taxonomies = $taxonomies->orderBy('created_at','desc');
            }
        }else{
            $taxonomies = $taxonomies->orderBy('created_at','desc');
        }

        $taxonomies = $taxonomies->take($take)->skip($start)->with($with)->get();

        $data = [];
        $i = 0;
        // root
        foreach ($taxonomies as $key => $value) {
            $data[$i] = $value;
            $i++;
            
            if(empty($search['value'])){
                // 1st child
                foreach ($value->childs as $key => $childs) {
                    $data[$i] = $childs;
                    $data[$i]['child_level'] = "1";
                    $i++;
                    // 2nd child
                    foreach ($childs->childs as $key => $nd_childs) {
                        $data[$i] = $nd_childs;    
                        $data[$i]['child_level'] = "2";
                        $i++;
                    }   
                }
            }
            
        }

        return json_encode(['data'=>$data,'draw'=>$req->input('draw',1),'recordsTotal'=>$recordsTotal,'recordsFiltered'=>$recordsFiltered, 'search'=>$search]);

    }

    public function index($type = 'type')
    {
        if(!app('Arcana\Core\ArcanaAcl')->multiAccess([12,16])){
            abort(404, 'Unauthorized action.');
        }
        
        $info = getConfigBySlug('taxonomies',$type);

        $this->layout->activeParent = 'main_taxonomy';
        $this->layout->active = 'taxonomy_'.$type;
        $this->layout->title = setPageTitle('All '.$info['plural']);   
        $this->layout->content = view('arcana::admin.taxonomy.index',[
            'type'       => $type,
            'info'       => $info
            ]);
    }

    public function create($type = 'type')
    {
        if(!app('Arcana\Core\ArcanaAcl')->multiAccess([12])){
            abort(404, 'Unauthorized action.');
        }

        Logger::info("Create Taxonomy : ".$type);

        $taxonomies = Taxonomies::with(['childs'])->whereTaxonomyType($type)->get();
        $info = getConfigBySlug('taxonomies',$type);
        $this->layout->activeParent = 'main_taxonomy';
        $this->layout->active = 'taxonomy_'.$type;
        $this->layout->title = setPageTitle("Create ".$info['single']);   
        $this->layout->content = view('arcana::admin.taxonomy.createEdit',[
            'type' => $type,
            'taxonomies' => $taxonomies,
            'info'       => $info
            ]);
    }

    public function edit($type = 'type',$id = 0)
    {

        if(!app('Arcana\Core\ArcanaAcl')->multiAccess([12,16])){
            abort(404, 'Unauthorized action.');
        }

       Logger::info("Edit Taxonomy : ".$type);
       
       $taxonomies = Taxonomies::with('childs')->where('id','!=',$id)->whereTaxonomyType($type)->get();
       $taxonomy = Taxonomies::with(['user','user.metas'])->find($id);
       if(empty($taxonomy)) abort(404);

       $info = getConfigBySlug('taxonomies',$type);

       $this->layout->activeParent = 'main_taxonomy';
       $this->layout->active = 'taxonomy_'.$type;
       $this->layout->title = setPageTitle("Edit ".$info['single']);    
       $this->layout->content = view('arcana::admin.taxonomy.createEdit',[
            'type' => $type,
            'taxonomy' => $taxonomy,
            'taxonomies' => $taxonomies,
            'info'       => $info
            ]);
    }
    public function changeSlug(Request $req)
    {
        $id = $req->id;

        $taxonomy = Taxonomies::find($id);
        if(empty($taxonomy)) return json_encode(array('status'=>0));

        $slug = str_slug(\Request::input('slug'));
        if(empty($slug)){
            $slug = str_slug("Category");
        }

        $check = Taxonomies::where('id','!=',$id)->where('taxonomy_slug','like',$slug.'%')->count();
        if($check > 0){
            $slug .= '-'.$check;
        }

        $taxonomy->taxonomy_slug = $slug;
        $taxonomy->save();

        return json_encode(array('status'=>1,'slug'=>$slug));
    }

    public function save($type = 'type',$id = 0,Request $requests)
    {
        if(!app('Arcana\Core\ArcanaAcl')->multiAccess([12])){
            abort(404, 'Unauthorized action.');
        }

        $input = \Request::all();
        $this->validate($requests,[
            'name' => 'required'
            ]);
        $taxonomy = Taxonomies::find($id);
        if($id == 0 || empty($taxonomy)){
            $taxonomy = new Taxonomies();
        }

        $slug = str_slug(\Request::input('slug'));
        if(empty($slug)){
            $slug = str_slug($input['name']);
        }

        $check = Taxonomies::where('id','!=',$id)->where('taxonomy_slug','like',$slug.'%')->count();
        if($check > 0){
            $slug .= '-'.$check;
        }
        $taxonomy->taxonomy_slug = $slug;
        $taxonomy->taxonomy_type = $type;
        $taxonomy->parent = $requests->input('parent',0);
        $taxonomy->name             = @$input['name'];
        $taxonomy->description      = @$input['desc'];
        $taxonomy->modified_by      = app('AdminUser')->user->id;
        $taxonomy->seo_title        = @$input['seo_title'];
        $taxonomy->seo_description  = @$input['seo_description'];
        $taxonomy->seo_keyword      = @$input['seo_keyword'];
        $taxonomy->save();

        Logger::info("Save Taxonomy : ".$taxonomy->name);
        return redirect()->route('arcana_admin_taxonomy_edit',[$type,$taxonomy->id])->with('msg','Data Saved!');

    }

    public function delete($id)
    {
        
        if(!app('Arcana\Core\ArcanaAcl')->multiAccess([12])){
            abort(404, 'Unauthorized action.');
        }

       $taxonomy = Taxonomies::find($id);
       if(empty($taxonomy))
        return redirect()->back()->with('delete_msg','Data Deleted!');
      
       Logger::info("Delete Taxonomy : ".$taxonomy->name);

       $update = Taxonomies::whereParent($taxonomy->id)->update(['parent'=>0]);
      
       \DB::table('entry_taxonomy')->where('taxonomy_id', '=', $taxonomy->id)->delete();
       $taxonomy->delete();

       return redirect()->back()->with('delete_msg','Data Deleted!');

    }
}
