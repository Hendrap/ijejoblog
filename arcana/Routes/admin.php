<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Route untuk halaman CMS
|
*/

Route::group(['middleware' => 'ArcanaAdminGroup'], function () {
	
	Route::group(['namespace'=>'Arcana\Controllers','prefix' => 'admin'], function () {

		Route::get('refresh-csrf',['as'=>'arcana_admin_refresh_token','uses'=>function(){
			    return csrf_token();
			}]);

		//login logout 
		Route::get('/login',['as'=>'arcana_get_login','uses'=>'Credential@login']);
		Route::get('/logout',['as'=>'arcana_get_logout','uses'=>'Credential@logout']);
		Route::post('/login',['as'=>'arcana_post_login','uses'=>'Credential@postLogin']);
		Route::get('forgot-password',['as'=>'arcana_forgot_password','uses'=>'Credential@forgotPassword']);
		Route::post('forgot-password',['as'=>'arcana_post_forgot_password','uses'=>'Credential@postForgotPassword']);
		Route::get('password-recovery-success',['as'=>'arcana_success_password_recovery','uses'=>'Credential@successForgotPassword']);

		// ini ada  middleware buat bataesin akses admin
		// kalau AuthAdmin not found, artinya belum ditambahkan ke app/Http/kernel.php
		Route::group(['middleware' => 'Arcana\Middleware\AuthAdmin'],function(){
			
			Route::get('/',['as'=>'arcana_admin_index','uses'=>'Dashboard@index']);
			Route::get('/dashboard',['as'=>'arc_dashboard','uses'=>'Dashboard@index']);
				
			//settings prefix ,seluruh halaman /admin/setting masuk sini yah
			Route::group(['prefix' => 'setting'], function () {
				//general setting,isinya seo atau site description gitu
				
				Route::post('general/{type?}',['as'=>'arcana_admin_post_setting_general','uses'=>'Setting@postGeneral']);

				//general site, untuk domain path url dll
				Route::get('site-general',['as'=>'arcana_admin_get_setting_site_general','uses'=>'Setting@getSiteGeneral']);
				Route::get('extras',['as'=>'arcana_admin_get_extras','uses'=>'Setting@getExtras']);
				Route::get('labels',['as'=>'arcana_admin_get_labels','uses'=>'Setting@getLabels']);
				Route::any('get-data-log',['as'=>'arcana_admin_get_data_log','uses'=>'Setting@getDataActivityLog']);
				Route::get('activity-log',['as'=>'arcana_admin_activity_log','uses'=>'Setting@viewActivityLog']);				
					//
				Route::get('change-status-plugins/{plugin}/{status}',['as'=>'arcana_admin_get_status_plugins','uses'=>'Setting@getStatusPlugins']);


				//config crop images
				Route::resource('image','Image');
				//Roles & access

				//Entry Types
				Route::resource('entry_type','EntryType');

				//Taxonomy Types
				Route::resource('taxonomy_type','TaxonomyType');
				//user types;
				// Route::resource('user_type','UserType');
			});


			Route::group(['prefix' => 'general'], function () {
				Route::get('site-description',['as'=>'arcana_admin_get_setting_general','uses'=>'Setting@getSiteDescription']);
				Route::get('analytics',['as'=>'arcana_admin_get_analytics','uses'=>'Setting@getAnalytics']);
				Route::get('contacts',['as'=>'arcana_admin_get_contacts','uses'=>'Setting@getContacts']);
			});
			Route::get('plugins/all-plugins',['as'=>'arcana_admin_get_plugins','uses'=>'Setting@getPlugins']);

			//taxonomies
			Route::group(['prefix' => 'taxonomy'], function () {
				
				Route::get('/{type}',['as'=>'arcana_admin_taxonomy_index','uses'=>'Admin\Taxonomy@index']);
				Route::any('get-data/{type}',['as'=>'arcana_admin_taxonomy_get_data','uses'=>'Admin\Taxonomy@getData']);

				Route::get('/{type}/create',['as'=>'arcana_admin_taxonomy_create','uses'=>'Admin\Taxonomy@create']);
				Route::get('/{type}/edit/{id}',['as'=>'arcana_admin_taxonomy_edit','uses'=>'Admin\Taxonomy@edit']);
				Route::post('/delete/{id}',['as'=>'arcana_admin_taxonomy_delete','uses'=>'Admin\Taxonomy@delete']);
				Route::post('/{type}/save/{id}',['as'=>'arcana_admin_taxonomy_save','uses'=>'Admin\Taxonomy@save']);
				Route::post('/change-slug',['as'=>'arcana_admin_taxo_change_slug','uses'=>'Admin\Taxonomy@changeSlug']);
			});
			
			//entries
			Route::group(['prefix' => 'entry'], function () {
				
				Route::get('/{type}',['as'=>'arcana_admin_entry_index','uses'=>'Admin\Entry@index']);
				Route::any('get-data/{type}',['as'=>'arcana_admin_entry_get_data','uses'=>'Admin\Entry@getData']);

				Route::get('/{type}/create',['as'=>'arcana_admin_entry_create','uses'=>'Admin\Entry@create']);
				Route::get('/{type}/edit/{id}',['as'=>'arcana_admin_entry_edit','uses'=>'Admin\Entry@edit']);
				Route::post('/delete/{id}',['as'=>'arcana_admin_entry_delete','uses'=>'Admin\Entry@delete']);
				Route::post('/{type}/save/{id}',['as'=>'arcana_admin_entry_save','uses'=>'Admin\Entry@save']);
				Route::get('/status/{id}/{status}',['as'=>'arcana_admin_entry_status','uses'=>'Admin\Entry@status']);
				Route::post('/change-slug',['as'=>'arcana_admin_change_slug','uses'=>'Admin\Entry@changeSlug']);

				Route::get('/sequence/{EntryType}',['as'=>'arcana_admin_sequence_product','uses'=>'Admin\Entry@sequence']);
				Route::post('/save-sequence/{EntryType}',['as'=>'arcana_admin_save_sequence','uses'=>'Admin\Entry@saveSequence']);			
			});

			//media library
			Route::group(['prefix' => 'media'], function () {
				
				Route::any('get-data-media/{type}',['as'=>'arcana_admin_media_get_data','uses'=>'Admin\Media@getData']);
				Route::get('image',['as'=>'arcana_admin_media_image','uses'=>'Admin\Media@image']);
				Route::post('upload-image',['as'=>'arcana_admin_media_upload_image','uses'=>'Admin\Media@uploadImage']);

				Route::get('audio',['as'=>'arcana_admin_media_audio','uses'=>'Admin\Media@audio']);
				Route::post('upload-audio',['as'=>'arcana_admin_media_upload_audio','uses'=>'Admin\Media@uploadAudio']);

				Route::get('video',['as'=>'arcana_admin_media_video','uses'=>'Admin\Media@video']);
				Route::post('upload-video',['as'=>'arcana_admin_media_upload_video','uses'=>'Admin\Media@uploadVideo']);

				Route::get('other',['as'=>'arcana_admin_media_other','uses'=>'Admin\Media@other']);
				Route::post('upload-other',['as'=>'arcana_admin_media_upload_other','uses'=>'Admin\Media@uploadOther']);

				Route::post('embed-video-media',['as'=>'arcana_admin_media_embed_video','uses'=>'Admin\Media@embedVideo']);
				Route::get('entry-media',['as'=>'arcana_admin_media_entry','uses'=>'Admin\Media@entryMedia']);

				

				Route::post('delete-media',['as'=>'arcana_admin_media_delete','uses'=>'Admin\Media@delete']);
				Route::post('save-media',['as'=>'arcana_admin_media_save','uses'=>'Admin\Media@saveDescription']);

			});	


			//users
			Route::group(['prefix' => 'accounts'], function () {
				Route::get('roles-and-access',['as'=>'arcana_admin_all_roles','uses'=>'Role@index']);
				Route::any('roles-and-access/delete/{id}',['as'=>'arcana_admin_delete_roles','uses'=>'Role@destroy']);
				Route::get('roles-and-access/edit/{id}',['as'=>'arcana_admin_edit_roles','uses'=>'Role@edit']);
				Route::post('roles-and-access/store',['as'=>'arcana_admin_save_roles','uses'=>'Role@store']);
				Route::get('roles-and-access/create',['as'=>'arcana_admin_create_roles','uses'=>'Role@create']);

				Route::get('all-accounts',['as'=>'arcana_admin_all_account','uses'=>'Admin\User@index']);
				Route::get('new-account',['as'=>'arcana_admin_new_account','uses'=>'Admin\User@create']);
				Route::get('edit-account/{id}',['as'=>'arcana_admin_edit_account','uses'=>'Admin\User@edit']);
			});
			Route::resource('accounts','Admin\User');

			Route::get('user/change-status/{id}/{status}',['as'=>'arcana_admin_user_change_status','uses'=>'Admin\User@status']);
			Route::get('my-account',['as'=>'arcana_admin_myaccount','uses'=>'Admin\User@myAccount']);
			Route::any('users/get-data',['as'=>'arcana_admin_user_get_data','uses'=>'Admin\User@getData']);

			Route::post('save-my-account',['as'=>'arcana_admin_savemyaccount','uses'=>'Admin\User@saveMyAccount']);

		});
		// End bagian auth admin
	});

});