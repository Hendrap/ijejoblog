<?php 
namespace Arcana\Core;
/**
* 
*/
class AdminUser
{
	
	public $user;
	public $roles;
	public $avatar;
	public $name;
	function __construct()
	{
		$this->user = new \StdClass();
		$this->roles = array();
		$this->avatar = "";
		$this->name = "";
	}

}