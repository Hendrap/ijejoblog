<?php 

namespace Arcana\Core;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;


// Providers
// use Illuminate\Support\ServiceProvider;
// use Illuminate\Filesystem\Filesystem;

use \Config as Config;
use \Request as Request;
use \Logger;


class ArcanaController extends BaseController
{
	use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
	
	public $layout;
	public $acl;
	public function __construct(){
		set_time_limit(0);
		$this->acl = app('acl'); #kalau gak bisa, artinya di arcana/config/providers belum ada, package belum ditambahkan.
		$this->layout = view('arcana::admin.layout');
		$this->layout->active = '';
		$this->layout->activeParent = '';
	}

	public function callAction($method, $parameters)
	{

		$response = call_user_func_array(array($this, $method), $parameters);
		// If no response is returned from the controller action and a layout is being
		// used we will assume we want to just return the layout view as any nested
		// views were probably bound on this view during this controller actions.
		if (is_null($response) && ! is_null($this->layout))
		{
			$response = $this->layout;
		}
		return $response;
	}

}
