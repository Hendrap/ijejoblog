<?php 
namespace Arcana\Core;
/**
* 
*/
class ArcanaSetup
{
	
	function __construct()
	{
		
	}

	public function run()
	{
		$this->setRoles();		
		$this->setSingleton();
		app('ArcanaSetting');
	}

	public function setSingleton()
	{
		// memudahkan memanggil app('ArcanaSetting');
		app()->singleton('ArcanaSetting',function(){
			return new ArcanaSetting();
		});

		app()->singleton('AdminUser',function(){
			return new AdminUser();
		});

		// app()->singleton('ArcanaPlugin',function(){
		// 	return new Plugin();
		// });


		// app()->singleton('ArcanaMenu',function(){
		// 	return new Menu();
		// });
		
		// app()->bind('ArcanaMailMessage','Swift_Message');
		// app()->bind('ArcanaMailSender','Arcana\Core\Mailer');

	}
	
	// public function loadPlugins(){
	// 	$plugins = \Config::get('arcana.plugins');
	// 	if(empty($plugins)) return false;

	// 	foreach ($plugins as $key => $value) {
	// 		$plugin = app('ArcanaSetting')->getSetting($key);
	// 		if(class_exists('\\'.$key)){
	// 			app('ArcanaPlugin')->addPlugin($key,@$value['name']);				
	// 			if(empty($plugin)){
	// 				$plugin = new \Setting();
	// 				$plugin->bundle = 'arcana.plugins';
	// 				$plugin->setting_key = $key;
	// 				$plugin->setting_value = 'no';
	// 				$plugin->save();
	// 			}else{
	// 				if($plugin == 'yes'){
	// 					app()->register($key);
	// 				}
	// 			}
	// 		}else{
	// 			unset(app('ArcanaPlugin')->plugins[$key]);
	// 		}
	// 	}
	// }	
	public function setRoles()
	{
		
		$acl = app('acl');
		$acl->addResource('arcanaCMS');
		$roles = \Role::with('rules')->select(['id','name'])->get();

		foreach($roles as $v){

			$acl->addRole((string)$v->name);
			$access = array();
			foreach ($v->rules as $key => $value) {
				$access[] = $value->id;
			}
			if(!empty($access)){
				$acl->allow((string)$v->name,'arcanaCMS',$access);				
			}
		
		}


	}
}