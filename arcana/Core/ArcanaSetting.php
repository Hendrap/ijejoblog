<?php 

namespace Arcana\Core;

use Arcana\Models\Setting;
/**
* 
*/
class ArcanaSetting 
{
	
	public $settings = array();
	public $taxonomies = array();
	public $images = array();
	public $entries = array();
	function __construct()
	{
		$this->loadSetting();
	}

	function loadSetting()
	{
		$settings = Setting::select(['setting_key','setting_value','bundle','id'])->where('autoload','=','yes')->get();
		foreach ($settings as $key => $value) {
			if(!empty($value->setting_key)){
				$this->settings[$value->setting_key] = $value->setting_value;				
			}
		}
		
		$this->parseData($settings);
	}

	function getSetting($key = '')
	{
		$value = @$this->settings[$key];
		if(empty($value)){
			$tmpSetting = \Setting::whereSetting_key($key)->select('setting_value')->first();
			if(!empty($tmpSetting)){
				$value = $tmpSetting->setting_value;
				$this->settings[$key] = $value;
			}
		}
		return $value;
	}
	public function parseData($settings = array()){
		$vars = ['taxonomy' => 'taxonomies','entry'=>'entries','image'=>'images'];
		foreach ($vars as $key => $value) {
			foreach ($settings as $k => $v) {
				if($v->bundle == 'arcana.'.$key){
					$this->{$value}[$v->id] = unserialize($v->setting_value);
				}
			}
		}
	}
	function updateSetting($key = '',$value = '',$autoload = 'yes')
	{

		$updateSetting = \Setting::whereSetting_key($key)->first();
		if(empty($autoload)){
			$autoload = 'no';
		}
		if(empty($updateSetting)){
			$updateSetting = new \Setting();
			$updateSetting->autoload = $autoload;
		}
		$updateSetting->setting_key = $key;
		$updateSetting->setting_value = $value;
		$updateSetting->save();

		$this->settings[$key] = $value;
	}
}