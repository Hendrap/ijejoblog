@if($type == 'image')
<div class="col-sm-3">
    <div class="media-popup-thumbnail">
         <img onerror="this.src='{{ asset('backend/assets/images/placeholder_default.jpg') }}'" src="<?php echo $img ?>" alt="<?php echo $img ?>">
    </div>
    <input type="hidden" value="{{ $id }}" name="media[]" class="mid">
    <p style="display: none" class="title-entry-media">{{$title}}</p>
    <p style="display: none" class="title-entry-desc">{{$description}}</p>
</div>
@else
<div class="col-sm-3">
	<div class="media-popup-thumbnail video-audio-content">
	<h6><i class="icon-play"></i><br>{{$title}}</h6>
	<img src="{{ asset('backend/assets/images/placeholder_default.jpg') }}" alt="image name">
     <input type="hidden" value="{{ $id }}" name="media[]" class="mid">
	</div>
</div>
@endif