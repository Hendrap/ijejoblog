                                <td>
                                    <a href="#" class="play-media"><i class="icon-play4"></i></a>
                                    <a href="#" class="pause-media hide"><i class="icon-pause2"></i></a>
                                    
                                    <audio controls class="hide">
                                      <source src="{{ asset($entry->path) }}" type="audio/mpeg">
                                        Your browser does not support the audio element.
                                    </audio>
                                    
                                </td>
                                |alpha--datatable-separator--|<td class="text-bold">
                                    <h6>{{$entry->title}}</h6>
                                </td>
                                |alpha--datatable-separator--|<td class="td-date">
                                    {{date_format(date_create($entry->published_at),'j M Y')}}
                                    <br><span class="text-muted">{{date_format(date_create($entry->published_at),'g:i A')}}</span>
                                </td>
                                |alpha--datatable-separator--|<td class="td-date">
                                    {{date_format(date_create($entry->updated_at),'j M Y')}}
                                    <br><span class="text-muted">{{date_format(date_create($entry->updated_at),'g:i A')}}</span>
                                </td>
                                |alpha--datatable-separator--|<td class="td-date"><?php $info = unserialize($entry->media_meta); ?>{{ formatBytes((int)@$info['size']) }}</td>
                                |alpha--datatable-separator--|<td class="text-right table-actions">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-icon dropdown-toggle" data-toggle="dropdown">
                                            <i class="icon-menu7"></i> &nbsp;<span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="#" class="delete_media" data-id="{{$entry->id}}"><i class="icon-trash"></i> Delete</a></li>
                                        </ul>
                                    </div>
                                </td>
                                |alpha--datatable-separator--|<td>Category Cin</td>
