            <div class="logo">
                <img src="{{asset('cms/img/arcana/arcana-project-cms.png')}}">
            </div>
            <p>
            </p>
            <p>Login in. To see it in action.</p>
            <?php
               $n = 0;
            ?>
                @if (count($errors) > 0)
                @foreach($errors as $key => $error)
                    @if($n == 0)
                    <div class="alert alert-danger alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    {{ str_replace('metas.','',$error) }}
                    <?php $n++; ?>
                    @endif
                @endforeach
                @endif
            </div>
            <form class="m-t" action="{{ route('arcana_post_login') }}" method="POST" id="login-form">
                <div class="form-group">
                    <input type="email" class="form-control" name="email" value="{{ old('email') }}"  placeholder="Email" required="">
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" name="password" value="{{ old('password') }}"  placeholder="Password" required="">
                </div>
                <button type="submit" class="btn btn-primary block full-width m-b">Login</button>

                <input type="hidden" name="redirect" value="{{$redirect}}">
            </form>


            <style type="text/css">
                .logo img{
                    width: 100%;
                    padding: 10px;
                }

                .logo{
                    margin-top: 70px;
                }
            </style>