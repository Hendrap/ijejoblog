            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Entry - {{ $config['single'] }}</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a>Entry</a>
                        </li>
                        <li class="active">
                            <strong>{{ $config['single'] }}</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>

            <div class="wrapper wrapper-content animated fadeInRight">

                <form name="arcanaForm" enctype="multipart/form-data" method="POST" action="{{ route('arcana_admin_entry_save',[$type,0]) }}" class="form-horizontal">
                    
                <div class="row">
                    <div class="col-lg-9">
                        <div class="ibox float-e-margins">
                            @if(count(Config::get('arcana.application.locales')) > 1)
                            <div class="pull-right">
                                <select width="200" id="changeLang" class="select-language">
                                    @foreach(Config::get('arcana.application.locales') as $key  => $lang)
                                        <option value="{{ $key }}">{{ $lang }}</option>
                                    @endforeach
                                </select>
                            </div>
                            @endif
                            <div class="ibox-title">
                                <h3>
                                    Main Content
                                </h3>
                                @if(count($errors) > 0)
                                @foreach($errors->all() as $key => $error)
                                @if($key == 0)
                                <div class="alert bg-danger alert-styled-left">
                                    <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
                                    @if(Illuminate\Support\Str::contains($error,'required'))
                                        {{$error}}
                                        <span class="text-semibold">Required field(s) error or missing</span>
                                        
                                    @else
                                        <span class="text-semibold">{{ str_replace('metas.','',$error) }}</span>
                                    @endif
                                </div>
                                @endif
                                @endforeach
                                @endif
                            </div>
                            <div class="ibox-content">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">page Title</label>
                                    <div class="col-sm-10">
                                        @foreach(Config::get('arcana.application.locales') as $key  => $lang)
                                            <input style="display:none" id="title_{{$key}}" name="title[{{$key}}]" type="text" class="form-control" value="{{old('title.'.$key)}}">
                                        @endforeach
                                    </div>
                                    <!-- slug -->
                                    <div class="col-sm-2"></div>
                                    <div class="col-sm-10">
                                        <div class="post-url"><?php echo url('category/'.$type) ?>/<span class="slug"></span>
                                            <input type="text" style="display:none" name="slug" class="slug form-contro" value="{{ old('slug') }}">
                                        </div>&nbsp;&nbsp;&nbsp;
                                        <div class="display-inline-block mt-10 post-url-buttons">
                                            <a href="#" class="edit-url" style="display: inline;">Edit</a>
                                            <a href="javascript:void(0)" class="save-url" style="display:none"><i class="fa fa-check" style="font-size:10px;"></i></a>
                                            <span class="separator-url-form" style="display:none;opacity:0;">|</span>
                                            <a href="javascript:void(0)" class="cancel-edit-url" style="display:none"><i class="fa fa-ban" style="font-size:15px;"></i></a>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group @if(count($errors->get('content.'.$key))) invalid-input @endif">
                                        <div class="col-sm-2"></div>
                                        @foreach(Config::get('arcana.application.locales') as $key  => $lang)
                                        <div class="col-sm-10">
                                           <textarea style="display:none" id="content_{{$key}}" name="content[{{$key}}]" rows="10"  class="text-editor" cols="80" >{{old('content.'.$key)}}</textarea>
                                        </div>
                                        @endforeach
                                </div>

                                <div class="hr-line-dashed"></div>

                                <div class="form-group">
                                    <label class="control-label col-lg-2"></label>
                                    <div class="col-lg-10">
                                        @foreach(Config::get('arcana.application.locales') as $key  => $lang)
                                            <textarea style="display:none" id="excerpt_{{$key}}" name="excerpt[{{$key}}]" class="form-control">{{old('excerpt.'.$key)}}</textarea>
                                        @endforeach
                                    </div>
                                </div>

                                @if(!empty($metas))

                                <?php $nMedia = 0; ?>
                                @foreach($metas as $meta)
                                    @if(!empty($meta->meta_key) && !empty($meta->meta_key) && @$meta->meta_type == 'media_container' & $nMedia == 0)
                                        <div class="form-group">
                                        <label class="control-label col-lg-2">{{ $meta->meta_name }}</label>
                                            <div class="col-lg-10">
                                                {!! view('arcana::admin.entry.templates.media-create') !!}
                                            </div>
                                        </div>
                                        <?php $nMedia++; ?>
                                    @endif
                                @endforeach
                                
                                @foreach($metas as $meta)
                                     @if(!empty($meta->meta_key) && !empty($meta->meta_key) && (@$meta->meta_type == 'multi_lang' || empty($meta->meta_type)))
                                        <div class="form-group">
                                            <label class="control-label col-lg-2">{{ $meta->meta_name }}</label>
                                            <div class="col-lg-10">
                                                @foreach(Config::get('arcana.application.locales') as $key  => $lang)
                                                <?php echo view('arcana::admin.meta.text',[
                                                                    'meta_key' => $meta->meta_key,
                                                                    'value' => old('multilangmetas.'.$meta->meta_key.'.'.$key),
                                                                    'isMultiLang' => true,
                                                                    'lang' => $key
                                                                     ]) ?>
                                                @endforeach
                                            </div>
                                        </div>
                                    @endif
                                @endforeach


                                 @foreach($metas as $meta)
                                     @if(!empty($meta->meta_key) && !empty($meta->meta_key) && (@$meta->meta_type == 'default' || empty($meta->meta_type)))                        
                                        <div class="form-group">
                                            <label class="control-label col-lg-2">{{ $meta->meta_name }}</label>
                                            <?php 
                                                $sizes = config('arcana.style.grids');
                                                $metaSize = @$sizes[$meta->meta_size];
                                                if(empty($metaSize)) $metaSize = 'col-lg-10';
                                                if($metaSize == 'col-lg-9') $metaSize = 'col-lg-10';
                                             ?>
                                            <div class="{{ $metaSize }}">
                                                <?php echo view('arcana::admin.meta.'.$meta->meta_data_type,[
                                                                    'meta_key' => $meta->meta_key,
                                                                    'value' => old('metas.'.$meta->meta_key)
                                                                     ]) ?>
                                            </div>
                                        </div>
                                    @endif
                                @endforeach



                                @foreach($metas as $meta)

                                @if(!empty($meta->meta_key) && !empty($meta->meta_key) && @$meta->meta_type == 'file_upload')
                                    <div class="form-group" id="meta_{{$meta->meta_key}}">
                                        <label class="control-label col-lg-2">{{ $meta->meta_name }}</label>
                                        <div class="col-lg-10">
                                        <div class="input-group">
                                            <input readonly="" name="file_upload[{{$meta->meta_key}}]" id="file_upload_{{$meta->meta_key}}" style="height: 36px" type="text" class="form-control" placeholder="File name">
                                            <input class="meta_uploader" data-target="file_upload_{{$meta->meta_key}}" type="file" name="uploader[{{$meta->meta_key}}]" style="display:none">
                                            <span class="input-group-btn">
                                                <button class="btn btn-default btn-uploader-metas" type="button">Upload File</button>
                                            </span>
                                        </div>
                                        </div>
                                    </div>
                                @endif

                                @if(!empty($meta->meta_key) && !empty($meta->meta_key) && @$meta->meta_type == 'media')
                                    <div class="form-group" id="meta_{{$meta->meta_key}}">
                                        <label class="control-label col-lg-2">{{ $meta->meta_name }}</label>
                                        <div class="col-lg-10">
                                            <div class="input-group">
                                                <input readonly="" name="media_container[{{$meta->meta_key}}]" id="media_container{{$meta->meta_key}}" style="height: 36px" type="text" class="form-control" placeholder="Media name">
                                                <input class="media_uploader" type="text" id="media_ids{{$meta->meta_key}}" name="media_ids[{{$meta->meta_key}}]" style="display:none">
                                                <span class="input-group-btn">
                                                    <button data-meta="{{$meta->meta_key}}" class="btn btn-default btn-uploader-media" type="button">Open Media Library</button>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                @endif


                                @endforeach
                                @endif




                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        
                        <!-- Publish -->
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h3>
                                    Publish
                                </h3>
                            </div>
                            <div class="ibox-content">
                                <div class="form-group no-margin-left no-margin-right">
                                    <?php $status = ['published','draft','disabled','scheduled_publish'] ?>
                                    <select class="select-two form-control @if(count($errors->get('status'))) invalid-input @endif" id="publish_option" name="status">
                                        @foreach($status as $s)
                                            <option <?php echo ($s == old('status')) ? 'selected' : '' ?> value="{{ $s }}"><?php $s = str_replace('_publish','',$s) ?>{{  Illuminate\Support\Str::title($s) }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group has-feedback publish-date no-margin-left no-margin-right" style="display: none;">
                                    <?php 
                                    $date = old('published_at');
                                    if(empty($date)) $date = date('Y-m-d H:i:s');
                                 ?>
                                    <input value="{{ date(app('ArcanaSetting')->getSetting('date_format'),strtotime($date)) }}" type="text" name="published_at" class="date-picker form-control">

                                    <div class="form-control-feedback">
                                        <i style="line-height:30px" class="icon-calendar"></i>
                                    </div>
                                </div>
                                                        
                                <div class="form-group no-margin-bottom no-margin-left no-margin-right">
                                    <button type="submit" class="btn btn-primary pull-right ml-10">Save</button>
                                    <a href="{{route('arcana_admin_entry_index',[$type])}}" class="btn btn-white pull-right">Cancel</a>
                                </div>
                            </div>
                        </div>

                        <!-- Taxonomy -->
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h3>
                                    Taxonomy
                                </h3>
                            </div>
                            <div class="ibox-content">
                                <div class="form-group no-margin-left no-margin-right">
                                    <label>Parent</label>
                                    <select class="form-control entry-parent" placeholder="Select Parent" name="parent">
                                      <option selected="" value="0">None</option>
                                        @foreach($parents as $parent)
                                            <option <?php echo ($parent->id == old('parent')) ? 'selected' : '' ?> value="{{ $parent->id }}">{{ parseMultiLang($parent->title) }}</option>
                                            @if(!empty($parent->childs))
                                            @foreach($parent->childs as $p)
                                                <option <?php echo ($p->id == old('parent')) ? 'selected' : '' ?> value="{{ $p->id }}">&mdash; {{ parseMultiLang($p->title) }}</option>
                                            @endforeach
                                            @endif
                                        @endforeach
                                    </select>
                                </div>

                                <!-- Using Bootstrap multiselect, also check custom.js -->

                                <!--if category number < 5-->
                                @foreach($parsedTaxonomies['triTaxo'] as $key => $value)
                                    <div class="form-group no-margin-left no-margin-right">
                                        <label>{{  $infoTaxoContainer[$key]['single']  }}</label>
                                        @foreach($value as $item)
                                            <div class="checkbox">
                                                <label>
                                                    <input <?php echo in_array($item->id, old('taxonomies',[])) ? 'checked' : '' ?> name="taxonomies[]" value="{{ $item->id }}" type="checkbox" class="styled">
                                                    {{$item->name}}
                                                </label>
                                            </div>
                                        @endforeach
                                    </div>
                                @endforeach
                                 <!--/if category number < 5-->
                                
                                <!--if category number >= 5-->
                                @foreach($parsedTaxonomies['fiveTaxo'] as $key => $value)
                                <div class="form-group no-margin-left no-margin-right">
                                    <div class="multi-select-full">
                                        <select data-placeholder="Select {{ strtolower($infoTaxoContainer[$key]['single']) }}" name="taxonomies[]" class="multiselect-filtering" multiple="multiple">
                                        @foreach($value as $item)
                                            <option <?php echo in_array($item->id, old('taxonomies',[])) ? 'selected' : '' ?> value="{{$item->id}}">{{$item->name}}</option>
                                        @endforeach
                                        </select>
                                    </div>
                                </div>
                                @endforeach
                                <!--/if category number >= 5-->

                                <div class="form-group no-margin-left no-margin-right" style="margin-bottom:5px">
                                    <label>Keyword Tags</label>
                                    <input placeholder="Your keyword tags" data-role="tagsinput"  value="{{ old('metas.arcana_keyword_tags') }}" name="metas[arcana_keyword_tags]" type="text" class="keyword-tag form-control" style="width:100%;!important" readonly>
                                </div>
                                <div class="form-group clearfix add-keywords no-margin-left no-margin-right">
                                     <div class="input-group">
                                         <input type="text" placeholder="Start typing your keyword.." class="form-control pull-left keyword-input">
                                         <span class="input-group-btn">
                                            <button class="btn btn-primary pull-left add-tag-button" type="button">+</button>
                                        </span>
                                     </div>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>

                {{csrf_field()}}
                </form>
            </div>
            {!! view('arcana::admin.error-script',[
                    'errors'=>$errors,
                    'fields' => [
                        'name' => 'required',
                    ]
                ]); 
            !!}
            <script type="text/javascript" src="{{ asset('backend/assets/js/ckeditor/ckeditor.js') }}"></script>

            <?php $err = count($errors); ?>
            <!-- Slug -->
            <script type="text/javascript">
                $(document).on('click','.delete-img-entry-media',function(e){
                    e.preventDefault();
                    var that = $(this)
                    swal({
                        title: "Delete ?",
                        text: "You will not be able to recover deleted items, are you sure want to delete ?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, delete it!",
                        closeOnConfirm: true },
                        function(isConfirm){
                            if(isConfirm){
                                $(that).parent().parent().parent().remove();
                            }
                        });
                });

                // $( "#containerMedia" ).sortable({
                //   revert: true
                // });

                // MultiLang configurations Here
                function hideAllTitle(){
                    @foreach(Config::get('arcana.application.locales') as $key  => $lang)
                        $("#title_" + "{{ $key }}").hide();
                        $(".multilangmetas_" + "{{ $key }}").hide();
                    @endforeach
                }
                function hideAllEditors(){
                    @foreach(Config::get('arcana.application.locales') as $key  => $lang)
                        $("#cke_content_" + "{{ $key }}").hide();
                        $("#excerpt_" + "{{ $key }}").hide();
                    @endforeach
                }
                function showDefaultEditor(){
                    $("#cke_content_" + defaultLang).show();
                    $("#excerpt_" + defaultLang).show();
                }

                var defaultTitle = "{{ Config::get('arcana.application.default_locale') }}";
                var defaultLang = "{{ Config::get('arcana.application.default_locale') }}";
                @foreach(Config::get('arcana.application.locales') as $key  => $lang)
                        CKEDITOR.replace("content_" + "{{ $key }}",{height: '400px'});
                @endforeach
                CKEDITOR.on("instanceReady", function(event)
                {
                    hideAllEditors();
                    showDefaultEditor();
                });

                $("#changeLang").change(function(e){
                    hideAllEditors();
                    hideAllTitle();
                    defaultLang = $("#changeLang option:selected").val();
                    $("#title_" + defaultLang).show();
                    $(".multilangmetas_" + defaultLang).show();
                    showDefaultEditor();

                });

                $("#title_" + defaultLang).show();
                $(".multilangmetas_" + defaultLang).show();

                $("#title_" + defaultTitle).slug({
                    slug: 'slug',
                    hide:false
                });

                // End of MultiLang Config

                // Date Picker on Scheduled Publish
                $('.additional-info-panel .date-picker').addClass('datepicker-top').removeClass('date-picker');
        
                $('.datepicker-top').daterangepicker({
                                timePicker: true,
                                opens: "left",
                                applyClass: 'bg-slate-600',
                                cancelClass: 'btn-default',
                                singleDatePicker: true,
                                autoApply : true,
                                drops: "up",
                                locale: {
                                    format: 'MM/DD/YYYY h:mm a'
                                }
                });

                // End of Date Picker on Scheduled Publish

                // Select2 & Multiselect
                $(document).ready(function() {

                    initPreventClose();

                    $('.select-language').each(function() {
                        $(this).select2({
                            minimumResultsForSearch: Infinity,
                            width: '200px',
                        });
                    });

                    $('.entry-parent').each(function() {
                        select_placeholder = $(this).attr('placeholder');
                        $(this).select2({
                             placeholder: select_placeholder,
                        });
                    });

                    // autosize($('textarea'));
                    if($("#publish_option").val() == 'scheduled_publish')
                    {
                        $(".publish-date").show();
                    }

                });

            </script>

            <!-- Media -->
            <script type="text/javascript">
                var templateAddToEntry = '<?php echo newLine(view('arcana::admin.media.templates.img-entry-media',['img'=>'','id'=>0])) ?>';
                var elem = $('.add-edit-product-photos-content');
                var eleminner = $('.product-photo-container');
                var eleminnertwo = $('.product-photo-container-add');
                function checkimagescroll(){
               
                    totalinnerwidth = eleminner.innerWidth() + eleminnertwo.innerWidth() - 12;
                    if(elem.scrollLeft() + elem.width() ==  totalinnerwidth){
                        $('.product-photo-navigation.next').addClass('hide');
                    }
                    if(elem.scrollLeft() + elem.width() <  totalinnerwidth){
                        $('.product-photo-navigation.next').removeClass('hide');
                    }
                    if(elem.scrollLeft()  ==  0){
                        $('.product-photo-navigation.prev').addClass('hide');
                    }
                    if(elem.scrollLeft()  >  0){
                        $('.product-photo-navigation.prev').removeClass('hide');
                    }
                }
                checkimagescroll();
                
                
                $('.product-photo-navigation.next a').click(function(e){
                    e.preventDefault();
                    elem.get(0).scrollLeft += 250;
                    checkimagescroll();
                });
                $('.product-photo-navigation.prev a').click(function(e){
                    e.preventDefault();
                    elem.get(0).scrollLeft -= 250;
                    checkimagescroll();
                });
            </script>

            <script type="text/javascript">
                $(".btn-uploader-metas").click(function(e){
                    e.preventDefault();
                    if($(this).text().trim() == "Upload File")
                    {
                        $(this).parent().parent().parent().find('[type=file]').click();
                        $(this).text("Remove");
                    }else{
                        $(this).parent().parent().parent().find('[type=file]').val("");
                        $(this).parent().parent().parent().find('[type=text]').val("");
                        $(this).text("Upload File");
                    }
                    
                });
                $(".meta_uploader").change(function(e){
                    $("#" +  $(this).data('target')).val($(this).get(0).files[0].name);
                });

                $(".btn-uploader-media").click(function(e){
                    e.preventDefault();
                    if($(this).text().trim() == "Open Media Library")
                    {
                        var meta = $(this).data('meta');
                        // check layout.blade.php where is loadMedias() loaded
                        loadMedias("<?php echo route('arcana_admin_media_entry')."?page=1" ?>",function(){
                            $("#modalEntryMedia").modal('show');
                            editorName = 'metas.' + meta;
                        });
                    }else{
                        $(this).parent().parent().find("input").val("");
                        $(this).parent().parent().find("button").html("Open Media Library")
                    }

                });
            </script>

