 <script type="text/javascript" src="{{ asset('backend/assets/js/ckeditor/ckeditor.js') }}"></script>
    
    <script>
        $(document).on('click','.delete-img-entry-media',function(e){
            e.preventDefault();
            var that = $(this)
            swal({
                title: "Delete ?",
                text: "You will not be able to recover deleted items, are you sure want to delete ?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: true },
                function(isConfirm){
                    if(isConfirm){
                        $(that).parent().parent().parent().remove();
                        if(typeof productimagecheck == "function"){
                            productimagecheck();
                        }
                    }
                });
        });


        $( "#containerMedia" ).sortable({
          revert: true
        });
        function hideAllTitle(){
            @foreach(Config::get('arcana.application.locales') as $key  => $lang)
                $("#title_" + "{{ $key }}").hide();
            @endforeach
        }
        function hideAllEditors(){
            @foreach(Config::get('arcana.application.locales') as $key  => $lang)
                $("#cke_content_" + "{{ $key }}").hide();
                $("#excerpt_" + "{{ $key }}").hide();
            @endforeach
        }
        function showDefaultEditor(){
            $("#cke_content_" + defaultLang).show();
            $("#excerpt_" + defaultLang).show();
        }
        var defaultTitle = "{{ Config::get('arcana.application.default_locale') }}";
        var defaultLang = "{{ Config::get('arcana.application.default_locale') }}";
        @foreach(Config::get('arcana.application.locales') as $key  => $lang)
                CKEDITOR.replace("content_" + "{{ $key }}",{height: '400px'});
        @endforeach
        CKEDITOR.on("instanceReady", function(event)
        {
            hideAllEditors();
            showDefaultEditor();
        });

        $("#changeLang").change(function(e){
            hideAllEditors();
            hideAllTitle();
            defaultLang = $("#changeLang option:selected").val();
            $("#title_" + defaultLang).show();
            showDefaultEditor();

        });

        $("#title_" + defaultLang).show();

        @if(!isset($editMode))
        $("#title_" + defaultTitle).slug({
            slug: 'slug',
            hide:false
        });
        @endif
        
        $('.additional-info-panel .date-picker').addClass('datepicker-top').removeClass('date-picker');
            
        $('.datepicker-top').daterangepicker({
                        timePicker: true,
                        opens: "left",
                        applyClass: 'bg-slate-600',
                        cancelClass: 'btn-default',
                        singleDatePicker: true,
                        autoApply : true,
                        drops: "up",
                        locale: {
                            format: 'MM/DD/YYYY h:mm a'
                        }
        });

            $(document).ready(function() {
                templateAddToEntry = '<?php echo newLine(view('arcana::admin.media.templates.img-entry-media',['img'=>'','id'=>0])) ?>';

                            initPreventClose();

               $('.select-language').each(function() {
                                    $(this).select2({
                                        minimumResultsForSearch: Infinity,
                                        width: '200px',
                                    });
                });
                
                autosize($('textarea'));
                $("#publish_option").trigger('change');
                
            });

            var elem = $('.add-edit-product-photos-content');
            var eleminner = $('.product-photo-container');
            var eleminnertwo = $('.product-photo-container-add');
                
                function checkimagescroll(){
                   
                    totalinnerwidth = eleminner.innerWidth() + eleminnertwo.innerWidth() - 12;
                    if(elem.scrollLeft() + elem.width() ==  totalinnerwidth){
                        $('.product-photo-navigation.next').addClass('hide');
                    }
                    if(elem.scrollLeft() + elem.width() <  totalinnerwidth){
                        $('.product-photo-navigation.next').removeClass('hide');
                    }
                    if(elem.scrollLeft()  ==  0){
                        $('.product-photo-navigation.prev').addClass('hide');
                    }
                    if(elem.scrollLeft()  >  0){
                        $('.product-photo-navigation.prev').removeClass('hide');
                    }
                }
                checkimagescroll();
                
                
                $('.product-photo-navigation.next a').click(function(e){
                    e.preventDefault();
                    elem.get(0).scrollLeft += 250;
                    checkimagescroll();
                });
                $('.product-photo-navigation.prev a').click(function(e){
                    e.preventDefault();
                    elem.get(0).scrollLeft -= 250;
                    checkimagescroll();
                });

    </script>
    <style>
        
        .bootstrap-tagsinput input {
            
            width: 100%!important;
        }
        
    </style>
    <script type="text/javascript">
        $(".btn-uploader-metas").click(function(e){
            e.preventDefault();
            if($(this).text().trim() == "Upload File")
            {
                $(this).parent().parent().parent().find('[type=file]').click();
                $(this).text("Remove");
            }else{
                $(this).parent().parent().parent().find('[type=file]').val("");
                $(this).parent().parent().parent().find('[type=text]').val("");
                $(this).text("Upload File");
            }
            
        });
        $(".meta_uploader").change(function(e){
            $("#" +  $(this).data('target')).val($(this).get(0).files[0].name);
        });

        $(".btn-uploader-media").click(function(e){
            e.preventDefault();
            if($(this).text().trim() == "Open Media Library")
            {
                var meta = $(this).data('meta');
                loadMedias("<?php echo route('arcana_admin_media_entry')."?page=1" ?>",function(){
                    $("#modalEntryMedia").modal('show');    
                    editorName = 'metas.' + meta;
                });
            }else{
                $(this).parent().parent().find("input").val("");
                $(this).parent().parent().find("button").html("Open Media Library")
            }

        });
    </script>
