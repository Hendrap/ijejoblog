                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <div class="panel-heading-title">
                            <h5 class="panel-title">Media</h5>
                        </div>
                    </div>
                    <div class="panel-body">
                        {!! $content !!}
                    </div>
                </div>