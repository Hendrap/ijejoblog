<script type="text/javascript">
    //slug
             var currentSlug = "<?php echo $entry->slug ?>";
             $('.edit-url-custom').click(function(e){
                e.preventDefault;
                $('.post-url span').hide();
                $('.post-url input').show();
                $('.cancel-edit-url-custom, .save-url-custom, .separator-url-form').show();
                $('.edit-url-custom').hide();
            });
            
            
            $('.save-url-custom').click(function(e){
                e.preventDefault;
                newurl = $('.post-url input').val();
                currentSlug = newurl;
                $.ajax({
                    url:"<?php echo route("arcana_admin_change_slug") ?>",
                    data:{slug:currentSlug,entry_id:"<?php echo $entry->id ?>"},
                    type:"POST",
                    dataType:"json",
                    success:function(data){
                        $('.cancel-edit-url-custom, .save-url-custom, .separator-url-form').hide();
                        $('.post-url span').show();
                        $('.post-url input').hide();
                        $('.edit-url-custom').show();
                        currentSlug = data.slug;
                        $('.post-url span').html('').html(currentSlug);
                        $('.post-url input').val(currentSlug);;
                    }
                });
                
            });
            
            $('.cancel-edit-url-custom').click(function(e){
                e.preventDefault;
                $('.cancel-edit-url-custom, .save-url-custom, .separator-url-form').hide();
                $('.post-url span').show();
                $('.post-url input').hide();
                $('.edit-url-custom').show()
                $('.post-url input').val(currentSlug);;
                
            });
</script>