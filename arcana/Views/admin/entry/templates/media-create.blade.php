                    <div class="add-edit-product-photos">
                            <div class="product-photo-navigation next">
                                <a href="#"><i class="fa fa-angle-right"></i></a>
                            </div>
                            <div class="product-photo-navigation prev">
                                <a href="#"><i class="fa fa-angle-left"></i></a>
                            </div>
                            <div class="add-edit-product-photos-content">
                                <div class="product-photo-container-add clearfix">
                                    <div class="add-product-photo">
                                        <a href="javascript:void(0)" id="btnShowEntryMedia" class="add-product-photo-trigger">
                                            <div class="add-product-content">
                                                <span>Add Photo(s)</span>
                                                <span class="btn btn-rounded btn-primary">+</span>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div id="containerMedia" class="product-photo-container clearfix">
                                   
                                </div>
                            </div>
                        </div> 