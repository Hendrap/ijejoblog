            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Entry - {{ $config['single'] }}</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a>Entry</a>
                        </li>
                        <li class="active">
                            <strong>{{ $config['single'] }}</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>

            <div class="wrapper wrapper-content animated fadeInRight">

                <form name="arcanaForm" enctype="multipart/form-data" method="POST" action="{{ route('arcana_admin_entry_save',[$type,0]) }}" class="form-horizontal">
                    
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            @if(count(Config::get('arcana.application.locales')) > 1)
                            <div class="pull-right">
                                <select width="200" id="changeLang" class="select-language">
                                    @foreach(Config::get('arcana.application.locales') as $key  => $lang)
                                        <option value="{{ $key }}">{{ $lang }}</option>
                                    @endforeach
                                </select>
                            </div>
                            @endif
                            <div class="ibox-title">
                                <h3>
                                    Slideshow Content
                                </h3>
                                @if(count($errors) > 0)
                                @foreach($errors->all() as $key => $error)
                                @if($key == 0)
                                <div class="alert bg-danger alert-styled-left">
                                    <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
                                    @if(Illuminate\Support\Str::contains($error,'required'))
                                        {{$error}}
                                        <span class="text-semibold">Required field(s) error or missing</span>
                                        
                                    @else
                                        <span class="text-semibold">{{ str_replace('metas.','',$error) }}</span>
                                    @endif
                                </div>
                                @endif
                                @endforeach
                                @endif
                            </div>
                            <div class="ibox-content">
                                <input type="hidden" name="id" value="0">
                                <input type="hidden" name="status" value="published">
                                <input type="hidden" name="published_at" value="{{date('Y-m-d H:i:s')}}">

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">page Title</label>
                                    <div class="col-sm-10">
                                        @foreach(Config::get('arcana.application.locales') as $key  => $lang)
                                            <input style="display:none" id="title_{{$key}}" name="title[{{$key}}]" type="text" class="form-control" value="{{old('title.'.$key)}}">
                                        @endforeach
                                    </div>
                                    <!-- slug -->
                                    <div class="col-sm-2"></div>
                                    <div class="col-sm-10">
                                        <div class="post-url"><?php echo url('category/'.$type) ?>/<span class="slug"></span>
                                            <input type="text" style="display:none" name="slug" class="slug form-contro" value="{{ old('slug') }}">
                                        </div>&nbsp;&nbsp;&nbsp;
                                        <div class="display-inline-block mt-10 post-url-buttons">
                                            <a href="#" class="edit-url" style="display: inline;">Edit</a>
                                            <a href="javascript:void(0)" class="save-url" style="display:none"><i class="fa fa-check" style="font-size:10px;"></i></a>
                                            <span class="separator-url-form" style="display:none;opacity:0;">|</span>
                                            <a href="javascript:void(0)" class="cancel-edit-url" style="display:none"><i class="fa fa-ban" style="font-size:15px;"></i></a>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group @if(count($errors->get('content.'.$key))) invalid-input @endif">
                                        <div class="col-sm-2">Description</div>
                                        @foreach(Config::get('arcana.application.locales') as $key  => $lang)
                                        <div class="col-sm-10">
                                           <textarea style="display:none" id="content_{{$key}}" name="content[{{$key}}]" rows="10"  class="text-editor" cols="80" >{{old('content.'.$key)}}</textarea>
                                        </div>
                                        @endforeach
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-lg-2">Excerpt</label>
                                    <div class="col-lg-10">
                                        @foreach(Config::get('arcana.application.locales') as $key  => $lang)
                                            <textarea style="display:none" id="excerpt_{{$key}}" name="excerpt[{{$key}}]" class="form-control">{{old('excerpt.'.$key)}}</textarea>
                                        @endforeach
                                    </div>
                                </div>

                                <div class="form-group no-margin-bottom no-margin-left no-margin-right">
                                    <button type="submit" class="btn btn-primary pull-right ml-10">Save</button>
                                    <a href="{{route('arcana_admin_entry_index',[$type])}}" class="btn btn-white pull-right">Cancel</a>
                                </div>

                                <div class="hr-line-dashed"></div>


                                <div class="slider-list">

                                    <div class="slider-list-header clearfix mb-15">

                                        <div class="form-group pull-left  mr-5 no-margin-bottom">
                                            <label style="height: 30px; line-height: 30px;margin-bottom: 0px;"><strong>Media</strong></label>
                                        </div>
                                    </div>

                                    <div class="slider-list-body">
                                    <div class="media-row slider-thumbnail-row" id="containerMedia">
                                                <div class="media-grid  add-image-cont">
                                                     <a href="#" class="add-image">

                                                        <div class="thumbnail">
                                                            
                                                            
                                                                   
                                                                    <div class="thumb-overlay" id="btnShowEntryMedia">

                                                                        <span>Add Image(s)</span>
                                                                        <span class="btn btn-rounded btn-primary">+</span>

                                                                    </div>
                                                             
                                                            
                                                            <div class="thumb add-new">
                                                                <img src="{{asset('backend/assets/images/slide-thumbnail-big.png')}}" alt="image name">
                                                            </div>
                                                             <div class="caption">
                                                                <h6 class="no-margin-top text-semibold">
                                                                    <a href="#" class="text-default">&nbsp;</a>
                                                                   
                                                                    
                                                                    
                                                                </h6>
                                                                <span>&nbsp;</span>
                                                            </div>
                                                           
                                                        </div>
                                                        </a>
                                                    </div>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>

                {{csrf_field()}}
                </form>
            </div>

        <?php echo view('arcana::admin.media.modal.edit-image') ?>


{!! view('arcana::admin.entry.templates.response-handler',['errors'=>@$errors]) !!}
<script type="text/javascript">
    $(document).ready(function () {

    $( "#containerMedia" ).sortable({
      revert: true
    });

    function hideAllTitle(){
        @foreach(Config::get('arcana.application.locales') as $key  => $lang)
            $("#title_" + "{{ $key }}").hide();
        @endforeach
    }
    function hideAllEditors(){
        @foreach(Config::get('arcana.application.locales') as $key  => $lang)
            $("#content_" + "{{ $key }}").hide();
        @endforeach
    }
    function showDefaultEditor(){
        $("#content_" + defaultLang).show();
    }
    var defaultTitle = "{{ Config::get('arcana.application.default_locale') }}";
    var defaultLang = "{{ Config::get('arcana.application.default_locale') }}";
   
    
    hideAllEditors();
    showDefaultEditor();
    

    $("#changeLang").change(function(e){
        hideAllEditors();
        hideAllTitle();
        defaultLang = $("#changeLang option:selected").val();
        $("#title_" + defaultLang).show();
        showDefaultEditor();

    });

    $("#title_" + defaultLang).show();

    $("#title_" + defaultTitle).slug({
        slug: 'slug',
        hide:false
    });
});
</script>
<script type="text/javascript">
    $(document).ready(function(){
        initPreventClose();
    })
</script>


