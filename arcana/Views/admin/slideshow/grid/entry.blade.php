                                <td class="title-column">
                                    <h6>
                                        <a href="{{ route('arcana_admin_entry_edit',[$type,$entry->id]) }}">
                                         @if($entry->entry_parent > 0)
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        @endif
                                        {{parseMultiLang($entry->title)}}
                                        <?php if(!empty($entry->parent)){ ?>
                                            <i class="text-info">Parent : {{parseMultiLang($entry->parent->title)}}</i>
                                        <?php } ?>
                                        </a>
                                    </h6>
                                </td>
                                |arcana--datatable-separator--|<td>{{count($entry->medias)}}</td>
                                |arcana--datatable-separator--|<td>{{$entry->user->username}}</td>
                                |arcana--datatable-separator--|<td class="td-date">
                                    {{date_format(date_create($entry->published_at),app('ArcanaSetting')->getSetting('date_format'))}}
                                    <br><span class="text-muted">{{date_format(date_create($entry->published_at),app('ArcanaSetting')->getSetting('time_format'))}}</span>
                                </td>
                                |arcana--datatable-separator--|<td class="td-date">
                                    {{date_format(date_create($entry->created_at),app('ArcanaSetting')->getSetting('date_format'))}}
                                    <br><span class="text-muted">{{date_format(date_create($entry->created_at),app('ArcanaSetting')->getSetting('time_format'))}}</span> </td>
                                |arcana--datatable-separator--|<td class="td-date">
                                    {{date_format(date_create($entry->updated_at),app('ArcanaSetting')->getSetting('date_format'))}}
                                    <br><span class="text-muted">{{date_format(date_create($entry->updated_at),app('ArcanaSetting')->getSetting('time_format'))}}</span>
                                </td>
                                |arcana--datatable-separator--|
                                <td>
                                    <?php 
                                    $color = 'label-danger';
                                    if($entry->status =='published' && strtotime($entry->published_at) <= time())
                                        $color = 'label-primary';
                                    elseif($entry->status == 'published' && strtotime($entry->published_at) > time())
                                        $color = 'badge-info';
                                    elseif($entry->status =='disabled')
                                        $color = 'bg-grey';
                                    elseif($entry->status =='draft')
                                        $color = 'label-warning';
                                    else
                                    ?>
                                    <span class="label {{$color}}">{{ ucfirst($entry->status) }}</span>
                                </td>
                                |arcana--datatable-separator--|<td class="text-right table-actions">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-icon dropdown-toggle" data-toggle="dropdown">
                                            <i class="icon-menu7"></i> &nbsp;<span class="caret"></span>
                                        </button>

                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="{{ route('arcana_admin_entry_edit',[$type,$entry->id]) }}"><i class="icon-pencil5"></i> Edit</a></li>
                                            @if($entry->status != 'disabled')
                                            <li><a href="{{ route('arcana_admin_entry_status',[$entry->id,'disabled']) }}"><i class="icon-cancel-square"></i> Disable</a></li>
                                            @endif
                                            @if($entry->status != 'published')
                                            <li><a href="{{ route('arcana_admin_entry_status',[$entry->id,'published']) }}"><i class="icon-checkmark4"></i> Publish</a></li>
                                            @endif
                                            @if($entry->status != 'draft')
                                            <li><a href="{{ route('arcana_admin_entry_status',[$entry->id,'draft']) }}"><i class="icon-clipboard2"></i> Draft</a></li>
                                            @endif
                                            <li class="divider"></li>
                                            <li><a href="#" class="confirm" data-id="{{$entry->id}}"><i class="icon-trash"></i> Delete</a></li>
                                        </ul>
                                    </div>
                                </td>
                                |arcana--datatable-separator--|<td>Category Cin</td>
