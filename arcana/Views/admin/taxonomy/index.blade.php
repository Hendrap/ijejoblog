
 @if(session('msg'))

    <div class="alert bg-success alert-styled-left">

        <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>

        <span class="text-semibold">{{session('msg')}}</span>

    </div>

@endif

            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>TAXONOMY - {{ $info['plural'] }}</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a>Taxonomy</a>
                        </li>
                        <li class="active">
                            <strong>{{ $info['plural'] }}</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                    <div class="heading-elements">

                        <a href="{{ route('arcana_admin_taxonomy_create',[$type]) }}" class="btn btn-primary"><b><i class="fa fa-file-o"></i></b> New Category</a>

                    </div>
                </div>
            </div>

        <div class="wrapper wrapper-content animated fadeInRight ecommerce" id="taxonomy">


            <div class="ibox-content m-b-sm border-bottom">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Type to filter.." @keyup="filterData"> <span class="input-group-addon"><i class="fa fa-search"></i></span>
                        </div>
                    </div>
                    <!-- 
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="status">Status</label>
                            <select name="status" id="status" class="form-control">
                                <option value="" selected>All Status</option>
                                <option value="1">Enabled</option>
                                <option value="0">Disabled</option>
                            </select>
                        </div>
                    </div> -->
                </div>

            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">
                        <div class="ibox-content">

                            <table class="footable table table-stripped toggle-arrow-tiny" data-page-size="15">
                                <thead>
                                <tr>

                                    <th data-toggle="true">Name</th>
                                    <th data-hide="">Creator</th>
                                    <th data-hide="phone">Created</th>
                                    <th data-hide="phone">Modified</th>
                                    <th class="text-right" data-sort-ignore="true"></th>

                                </tr>
                                </thead>
                                <tbody>
                                <tr v-for="data in tableData">
                                    <td>
                                       <span class="fa fa-angle-right" v-show="data.child_level == 1"></span>
                                       <span class="fa fa-angle-double-right" v-show="data.child_level == 2"></span>
                                        @{{ data.name }} <i class="text-info" v-if="data.parents">Parent : @{{data.parents.name}}</i>
                                    </td>
                                    <td v-if="data.user"> @{{ data.user.email }}
                                    </td>
                                    <td v-else> Super Administrator
                                    </td>
                                    
                                    <td>
                                        @{{ data.created_at }}
                                    </td>
                                    <td>
                                        @{{ data.updated_at }}
                                    </td>   
                                    <td class="text-right">
                                        <div class="btn-group">
                                            <button data-toggle="dropdown" class="btn btn-default dropdown-toggle" aria-expanded="true"><i class="fa fa-bars"></i> <span class="caret"></span></button>
                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li><a :href="'{{ route('arcana_admin_taxonomy_edit',[$type,'']) }}/' + data.id"><i class="fa fa-edit"></i> Edit</a></li>
                                                <li class="divider"></li>
                                                <li><a v-bind:data-id="data.id" :href="'{{route('arcana_admin_taxonomy_delete',[''])}}/' + data.id" class="confirm"><i class="fa fa-trash-o"></i> Delete</a></li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                                
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td colspan="6">    
                                        <ul class="pagination pull-right"></ul>
                                    </td>
                                </tr>
                                </tfoot>
                            </table>

                            <form style="display:none" id="form_delete" action="{{route('arcana_admin_taxonomy_delete',[0])}}" method="POST">
                                {{csrf_field()}}
                                <input type="hidden" name="id" value="0" >
                            </form>


                        </div>
                    </div>
                </div>
            </div>

        </div>
        <script type="text/javascript">

            $(document).on('click',".confirm",function(e){
                e.preventDefault();
                    $("#form_delete").attr('action',$("#form_delete").attr('action').replace('/' + 0,'/' + $(this).data('id')));
                    $("#form_delete").find("[name=id]").val($(this).data('id'));

                swal({
                    title: "Delete ?",
                    text: "You will not be able to recover deleted items, are you sure want to delete ?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: false },
                    function(isConfirm){
                    if(isConfirm){
                        $("#form_delete").submit();
                    }

                });

            });

        </script>

        <script type="text/javascript">
            var urlGetData = "{{ route('arcana_admin_taxonomy_get_data',[$type]) }}";
            
            console.log(urlGetData);
            $(document).ready(function(){
                $('#page-render .pagination').addClass('custom-pagination');
                // fill data
                taxoTables.getData();
            });
        </script>
        <script src="{{ asset('cms/js/framework/vue2/taxonomy.js') }} "></script>