<?php
    $id = 0;
    $name = old('name');
    $slug = old('slug');
    $desc = old('desc');
    $parent = old('parent');
    $seo_title = old('seo_title');
    $seo_keyword = old('seo_keyword');
    $seo_description = old('seo_description');
    if(!empty($taxonomy)){
        $id = $taxonomy->id;
        $name = old('name',$taxonomy->name);
        $slug = old('slug',$taxonomy->taxonomy_slug);
        $desc = old('desc',$taxonomy->description);
        $parent = old('parent',$taxonomy->parent);
        $seo_title = old('seo_title',$taxonomy->seo_title);
        $seo_keyword = old('seo_keyword',$taxonomy->seo_keyword);
        $seo_description = old('seo_description',$taxonomy->seo_description);
    }

 ?>
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Taxonomy - {{ $info['single'] }}</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a>Taxonomy</a>
                        </li>
                        <li class="active">
                            <strong>{{ $info['single'] }}</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>

            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h3>
                                    @if($id == 0)
                                        Main Content
                                    @else
                                        {{ $taxonomy->name }}
                                    @endif
                                </h3>
                                @if($id != 0)
                                <p class="content-group-lg">Created on {{date_format(date_create($taxonomy->created_at),app('ArcanaSetting')->getSetting('date_format'))}} at {{date_format(date_create($taxonomy->created_at),app('ArcanaSetting')->getSetting('time_format'))}} by {{ generateName($taxonomy->user) }}, last modified on {{date_format(date_create($taxonomy->updated_at),app('ArcanaSetting')->getSetting('date_format'))}} at {{date_format(date_create($taxonomy->updated_at),app('ArcanaSetting')->getSetting('time_format'))}}</p>
                                @endif

                                @if(count($errors) > 0)
                                @foreach($errors->all() as $key => $error)
                                @if($key == 0)
                                <div class="alert bg-danger alert-styled-left">
                                    <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
                                    @if(Illuminate\Support\Str::contains($error,'required'))
                                        <span class="text-semibold">Required field(s) error or missing</span>
                                    @else
                                        <span class="text-semibold">{{ str_replace('metas.','',$error) }}</span>
                                    @endif
                                </div>
                                @endif
                                @endforeach
                                @endif
                            </div>
                            <div class="ibox-content">

                                <form name="arcanaForm" id="form-site-description" method="POST" action="{{route('arcana_admin_taxonomy_save',[$type,$id])}}" class="form-horizontal">
                                    <div class="form-group"><label class="col-sm-2 control-label">Category Name</label>
                                        <div class="col-sm-10"><input type="text" name="name" class="form-control" id="name" value="{{$name}}" ></div>
                                        <!-- slug -->
                                        <div class="col-sm-2"></div>
                                        @if($id == 0)
                                        <div class="col-sm-10">
                                            <div class="post-url"><?php echo url('category/'.$type) ?>/<span class="slug">{{$slug}}</span>
                                                <input type="text" style="display:none" name="slug" class="slug form-contro" value="{{$slug}}">
                                            </div>&nbsp;&nbsp;&nbsp;
                                            <div class="display-inline-block mt-10 post-url-buttons">
                                                <a href="#" class="edit-url" style="display: inline;">Edit</a>
                                                <a href="javascript:void(0)" class="save-url" style="display:none"><i class="fa fa-check" style="font-size:10px;"></i></a>
                                                <span class="separator-url-form" style="display:none;opacity:0;">|</span>
                                                <a href="javascript:void(0)" class="cancel-edit-url" style="display:none"><i class="fa fa-ban" style="font-size:15px;"></i></a>
                                            </div>
                                        </div>
                                        @else
                                        <div class="col-sm-10">                                
                                            <div class="post-url pull-left"><?php echo url('category/'.$type) ?>/<span class="slug">{{ $slug }}</span>
                                                <input type="text" style="display:none" name="slug" class="slug form-control" value="{{ $slug }}">
                                            </div>&nbsp;&nbsp;&nbsp;
                                            <div class="display-inline-block mt-10 post-url-buttons">
                                                <a href="javascript:void(0)" class="edit-url-custom">Edit</a>
                                                <a href="javascript:void(0)" class="save-url-custom" style="display:none"><i class="fa fa-check" style="font-size:10px;"></i></a>
                                                <span class="separator-url-form" style="display:none;opacity:0;">|</span>
                                                <a href="javascript:void(0)" class="cancel-edit-url-custom" style="display:none"><i class="fa fa-ban" style="font-size:15px;"></i></a>
                                            </div>
                                        </div>
                                        @endif
                                        
                                    </div>

                                    <div class="form-group"><label class="col-sm-2 control-label">Parent name</label>
                                        <div class="col-sm-10">
                                            <select class="form-control" name="parent" style="width:200px;">
                                                <option value="0">None</option>
                                            @foreach($taxonomies as $taxo)
                                                <option <?php echo ($taxo->id == $parent) ? 'selected' : '' ?> value="{{ $taxo->id }}">{{ $taxo->name }}</option>
                                                @foreach($taxo->childs as $v)
                                                    <option <?php echo ($v->id == $parent) ? 'selected' : '' ?> value="{{ $v->id }}">&mdash; {{ $v->name }}</option>
                                                @endforeach
                                            @endforeach
                                        </select>
                                        </div>
                                    </div>

                                    <div class="form-group"><label class="col-sm-2 control-label">Description</label>
                                        <div class="col-sm-10"><textarea name="desc" class="form-control" id="desc" value="">{{$desc}}</textarea></div>
                                    </div>

                                    <div class="hr-line-dashed"></div>

                                    <div class="form-group"><label class="col-sm-2 control-label">SEO Title</label>
                                        <div class="col-sm-10"><input type="text" name="seo_title" class="form-control" placeholder="Customize your page title for SEO purpose" id="seo_title" value="{{$seo_title}}"></div>
                                    </div>

                                    <div class="form-group"><label class="col-sm-2 control-label">SEO Description</label>
                                        <div class="col-sm-10"><textarea name="seo_description" class="form-control" value="" id="seo_description" placeholder="Default site description being used on your website">{{$seo_description}}</textarea></div>
                                    </div>

                                    <div class="form-group"><label class="col-sm-2 control-label">Site Keywords</label>
                                        <div class="col-sm-10"><input type="text" name="seo_keyword" class="form-control" placeholder="Default SEO keywords being used on your website" id="seo_keyword" placeholder="" value="{{$seo_keyword}}"></div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-4 col-sm-offset-2">
                                            <button class="btn btn-white" type="submit">Cancel</button>
                                            <button class="btn btn-primary" type="submit">Save changes</button>
                                        </div>
                                    </div>

                                    {{csrf_field()}}
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            

            {!! view('arcana::admin.error-script',[
                    'errors'=>$errors,
                    'fields' => [
                        'name' => 'required',
                    ]
                ]); 
            !!}

            <script type="text/javascript" src="{{ asset('backend/assets/js/ckeditor/ckeditor.js') }}"></script>

            <?php $err = count($errors); ?>
            <!-- Slug -->
            <script type="text/javascript">
                $(document).ready(function(){
                    @if($id == 0)
                    $('[name=name]').slug({
                        slug: 'slug',
                        hide:false
                    });
                    @endif
                    CKEDITOR.replace("desc");
                });

                @if($id > 0)
                var currentSlug = "<?php echo $slug ?>";
                 $('.edit-url-custom').click(function(e){
                    e.preventDefault;
                    $('.post-url span').hide();
                    $('.post-url input').show();
                    $('.cancel-edit-url-custom, .save-url-custom, .separator-url-form').show();
                    $('.edit-url-custom').hide();
                });
                
                
                $('.save-url-custom').click(function(e){
                    e.preventDefault;
                    newurl = $('.post-url input').val();
                    currentSlug = newurl;
                    $.ajax({
                        url:"<?php echo route("arcana_admin_taxo_change_slug") ?>",
                        headers: {
                              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        data:{slug:currentSlug,id:"<?php echo $id ?>"},
                        type:"POST",
                        dataType:"json",
                        success:function(data){
                            $('.cancel-edit-url-custom, .save-url-custom, .separator-url-form').hide();
                            $('.post-url span').show();
                            $('.post-url input').hide();
                            $('.edit-url-custom').show();
                            currentSlug = data.slug;
                            $('.post-url span').html('').html(currentSlug);
                            $('.post-url input').val(currentSlug);;
                        }
                    });
                    
                });
                
                $('.cancel-edit-url-custom').click(function(e){
                    e.preventDefault;
                    $('.cancel-edit-url-custom, .save-url-custom, .separator-url-form').hide();
                    $('.post-url span').show();
                    $('.post-url input').hide();
                    $('.edit-url-custom').show()
                    $('.post-url input').val(currentSlug);;
                    
                });
                @endif
            </script>

            <script type="text/javascript">
                $(document).ready(function(){
                    var status = "{{session('msg')}}";
                    var err = "{{$err}}";
                    if (status === 'Data Saved!') {
                        swal({
                            title: "SUCCESS",
                            text: "All changes has been saved successfuly",
                            confirmButtonColor: "#66BB6A",
                            type: "success",
                            html: true
                            });
                    }
                })
            </script>

            <script type="text/javascript">
                $(document).ready(function(){
                    initPreventClose();
                })
            </script>

