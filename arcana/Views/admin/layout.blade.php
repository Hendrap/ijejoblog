<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ $title or 'Arcana CMS' }}</title>
    <link rel="shortcut icon" type="image/x-icon" href="{{asset ('cms/img/arcana/favicon.ico') }}" />
    <link href="{{ asset('cms/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('cms/font-awesome/css/font-awesome.css') }}" rel="stylesheet">
    <!-- icon font code -->
    <link href="{{ asset('backend/assets/css/icons/icomoon/styles.css')}}" rel="stylesheet" type="text/css">
    <link href="{{ asset('backend/assets/css/core.css')}}" rel="stylesheet" type="text/css">

    <link href="{{ asset('cms/css/plugins/iCheck/custom.css')}}" rel="stylesheet">

    <!-- Toastr style -->
    <link href="{{ asset('cms/css/plugins/toastr/toastr.min.css') }}" rel="stylesheet">

    <!-- Gritter -->
    <link href="{{ asset('cms/js/plugins/gritter/jquery.gritter.css') }}" rel="stylesheet">

    <!-- FooTable -->
    <link href="{{ asset('cms/css/plugins/footable/footable.core.css') }}" rel="stylesheet">

    <link href="{{ asset('cms/css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('cms/css/style.css') }}" rel="stylesheet">

    <link href="{{ asset('backend/assets/css/components.css')}}" rel="stylesheet" type="text/css">
    <link href="{{ asset('cms/css/arcana.css') }}" rel="stylesheet">
    
    <!-- Mainly scripts -->
    <script src="{{ asset('cms/js/jquery-2.1.1.js') }} "></script>
    <script src="{{ asset('cms/js/bootstrap.min.js') }} "></script>
    <script src="{{ asset('cms/js/plugins/metisMenu/jquery.metisMenu.js') }} "></script>
    <script src="{{ asset('cms/js/plugins/slimscroll/jquery.slimscroll.min.js') }} "></script>

    <!-- Additional Alpha -->
    <script type="text/javascript" src="{{ asset('backend/assets/js/plugins/tables/datatables/datatables.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('backend/assets/js/plugins/tables/datatables/extensions/responsive.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('backend/assets/js/pages/datatables_basic.js')}}"></script>
    <script type="text/javascript" src="{{ asset('backend/assets/js/pages/datatables_responsive.js')}}"></script>
    <script type="text/javascript" src="{{ asset('backend/assets/js/plugins/loaders/blockui.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('backend/assets/js/plugins/forms/validation/validate.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('backend/assets/js/plugins/notifications/sweet_alert.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('backend/assets/js/plugins/forms/selects/select2.min.js')}}"></script>
    
    <script type="text/javascript" src="{{ asset('backend/assets/js/plugins/ui/moment/moment.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('backend/assets/js/plugins/pickers/daterangepicker.js')}}"></script>
    <script type="text/javascript" src="{{ asset('backend/assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('backend/assets/js/plugins/forms/selects/bootstrap_multiselect.js')}}"></script>
    <script type="text/javascript" src="{{ asset('backend/assets/js/plugins/forms/tags/tagsinput.min.js')}}"></script>

    
    <script type="text/javascript" src="{{ asset('backend/assets/js/core/app.js')}}"></script> 
    <script type="text/javascript" src="{{ asset('backend/assets/js/core/custom.js')}}"></script>
    <script type="text/javascript" src="{{ asset('backend/assets/js/pages/form_select2.js')}}"></script>
    <script type="text/javascript" src="{{ asset('backend/assets/js/plugins/sticky-kit/jquery.sticky-klt.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('backend/assets/js/jquery.slug.js') }}"></script>
    <script type="text/javascript" src="{{ asset('backend/assets/js/validate.min.js') }}"></script>
    
    <!-- Vue -->
    <script src="{{ asset('cms/js/framework/vue2/vue.js') }} "></script>
    <script src="{{ asset('cms/js/framework/vue-resource/vue-resource-1.5.0.js') }} "></script>

    <script type="text/javascript">
        $(document).ready(function(){
            
            $('#search_the_table').attr('placeholder', 'Type to filter...');
            $('.breadcrumb-line').parent().remove();            
        });
    </script>

    <script type="text/javascript">
      //ini di ppakai buat alpha media image
      var editorName = '';
      var mediaSite = "<?php echo route('arcana_admin_media_entry')."?editor=true&page=1" ?>";
        $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
        });
          
        $(document).ready(function(){
            $('#search_the_table').width('100%');
            $('#search_the_table').attr('placeholder', 'Type to filter...');
            $('.breadcrumb-line').parent().remove();            
        });
        $(window).load(function() {
              $('td.text-bold').addClass('title-column').removeClass('text-bold');
             $('.dataTables_filter input').attr('placeholder', 'Type to filter...');
        });
    </script>

</head>

<body>
    <div id="wrapper">
    
    @include('arcana::admin.partials.sidebar')

        <div id="page-wrapper" class="gray-bg dashbard-1">
            <div class="row border-bottom">

                @include('arcana::admin.partials.top')
            
            </div>

            <!-- Content -->
            <?php
                if(!empty($content)){
                    echo $content;
                }else{
                    //echo view('alpha::admin.empty');
                }
            ?>
            
            <div class="footer">
                <div class="pull-right">
                    Design by Inspinia v2.5.
                </div>
                <div>
                    <strong>Copyright</strong> Arcana Project &copy; 2018
                </div>
            </div>

        </div>
    </div>


    <!-- FooTable -->
    <script src="{{ asset('cms/js/plugins/footable/footable.all.min.js') }}"></script>
    
    <!-- Flot -->
    <script src="{{ asset('cms/js/plugins/flot/jquery.flot.js') }} "></script>
    <script src="{{ asset('cms/js/plugins/flot/jquery.flot.tooltip.min.js') }} "></script>
    <script src="{{ asset('cms/js/plugins/flot/jquery.flot.spline.js') }} "></script>
    <script src="{{ asset('cms/js/plugins/flot/jquery.flot.resize.js') }} "></script>
    <script src="{{ asset('cms/js/plugins/flot/jquery.flot.pie.js') }} "></script>

    <!-- Peity -->
    <script src="{{ asset('cms/js/plugins/peity/jquery.peity.min.js') }} "></script>
    <script src="{{ asset('cms/js/demo/peity-demo.js') }} "></script>

    <!-- Custom and plugin javascript -->
    <script src="{{ asset('cms/js/inspinia.js') }} "></script>
    <script src="{{ asset('cms/js/plugins/pace/pace.min.js') }} "></script>

    <!-- jQuery UI -->
    <script src="{{ asset('cms/js/plugins/jquery-ui/jquery-ui.min.js') }} "></script>

    <!-- GITTER -->
    <script src="{{ asset('cms/js/plugins/gritter/jquery.gritter.min.js') }} "></script>

    <!-- Sparkline -->
    <script src="{{ asset('cms/js/plugins/sparkline/jquery.sparkline.min.js') }} "></script>

    <!-- Sparkline demo data  -->
    <script src="{{ asset('cms/js/demo/sparkline-demo.js') }} "></script>

    <!-- ChartJS-->
    <script src="{{ asset('cms/js/plugins/chartJs/Chart.min.js') }} "></script>

    <!-- Toastr -->
    <script src="{{ asset('cms/js/plugins/toastr/toastr.min.js') }} "></script>

    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function() {

            $('.footable').footable();

        });

    </script>


</body>

<?php
        $pathMedia =  strtolower(Request::path());

        if(!Illuminate\Support\Str::startsWith($pathMedia,'admin/media')){
            echo view('arcana::admin.media.modal.entry-media');
        }
    ?>

</html>
