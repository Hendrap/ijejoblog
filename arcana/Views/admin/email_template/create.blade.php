            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Templates - New Template</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a>Templates</a>
                        </li>
                        <li class="active">
                            <strong>Email Templates</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>

            <div class="wrapper wrapper-content animated fadeInRight">

                <form name="arcanaForm" enctype="multipart/form-data" method="POST" action="{{ route('arcana_admin_entry_save',[$type,0]) }}" class="form-horizontal">
                    
                <div class="row">
                    <div class="col-lg-9">
                        <div class="ibox float-e-margins">
                            @if(count(Config::get('arcana.application.locales')) > 1)
                            <div class="pull-right">
                                <select width="200" id="changeLang" class="select-language">
                                    @foreach(Config::get('arcana.application.locales') as $key  => $lang)
                                        <option value="{{ $key }}">{{ $lang }}</option>
                                    @endforeach
                                </select>
                            </div>
                            @endif
                            <div class="ibox-title">
                                <h3>
                                    Main Content
                                </h3>
                                @if(count($errors) > 0)
                                @foreach($errors->all() as $key => $error)
                                @if($key == 0)
                                <div class="alert bg-danger alert-styled-left">
                                    <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
                                    @if(Illuminate\Support\Str::contains($error,'required'))
                                        {{$error}}
                                        <span class="text-semibold">Required field(s) error or missing</span>
                                        
                                    @else
                                        <span class="text-semibold">{{ str_replace('metas.','',$error) }}</span>
                                    @endif
                                </div>
                                @endif
                                @endforeach
                                @endif
                            </div>
                            <div class="ibox-content">
                                <p class="content-group-lg">This is your template editor, you can edit existing template or make a new one using available master template.<br>Use ”Bracket Code” to replace certain words based on the conditions of what, when and to whom your template will be sent.</p>

                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" name="published" value="{{ date('Y-m-d H:i:s') }}">
                                <input type="hidden" name="status" value="published"> 

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Template Title</label>
                                    <div class="col-sm-10">
                                        @foreach(Config::get('arcana.application.locales') as $key  => $lang)
                                            <input style="display:none" id="title_{{$key}}" name="title[{{$key}}]" type="text" class="form-control" value="{{old('title.'.$key)}}">
                                        @endforeach
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Subject</label>
                                    <div class="col-sm-10">
                                        <input id="metas[subject]" name="metas[subject]" type="text" class="form-control" value="{{ old('metas.subject') }}">
                                    </div>
                                </div>

                                <div class="form-group @if(count($errors->get('content.'.$key))) invalid-input @endif">
                                        <div class="col-sm-2"></div>
                                        @foreach(Config::get('arcana.application.locales') as $key  => $lang)
                                        <div class="col-sm-10">
                                           <textarea style="display:none" id="content_{{$key}}" name="content[{{$key}}]" rows="10"  class="text-editor" cols="80" >{{old('content.'.$key)}}</textarea>
                                        </div>
                                        @endforeach
                                </div>

                                <div class="hr-line-dashed"></div>

                                <div class="form-group">
                                    <label class="control-label col-lg-2"></label>
                                    <div class="col-lg-10">
                                        @foreach(Config::get('arcana.application.locales') as $key  => $lang)
                                            <textarea style="display:none" id="excerpt_{{$key}}" name="excerpt[{{$key}}]" class="form-control">{{old('excerpt.'.$key)}}</textarea>
                                        @endforeach
                                    </div>
                                </div>
                                                        
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary pull-right ml-10">Save</button>
                                    <a href="{{route('arcana_admin_entry_index',[$type])}}" class="btn btn-white pull-right">Cancel</a>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        
                        <!-- Publish -->
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h3>
                                    Bracket Code
                                </h3>
                            </div>
                            <table class="table-bracket-code">
                        
                               <tr>
                                    <td class="pr-10"><a href="#" class="add-code">[name]</a></td>
                                    <td>Customer's Name</td>
                               </tr>
                               <tr>
                                    <td class="pr-10"><a href="#" class="add-code">[email]</a></td>
                                    <td>Customer's Email</td>
                               </tr>
                               <tr>
                                    <td class="pr-10"><a href="#" class="add-code">[address]</a></td>
                                    <td>Customer's Address</td>
                               </tr>
                               <tr>
                                    <td class="pr-10"><a href="#" class="add-code">[shipto]</a></td>
                                    <td>Customer's Shipping Address</td>
                               </tr>
                               <tr>
                                    <td class="pr-10"><a href="#" class="add-code">[invoicenum]</a></td>
                                    <td>Invoice Number</td>
                               </tr>
                               <tr>
                                    <td class="pr-10"><a href="#" class="add-code">[invoice]</a></td>
                                    <td>Invoice Content</td>
                               </tr>
                               <tr>
                                    <td class="pr-10"><a href="#" class="add-code">[ordernum]</a></td>
                                    <td>Order Number</td>
                               </tr>
                               <tr>
                                    <td class="pr-10"><a href="#" class="add-code">[order]</a></td>
                                    <td>Order List</td>
                               </tr>
                                <!-- dst-->
                           </table>
                        </div>
                    </div>
                </div>

                {{csrf_field()}}
                </form>
            </div>
            {!! view('arcana::admin.error-script',[
                    'errors'=>$errors,
                    'fields' => [
                        'name' => 'required',
                    ]
                ]); 
            !!}
            <script type="text/javascript" src="{{ asset('backend/assets/js/ckeditor/ckeditor.js') }}"></script>

            <?php $err = count($errors); ?>
            <!-- Slug -->
            <script type="text/javascript">
                
                // MultiLang configurations Here
                function hideAllTitle(){
                    @foreach(Config::get('arcana.application.locales') as $key  => $lang)
                        $("#title_" + "{{ $key }}").hide();
                        $(".multilangmetas_" + "{{ $key }}").hide();
                    @endforeach
                }
                function hideAllEditors(){
                    @foreach(Config::get('arcana.application.locales') as $key  => $lang)
                        $("#cke_content_" + "{{ $key }}").hide();
                        $("#excerpt_" + "{{ $key }}").hide();
                    @endforeach
                }
                function showDefaultEditor(){
                    $("#cke_content_" + defaultLang).show();
                    $("#excerpt_" + defaultLang).show();
                }

                var defaultTitle = "{{ Config::get('arcana.application.default_locale') }}";
                var defaultLang = "{{ Config::get('arcana.application.default_locale') }}";
                @foreach(Config::get('arcana.application.locales') as $key  => $lang)
                        CKEDITOR.replace("content_" + "{{ $key }}",{height: '400px'});
                @endforeach
                CKEDITOR.on("instanceReady", function(event)
                {
                    hideAllEditors();
                    showDefaultEditor();
                });

                $("#changeLang").change(function(e){
                    hideAllEditors();
                    hideAllTitle();
                    defaultLang = $("#changeLang option:selected").val();
                    $("#title_" + defaultLang).show();
                    $(".multilangmetas_" + defaultLang).show();
                    showDefaultEditor();

                });

                $("#title_" + defaultLang).show();
                $(".multilangmetas_" + defaultLang).show();

                $("#title_" + defaultTitle).slug({
                    slug: 'slug',
                    hide:false
                });

                // End of MultiLang Config

            </script>

