
 @if(session('msg'))

    <div class="alert bg-success alert-styled-left">

        <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>

        <span class="text-semibold">{{session('msg')}}</span>

    </div>

@endif

            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10 col-sm-10 col-xs-12">
                    <h2>ENTRY - {{ $info['plural'] }}</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a>Entry</a>
                        </li>
                        <li class="active">
                            <strong>{{ $info['plural'] }}</strong>
                        </li>
                    </ol>
                    <div class="visible-xs">
                            <a href="{{ route('arcana_admin_entry_create',[$type]) }}" class="btn btn-primary"><b><i class="fa fa-file-o"></i></b> New {{ $info['plural'] }}</a> 
                    </div>
                </div>
                <div class="col-lg-2 col-sm-2 col-xs-hidden">

                    <div class="heading-elements">

                        <a href="{{ route('arcana_admin_entry_create',[$type]) }}" class="btn btn-primary"><b><i class="fa fa-file-o"></i></b> New {{ $info['plural'] }}</a>

                    </div>
                </div>
            </div>

        <div class="wrapper wrapper-content animated fadeInRight ecommerce" id="entry">


            <div class="ibox-content m-b-sm border-bottom">
                <div class="row">
                    <div class="col-sm-12">
                        <p>Preformatted HTML templates to be used in email deliveries or other documents.</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-2">
                        <div class="filter-table pull-left">
                            <div class="form-group pull-left has-feedback">
                                <input type="text" class="form-control" id="search_the_table" style="height:36px;">
                                <div class="form-control-feedback" style="line-height:36px;">
                                        <i class="icon-search4" style="font-size:12px"></i>
                                </div>
                            </div>
                            
                        </div>
                    </div>


                    <div class="col-sm-2">
                        <div class="form-group">
                            <select class="table-select2 with-search pull-left filter-table-column" placeholder="Filter by Category" searchin="7">
                                <option value="all">All Categories</option>
                                @foreach($taxoInfo->taxo as $key => $taxo)
                                <optgroup label="{{$taxo['plural']}}">
                                    @foreach($taxonomies as $v)
                                        @if($v->taxonomy_type == $taxo['slug'])
                                             <option value="{{$v->id}}">{{$v->name}}</option>
                                        @endif
                                    @endforeach
                                </optgroup>
                                @endforeach
                                
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-2">
                        <div class="form-group">
                            <select class="table-select2 pull-left filter-table-column" placeholder="Filter By Status">
                                <option data-col="4" value="0">All Status</option>
                                <option data-col="4" value="published">Published</option>
                                <option data-col="4" value="disabled">Disabled</option>
                                <option data-col="4" value="scheduled_publish">Scheduled</option>
                            </select>
                        </div>
                    </div>
                </div>

            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">
                        <div class="ibox-content">

                            <table class="table entry-table table-stripped">
                                <thead>
                                <tr>
                                    <th width="50px" class="sorter no-sort">&nbsp;</th>
                                    <th>Page</th>
                                    <th width="160px">Creator</th>
                                    <th width="140px" class="hide"><span>Published</span></th>
                                    <th width="140px"><span>Created</span></th>
                                    <th width="140px"><span>Modified</span></th>
                                    <th width="120px">Status</th>
                                    <th width="50px" class="no-sort text-right"></th>
                                    <th width="50px" class="no-sort text-right hide">Category</th>

                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>

                            <form style="display:none" id="form_delete" action="{{route('arcana_admin_entry_delete',[0])}}" method="POST">
                                {{csrf_field()}}
                                <input type="hidden" name="id" value="0" >
                            </form>


                        </div>
                    </div>
                </div>
            </div>

        </div>

        <script type="text/javascript">
            var entryGetDataUrl = "{{ route('arcana_admin_entry_get_data',[$type]) }}";
            var urlSaveSequence = "{{ route('arcana_admin_save_sequence',[$type]) }}";
            var entryType = "<?php echo $type ?>";
        </script>
        
        <script src="{{ asset('cms/js/entry-table.js') }} "></script> 

        <script type="text/javascript">

            $(document).on('click',".confirm",function(e){
                e.preventDefault();
                    $("#form_delete").attr('action',$("#form_delete").attr('action').replace('/' + 0,'/' + $(this).data('id')));
                    $("#form_delete").find("[name=id]").val($(this).data('id'));

                swal({
                    title: "Delete ?",
                    text: "You will not be able to recover deleted items, are you sure want to delete ?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: false },
                    function(isConfirm){
                    if(isConfirm){
                        $("#form_delete").submit();
                    }

                });

            });

        </script>