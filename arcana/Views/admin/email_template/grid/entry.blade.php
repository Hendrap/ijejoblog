                                    <td class="sorter">
                                        <i data-after="<?php echo (int)$entry->after ?>" data-before="<?php echo (int)$entry->before ?>" data-entry-id="<?php echo $entry->id ?>" class="icon-menu-open entry-data-container"></i>
                                    </td>
                                    |arcana--datatable-separator--|
                                    <td>
                                        <a href="{{ route('arcana_admin_entry_edit',[$type,$entry->id]) }}"><strong>{{parseMultiLang($entry->title)}}</strong></a>
                                        @if(!empty($entry->parent))
                                            <br><i class="text-info">Parent : {{parseMultiLang($entry->parent->title)}}</i>
                                        @endif
                                    </td>
                                    |arcana--datatable-separator--|
                                    <td> 
                                        @if($entry->user) 
                                        <strong>{{$entry->user->username}}</strong>
                                        <br><span class="text-muted">{{ $entry->user->roles[0]->name }}</span>
                                        @endif
                                    </td>
                                    |arcana--datatable-separator--|
                                    <td>
                                        {{date_format(date_create($entry->published_at),app('ArcanaSetting')->getSetting('date_format'))}}
                                        </br><span class="text-muted">{{date_format(date_create($entry->published_at),app('ArcanaSetting')->getSetting('time_format'))}}</span>
                                    </td> 
                                    |arcana--datatable-separator--|                                    
                                    <td>
                                        {{date_format(date_create($entry->created_at),app('ArcanaSetting')->getSetting('date_format'))}}
                                        </br><span class="text-muted">{{date_format(date_create($entry->created_at),app('ArcanaSetting')->getSetting('time_format'))}}</span>
                                    </td>
                                    |arcana--datatable-separator--|                                    
                                    <td>
                                        {{date_format(date_create($entry->updated_at),app('ArcanaSetting')->getSetting('date_format'))}}
                                        </br><span class="text-muted">{{date_format(date_create($entry->updated_at),app('ArcanaSetting')->getSetting('time_format'))}}</span>
                                    </td>  
                                    |arcana--datatable-separator--|
                                    <td>
                                        <?php 
                                        $color = 'label-danger';
                                        if($entry->status =='published' && strtotime($entry->published_at) <= time())
                                            $color = 'label-primary';
                                        elseif($entry->status == 'published' && strtotime($entry->published_at) > time())
                                            $color = 'badge-info';
                                        elseif($entry->status =='disabled')
                                            $color = 'bg-grey';
                                        elseif($entry->status =='draft')
                                            $color = 'label-warning';
                                        else
                                        ?>
                                        <span class="label {{$color}}">{{ ucfirst($entry->status) }}</span>
                                    </td>  
                                    |arcana--datatable-separator--|
                                    <td class="text-right">
                                        <div class="btn-group">
                                            <button data-toggle="dropdown" class="btn btn-default dropdown-toggle" aria-expanded="true"><i class="fa fa-bars"></i> <span class="caret"></span></button>
                                            <ul class="dropdown-menu dropdown-menu-right">

                                                <li><a href="{{ route('arcana_admin_entry_edit',[$type,$entry->id]) }}"><i class="fa fa-edit"></i> Edit</a></li>
                                                @if($entry->status != 'disabled' && $entry->status  != 'published')
                                                <li><a href="{{ route('arcana_admin_entry_status',[$entry->id,'disabled']) }}"><i class="icon-cancel-square"></i> Disable</a></li>
                                                @endif
                                                @if($entry->status != 'published')
                                                <li><a href="{{ route('arcana_admin_entry_status',[$entry->id,'published']) }}"><i class="icon-checkmark4"></i> Publish</a></li>
                                                @endif
                                                @if($entry->status != 'draft')
                                                <li><a href="{{ route('arcana_admin_entry_status',[$entry->id,'draft']) }}"><i class="icon-clipboard2"></i> Draft</a></li>
                                                @endif
                                                <li class="divider"></li>
                                                <li><a href="#" class="confirm" data-id="{{$entry->id}}"><i class="fa fa-trash-o"></i> Delete</a></li>

                                            </ul>
                                        </div>
                                    </td>
                                    |arcana--datatable-separator--|
                                    <td>Category</td>
