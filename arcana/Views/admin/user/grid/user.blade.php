                                    <td>
                                        <strong><a href="{{ route('arcana_admin_edit_account',[$user->id]) }}">{{ generateName($user) }}</a></strong>
                                        </br><span class="text-muted">{{$user->email}}</span>
                                    </td>
                                    |arcana--datatable-separator--|
                                    <td>
                                        {{@$user->roles[0]->name}}
                                    </td>
                                    |arcana--datatable-separator--|
                                    <td>
                                        {{ date(app('ArcanaSetting')->getSetting('date_format'),strtotime($user->created_at)) }}</br><span class="text-muted">{{ date(app('ArcanaSetting')->getSetting('time_format'),strtotime($user->created_at)) }}</span>
                                    </td>
                                    |arcana--datatable-separator--|
                                    <td>
                                        @if($user->last_login == '1970-01-01 00:00:00' || empty($user->last_login))
                                            Never
                                        @else
                                            {{ date(app('ArcanaSetting')->getSetting('date_format'),strtotime($user->last_login)) }}
                                             <br><span class="text-muted">{{ date(app('ArcanaSetting')->getSetting('time_format'),strtotime($user->last_login)) }}</span>
                                        @endif
                                    </td>
                                    |arcana--datatable-separator--|
                                    <td>
                                        <?php
                                        $className ='label-danger';
                                        switch ($user->status) {
                                            case 'active':
                                                $className = 'label-primary';
                                                break;
                                            case 'disabled':
                                                $className = 'bg-grey';
                                                break;
                                            case 'pending':
                                                $className = 'label-warning';
                                                break;                                            
                                        }
                                        ?>
                                        <span class="label {{ $className }}">{{ ucfirst($user->status) }}</span>
                                    </td>  
                                    |arcana--datatable-separator--|
                                    <td>
                                        <div class="btn-group .disable-dropup">
                                            <button data-toggle="dropdown" class="btn btn-default dropdown-toggle" aria-expanded="true"><i class="fa fa-bars"></i> <span class="caret"></span></button>
                                            <ul class="dropdown-menu dropdown-menu-right">

                                                <li>
                                                    <a href="{{ route('arcana_admin_edit_account',[$user->id]) }}">
                                                    <i class="fa fa-edit"></i> Edit</a>
                                                </li>

                                                <?php if($user->status != 'pending'){ ?>
                                                <li>
                                                    <a href="{{ route('arcana_admin_user_change_status',[$user->id,'pending']) }}">
                                                    <i class="fa fa-clock-o"></i> Pending </a>
                                                </li>
                                                <?php } ?>


                                                <?php if($user->status != 'disabled'){ ?>
                                                <li>
                                                    <a href="{{ route('arcana_admin_user_change_status',[$user->id,'disabled']) }}">
                                                    <i class="fa fa-times-circle-o"></i> Disable </a>
                                                </li>
                                                <?php } ?>

                                                <?php if($user->status != 'active'){ ?>
                                                <li>
                                                    <a href="{{ route('arcana_admin_user_change_status',[$user->id,'active']) }}">
                                                    <i class="fa fa-check-circle-o"></i> Enable </a>
                                                </li>
                                                <?php } ?>

                                                <li class="divider"></li>
                                                <li>
                                                    <a class="confirm" data-id="{{$user->id}}"  href="{{ route('accounts.destroy',[$user->id]) }}?_method=delete">
                                                    <i class="fa fa-trash-o"></i> Delete</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </td>