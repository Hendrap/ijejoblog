
<?php
    $email = old('email');
    $userStatus = old('status');
    $userRole = old('role',8);
    $oldUserMeta = old('metas');
    $userMetas = array();
    $avatar = asset('backend/assets/images/avatar-default.jpg');
    if(!empty($user)){
        $email = old('email',$user->email);
        $userStatus = old('status',$user->status);
        $userRole = old('role',@$user->roles[0]->id);
        $oldUserMeta = old('metas',$userMetas);
        if (empty($oldUserMeta) && isset($dataMetas)) {
            $oldUserMeta = $dataMetas;
            if (!empty($dataMetas['avatar'])) {
                $avatar = asset(getCropImage($dataMetas['avatar'],'default'));
            }
        }
    }else{
        $user = new StdClass();
        $user->id = 0;
    }


?>
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Accounts - {{ ($user->id == 0) ? 'New' : 'Edit' }} Accounts</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a>Home</a>
                        </li>
                        <li>
                            <a>Accounts</a>
                        </li>
                        <li class="active">
                            <strong>{{ ($user->id == 0) ? 'New' : 'Edit' }} Account</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>

            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h3>
                                    @if($user->id == 0)
                                        New Account
                                    @else
                                        {{ generateName($user) }}
                                    @endif
                                </h3>
                                @if($user->id != 0)
                                <p class="content-group-lg">Created on {{date_format(date_create($user->created_at),app('ArcanaSetting')->getSetting('date_format'))}} at {{date_format(date_create($user->created_at),app('ArcanaSetting')->getSetting('time_format'))}} by {{ generateName($user->modifiedBy) }}, last modified on {{date_format(date_create($user->updated_at),app('ArcanaSetting')->getSetting('date_format'))}} at {{date_format(date_create($user->updated_at),app('ArcanaSetting')->getSetting('time_format'))}}</p>
                                @endif

                                @if(count($errors) > 0)
                                @foreach($errors->all() as $key => $error)
                                @if($key == 0)
                                <div class="alert bg-danger alert-styled-left">
                                    <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
                                    @if(Illuminate\Support\Str::contains($error,'required'))
                                        <span class="text-semibold">Required field(s) error or missing</span>
                                    @else
                                        <span class="text-semibold">{{ str_replace('metas.','',$error) }}</span>
                                    @endif
                                </div>
                                @endif
                                @endforeach
                                @endif
                            </div>
                            <div class="ibox-content">

                                <form name="arcanaForm" enctype="multipart/form-data" method="POST" action="{{route('accounts.store')}}" class="form-horizontal">

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <div class="clearfix">
                                                    <label for="upload_avatar" class="change-avatar">
                                                    <img onerror="this.src='{{ asset('backend/assets/images/avatar-resized.jpg') }}'" src="{{$avatar}}" id="image-preview" alt="avatar" class="user-avatar display-inline-block valign-bottom">
                                                        <div class="overlay"><span class="btn btn-rounded btn-primary"><i class="fa fa-camera"></i></span></div>
                                                    </label>
                                                    
                                                     <input name="avatar" type="file" id="upload_avatar" class="hide">
                                                </div>    
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">

                                        <div class="col-sm-6 col-xs-12">
                                            <!-- Primary Data -->
                                            <div class="form-group">
                                                <label class="col-sm-12">Name</label>
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <input id="metas_first_name" type="text" class="form-control" placeholder="First Name" value="{{@$oldUserMeta['first_name']}}" name="metas[first_name]">
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <input id="metas_last_name" type="text" class="form-control" placeholder="Last Name" name="metas[last_name]" value="{{@$oldUserMeta['last_name']}}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-12">Email</label>
                                                <input id="email" type="text" class="form-control" placeholder="mail@domain.com" name="email" value="{{$email}}">
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-12">Password</label>
                                                <div class="row password-form">
                                                    <div class="col-sm-6">
                                                        <input id="password" type="password" class="form-control password" placeholder="Type Password" name="password">
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <input id="password_confirmation" type="password" class="form-control pas repassword" placeholder="Re-Type Password" name="password_confirmation">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="checkbox generate-password">
                                                <label>
                                                        <input type="checkbox" class="styled checkbox-password generate-password">
                                                        Generate Password
                                                </label>
                                            </div>
                                        </div>

                                        <div class="col-sm-4 col-xs-12">

                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <label>Role</label>
                                                        <select class="form-control" name="role" placeholder="Select Role">
                                                            @foreach($roles as $role)
                                                                <option <?php echo ($role->id == $userRole) ? 'selected' : '' ?>  value="{{$role->id}}">{{$role->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <label>Status</label>
                                                        <select class="form-control" {{ $user->id == 0  ? "disabled" : "" }} name="status" placeholder="Day" width="110">

                                                            <?php
                                                                $status = ['pending','active','disabled'];
                                                                if($user->id != 0) $status[] = 'deleted';
                                                             ?>
                                                            @foreach($status as $v)
                                                                <option <?php echo ($v == $userStatus) ? 'selected' : '' ?> value="{{$v}}">{{ucfirst($v)}}</option>
                                                            @endforeach
                                                        </select>
                                                        @if($user->id == 0)
                                                            <input type="hidden" name="status" value="pending">
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">

                                                <label>Date of Birth</label>
                                                <div class="row">
                                                    <div class="col-sm-3">
                                                        <select id="metas_day" name="metas[day]" class="form-control" placeholder="Day">
                                                            <option value="">Date</option>
                                                            @for($i=1; $i<=31; $i++)
                                                                <option <?php echo ($i == @$oldUserMeta['day']) ? 'selected' : '' ?>  value="{{$i}}">{{$i}}</option>
                                                            @endfor
                                                        </select>    
                                                    </div>
                                                    <div class="col-sm-5">
                                                        <select id="metas_month" name="metas[month]" class="form-control" placeholder="Month">
                                                         <option value="">Month</option>
                                                            @for($i=1; $i<=12; $i++)
                                                                <option <?php echo ($i == @$oldUserMeta['month']) ? 'selected' : '' ?>  value="{{$i}}">{{date('F',strtotime('2000-'.$i.'-01'))}}</option>
                                                            @endfor
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-4">
                                                    <select id="metas_year" name="metas[year]" class="form-control" placeholder="Year">
                                                        <option value="">Year</option>
                                                        @for($i=1980; $i<=date('Y'); $i++)
                                                            <option <?php echo ($i == @$oldUserMeta['year']) ? 'selected' : '' ?>  value="{{$i}}">{{$i}}</option>
                                                        @endfor
                                                    </select>
                                                </div>
                                                </div>

                                            </div>

                                            <div class="form-group">
                                                <label>Gender</label>
                                                <div class="row">
                                                    <label class="radio-inline">
                                                        <div class="i-checks"><label> <input type="radio" value="Male" name="metas[gender]" @if(@$oldUserMeta['gender'] == 'Male') checked="checked" @endif > <i></i> Male </label></div>
                                                    </label>
                                                    <label class="radio-inline">
                                                        <div class="i-checks"><label> <input type="radio" value="Female" name="metas[gender]"  @if(@$oldUserMeta['gender'] == 'Female') checked="checked" @endif > <i></i> Female </label></div>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col-sm-2 col-sm-offset-10 col-xs-12">
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <button class="btn btn-primary pull-right" type="submit" style="margin-left: 10px;">Save</button>
                                                    <a href="{{route('accounts.index')}}" class="btn btn-white pull-right">Cancel </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                        <!-- Button -->
                                    
                                    <input type="hidden" name="id" value="{{@$user->id}}">
                                    {{csrf_field()}}
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {!! view('arcana::admin.error-script',[
                    'errors'=>$errors,
                    'fields' => [
                        'name' => 'required',
                    ]
                ]); 
            !!}

            <style type="text/css">
                .form-horizontal .form-group {
                    margin: 0;
                    margin-bottom: 15px;
                }
            </style>
            <script type="text/javascript" src="{{ asset('backend/assets/js/ckeditor/ckeditor.js') }}"></script>

            <?php $err = count($errors); ?>

            <!-- Msg -->

            <!-- iCheck -->
            <script src="{{asset('cms/js/plugins/iCheck/icheck.min.js')}}"></script>
            <script>
                $(document).ready(function () {
                    $('.i-checks').iCheck({
                        checkboxClass: 'icheckbox_square-green',
                        radioClass: 'iradio_square-green',
                    });
                });
            </script>


            <script type="text/javascript">
                var status = "{{session('msg')}}";
                var err = "{{$err}}";
                
                if (status === 'Data Saved!') {
                    swal({
                        title: "SUCCESS",
                        text: "All changes has been saved successfuly",
                        confirmButtonColor: "#66BB6A",
                        type: "success"
                        });
                }
            </script>

            <script type="text/javascript">
                $(document).ready(function(){
                    initPreventClose();
                })
            </script>


