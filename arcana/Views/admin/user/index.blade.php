
 @if(session('msg'))

    <div class="alert bg-success alert-styled-left">

        <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>

        <span class="text-semibold">{{session('msg')}}</span>

    </div>

@endif

            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10 col-sm-10 col-xs-12">
                    <h2>Accounts - All Accounts</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a>Accounts</a>
                        </li>
                        <li class="active">
                            <strong>All Accounts</strong>
                        </li>
                    </ol>
                    <div class="visible-xs">
                            <a href="{{route('arcana_admin_new_account')}}" class="btn btn-primary"><b><i class="fa fa-file-o"></i></b> New Account</a> 
                    </div>
                </div>
                <div class="col-lg-2 col-sm-2 col-xs-hidden">

                    <div class="heading-elements">

                        <a href="{{route('arcana_admin_new_account')}}" class="btn btn-primary"><b><i class="fa fa-file-o"></i></b> New Account</a>

                    </div>
                </div>
            </div>

        <div class="wrapper wrapper-content animated fadeInRight ecommerce" id="user">


            <div class="ibox-content m-b-sm border-bottom">
                <div class="row">
                    <div class="col-sm-2">
                        <div class="filter-table pull-left">
                            <div class="form-group pull-left has-feedback">
                                <input type="text" class="form-control" id="search_the_table" style="height:36px;">
                                <div class="form-control-feedback" style="line-height:36px;">
                                        <i class="icon-search4" style="font-size:12px"></i>
                                </div>
                            </div>
                            
                        </div>
                    </div>

                    <div class="col-sm-2">
                        <div class="form-group">
                            <select class="table-select2 pull-left filter-table-column" placeholder="Filter By Role">
                                <option data-col="1" value="0">All Roles</option>
                                @foreach($roles as $role)
                                <option data-col="1" value="{{$role->id}}">{{$role->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    
                    <div class="col-sm-2">
                        <div class="form-group">
                            <select class="table-select2 pull-left filter-table-column" placeholder="Filter By Status">
                                <option data-col="4" value="0">All Status</option>
                                <option data-col="4" value="active">Active</option>
                                <option data-col="4" value="pending">Pending</option>
                                <option data-col="4" value="disabled">Disabled</option>
                            </select>
                        </div>
                    </div>
                </div>

            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">
                        <div class="ibox-content">

                            <table class="table entry-table table-stripped">
                                <thead>
                                <tr>

                                    <th data-toggle="true">Name</th>
                                    <th data-hide="phone,tablet">Role</th>
                                    <th data-hide="phone,tablet">Created</th>
                                    <th data-hide="phone">Last Login</th>
                                    <th data-hide="">Status</th>
                                    <th class="no-sort"></th>

                                </tr>
                                </thead>
                                <tbody>
                                
                                </tbody>
                            </table>

                            <form style="display:none" id="form_delete" action="{{route('accounts.destroy',[0])}}" method="POST">
                                {{csrf_field()}}
                                <input type="hidden" name="id" value="0">
                                {{ method_field('DELETE') }}
                            </form>


                        </div>
                    </div>
                </div>
            </div>

        </div>
        <script type="text/javascript">
            var userGetData = "{{route('arcana_admin_user_get_data')}}";
        </script>

        <script src="{{ asset('cms/js/user-table.js') }} "></script> 

        <script type="text/javascript">            

            $(document).on('click',".confirm",function(e){
                e.preventDefault();
                    $("#form_delete").attr('action',$("#form_delete").attr('action').replace('/' + 0,'/' + $(this).data('id')));
                    $("#form_delete").find("[name=id]").val($(this).data('id'));

                swal({
                    title: "Delete ?",
                    text: "You will not be able to recover deleted items, are you sure want to delete ?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: false },
                    function(isConfirm){
                    if(isConfirm){
                        $("#form_delete").submit();
                    }

                });

            });
        </script>
        