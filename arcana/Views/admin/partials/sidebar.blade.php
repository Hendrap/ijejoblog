    <?php 
    $id = Auth::user()->id;
    $user = \User::with(['metas','roles'])->find($id);
    $avatar = getEntryMetaFromArray($user->metas, 'avatar');
    ?>
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav metismenu" id="side-menu">
                    <li class="nav-header">
                        <div class="dropdown profile-element"> <span>
                            <img alt="image" onerror="this.src='{{asset ('cms/img/arcana/arcana-small.png') }}'" class="img-circle" src="{{asset ($avatar) }}" style="max-width: 48px;" />
                             </span>
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">{{ app("AdminUser")->name }}</strong>
                             </span> <span class="text-muted text-xs block">{{ app("AdminUser")->roles[0]->name }}<b class="caret"></b></span> </span> </a>
                            <!-- <ul class="dropdown-menu animated fadeInRight m-t-xs">
                                <li><a href="profile.html">Profile</a></li>
                                <li><a href="contacts.html">Contacts</a></li>
                                <li><a href="mailbox.html">Mailbox</a></li>
                                <li class="divider"></li>
                                <li><a href="login.html">Logout</a></li>
                            </ul> -->
                        </div>
                        <div class="logo-element">
                            Arc+
                        </div>
                    </li>
                    <li class="{{ ($activeParent == 'dashboard') ? 'active' : '' }}">
                        <a href="{{route('arc_dashboard')}}"><i class="fa fa-th-large"></i> <span class="nav-label">Dashboards</span></a>
                    </li>
                    <li class="@if($activeParent == 'general_setting') active @endif @if($active == 'email_template') active @endif">
                        <a href="#"><i class="fa fa-steam"></i> <span class="nav-label">General</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
                            <li class="@if($active == 'site_description') child-active @endif"><a href="{{route('arcana_admin_get_setting_general')}}">Site Description</a></li>
                            <li class="@if($active == 'analytics') child-active @endif"><a href="{{route('arcana_admin_get_analytics')}}">Analytics</a></li>
                            <li class="@if($active == 'contact') child-active @endif"><a href="{{route('arcana_admin_get_contacts')}}">Site Contact</a></li>
                            <li class="@if($active == 'email_template') child-active @endif"><a href="{{route('arcana_admin_entry_index',['email_template'])}}">Templates</a></li>
                        </ul>
                    </li>

                    <li class="@if($activeParent == 'main_account') active @endif">
                        <a href="#"><i class="fa fa-users"></i> <span class="nav-label">Accounts</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
                            <li class="@if($active == 'all_account') child-active @endif"><a href="{{route('arcana_admin_all_account')}}">All Accounts</a></li>
                            <li class="@if($active == 'role') child-active @endif"><a href="{{route('arcana_admin_all_roles')}}">Roles & Access</a></li>
                        </ul>
                    </li>

                    <li>
                        <a href="#" class="nav-header"><span class="nav-label">Content Management</span></a>
                    </li>
                    
                    <?php $entries = app('ArcanaSetting')->entries; ?>

                    <li class="">
                        <a href="#"><i class="fa fa-files-o"></i> <span class="nav-label">Pages</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
                            <li class="@if($active == 'page') child-active @endif"><a href="{{ route('arcana_admin_entry_index',['page']) }}">All Pages</a></li>
                        </ul>
                    </li>

                    <li class="@if($activeParent == 'main_taxonomy') active @endif">
                        <a href="#"><i class="fa fa-folder"></i> <span class="nav-label">Taxonomy</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
                            <?php $taxonomies = app('ArcanaSetting')->taxonomies; ?>
                              @foreach($taxonomies as $value)
                                  @if($value['show_ui'] == 'yes')
                                   <li class="{{ ($active == 'taxonomy_'.$value['slug']) ? 'child-active' : '' }}"><a href="{{ route('arcana_admin_taxonomy_index',[$value['slug']]) }}">{{ucfirst($value['plural'])}}</a></li>
                                   @endif
                              @endforeach
                        </ul>
                    </li>

                    <li class="{{ ($activeParent == 'main_media_library') ? 'active' : '' }}">
                        <a href="#"><i class="icon-media"></i> <span class="nav-label">Media Library</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
                            <li class="{{ ($active == 'media_image') ? 'child-active' : '' }}">
                                <a href="{{route('arcana_admin_media_image')}}"> <span>Images</span></a>
                            </li>
                            <li class="{{ ($active == 'media_audio') ? 'child-active' : '' }}">
                                <a href="{{route('arcana_admin_media_audio')}}"> <span>Audio</span></a>
                            </li>
                            <li class="{{ ($active == 'media_video') ? 'child-active' : '' }}">
                                <a href="{{route('arcana_admin_media_video')}}"> <span>Videos</span></a>
                            </li>
                            <li class="{{ ($active == 'slideshow') ? 'child-active' : '' }}">
                                <a href="{{ route('arcana_admin_entry_index',['slideshow']) }}"> <span>Slideshow Manager</span></a>
                            </li>
                        </ul>
                    </li>

                    @foreach($entries as $key => $value)
                    @if($value['show_ui'] == 'yes')
                    <li class="@if($activeParent == $value['slug']) active @endif">
                        <a href="#"><i class="fa fa-cube"></i> <span class="nav-label">{{ $value['plural'] }}</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
                            <li class="{{ ($active == $value['slug']) ? 'child-active' : '' }}">
                                <a href="{{ route('arcana_admin_entry_index',[$value['slug']]) }}"> <span>All {{ $value['plural'] }}</span></a>
                            </li>
                        </ul>
                    </li>
                    @endif
                    @endforeach
                   <li>
                        <a href="#" class="nav-header"><span class="nav-label">Configurations</span></a>
                    </li>
                    <li class="@if($activeParent == 'main_setting') active @endif">
                        <a href="#"><i class="fa fa-files-o"></i> <span class="nav-label">Settings</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
                            <li class="@if($active == 'setting_general') child-active @endif"><a href="{{ route('arcana_admin_get_setting_site_general') }}">General</a></li>
                            <li class="@if($active == 'setting_entry_type') child-active @endif"><a href="{{route('entry_type.index')}}">Entries</a></li>
                            <li class="@if($active == 'setting_taxonomy_type') child-active @endif"><a href="{{route('taxonomy_type.index')}}">Taxonomy</a></li>
                            <li class="@if($active == 'setting_image') child-active @endif"><a href="{{route('image.index')}}">Image Handling</a></li>
                            <li class="@if($active == 'setting_activity_log') child-active @endif"><a href="{{route('arcana_admin_activity_log')}}">Activity Log</a></li>
                        </ul>
                    </li>
                </ul>

            </div>
        </nav>