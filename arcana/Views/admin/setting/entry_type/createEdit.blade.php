<?php
    
    $taxonomies = Setting::whereBundle('arcana.taxonomy')->get();

    $slug = old('slug');
    $single = old('single');
    $plural = old('plural');
    $reorder = old('reorder');
    $custom_text = old('custom_text');
    $show_ui = old('show_ui');
    $meta_key = old('meta_key',array());
    $meta_name = old('meta_name',array());
    $meta_type = old('meta_type',array());
    $meta_data_type = old('meta_data_type',array());
    $entry_taxonomy = old('taxonomies',array());
    $filters = old('filters',array());
    

    if(!empty($setting)){
        $value = unserialize($setting->setting_value);
        $slug = old('slug',@$value['slug']);
        $single = old('single',@$value['single']);
        $plural = old('plural',@$value['plural']);
        $reorder = old('reorder',@$value['reorder']);
        $show_ui = old('show_ui',@$value['show_ui']);
        $filters = old('filters',@$value['filters']);
       

        $default = @$value['taxonomies'];
        $custom_text = old('custom_text',@$value['custom_text']);
        if(empty($default)) $default = [];
        if(empty($filters)) $filters = [];
        $entry_taxonomy = old('taxonomies',$default);
        $tmp_meta_key = array();
        $tmp_meta_name = array();
        $tmp_meta_data_type = array();
        $tmp_meta_type = array();
        $tmp_meta_size = array();

        if(!empty($value['metas'])){
            foreach ($value['metas'] as $k => $val) {
                $tmp_meta_key[] = $val->meta_key;
                $tmp_meta_name[] = $val->meta_name;
                $tmp_meta_data_type[] = $val->meta_data_type;
                $tmp_meta_type[] = @$val->meta_type;
                $tmp_meta_size[] = @$val->meta_size;
            }
            
        }

        $meta_key = old('meta_key',$tmp_meta_key);
        $meta_name = old('meta_name',$tmp_meta_name);
        $meta_data_type = old('meta_data_type',$tmp_meta_data_type);
        $meta_type = old('meta_type',$tmp_meta_type);
        $meta_size = old('meta_size',$tmp_meta_size);
    }

    if(empty($setting)){
        $setting = new StdClass();
        $setting->id = 0;
    }
 ?>
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Setting - Entry Type</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a>Settings</a>
                        </li>
                        <li class="active">
                            <strong><?php echo ($setting->id == 0) ? 'Add a new' : 'Edit' ?> Entry Type</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>

            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h3>
                                    @if($setting->id == 0)
                                        Entry Type
                                    @else
                                        {{ $single }}
                                    @endif
                                </h3>
                                @if($setting->id != 0)
                                    <p class="content-group-lg">Created on {{date_format(date_create($setting->created_at),app('ArcanaSetting')->getSetting('date_format'))}} at {{date_format(date_create($setting->created_at),app('ArcanaSetting')->getSetting('time_format'))}} by {{ generateName($setting->user) }}, last modified on {{date_format(date_create($setting->updated_at),app('ArcanaSetting')->getSetting('date_format'))}} at {{date_format(date_create($setting->updated_at),app('ArcanaSetting')->getSetting('time_format'))}}</p>                    
                                @endif

                                @if(count($errors) > 0)
                                @foreach($errors->all() as $key => $error)
                                @if($key == 0)
                                <div class="alert bg-danger alert-styled-left">
                                    <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
                                    @if(Illuminate\Support\Str::contains($error,'required'))
                                        <span class="text-semibold">Required field(s) error or missing</span>
                                    @else
                                        <span class="text-semibold">{{ str_replace('metas.','',$error) }}</span>
                                    @endif
                                </div>
                                @endif
                                @endforeach
                                @endif

                                <div style="display: none" id="errorMsgRequired" class="alert bg-danger alert-styled-left">
                                    <span class="text-semibold">Required field(s) error or missing</span>
                                </div>

                                <div style="display: none" id="errorMsgWrongFormat" class="alert bg-danger alert-styled-left">
                                    <span class="text-semibold">Wrong format found</span>
                                </div>

                            </div>
                            <div class="ibox-content">

                                <form name="arcanaForm" id="" method="POST" action="{{route('entry_type.store')}}" class="form-horizontal">
                                    <div class="form-group"><label class="col-sm-2 control-label">Slug</label>
                                        <div class="col-lg-4">
                                            <input id="slug" name="slug" type="text" class="form-control" value="{{$slug}}">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-lg-2">Label Single</label>
                                        <div class="col-lg-4">
                                            <input id="single" type="text" name="single" class="form-control" value="{{$single}}" >
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-lg-2">Label Plural</label>
                                        <div class="col-lg-4">
                                            <input id="plural" type="text" name="plural" class="form-control" value="{{$plural}}" >
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-lg-2">Custom Text</label>
                                        <div class="col-lg-4">
                                            <input id="custom_text" type="text" name="custom_text" class="form-control" value="{{$custom_text}}" >
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-lg-2">Show UI</label>
                                        <div class="col-lg-1">

                                            <select id="show_ui" name="show_ui" class="form-control">
                                                <?php $options = ['yes','no']?>
                                                @foreach($options as $v)
                                                <option {{ ($v == $show_ui ) ? 'selected' : '' }} value="{{$v}}">{{ucfirst($v)}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-lg-2">Reorder ?</label>
                                        <div class="col-lg-1">

                                            <select id="reorder" name="reorder" class="form-control">
                                                <?php $options = ['yes','no']?>
                                                @foreach($options as $v)
                                                <option {{ ($v == $reorder ) ? 'selected' : '' }} value="{{$v}}">{{ucfirst($v)}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-lg-2">Taxonomies</label>
                                         
                                        <div class="col-lg-8">
                                           
                                            <div class="row">
                                                @foreach($taxonomies as $taxo)
                                                <?php $value = unserialize($taxo->setting_value) ?>
                                                <div class="col-sm-4">
                                                    <div class="checkbox i-checks">
                                                        <label>
                                                                <input value="{{$taxo->id}}" name="taxonomies[]" <?php echo in_array($taxo->id, $entry_taxonomy) ? 'checked' : ''  ?> type="checkbox">
                                                               {{ $value['single'] }}
                                                        </label>
                                                    </div>
                                                </div>
                                                @endforeach

                                            </div>
                                            
                                        </div>
                                       
                                    </div>


                                     <div class="form-group">
                                        <label class="control-label col-lg-2">Filters</label>
                                         
                                        <div class="col-lg-8">
                                           
                                            <div class="row">
                                                @foreach($taxonomies as $taxo)
                                                <?php $value = unserialize($taxo->setting_value) ?>
                                                <div class="col-sm-4">
                                                    <div class="checkbox i-checks">
                                                        <label>
                                                                <input value="{{$taxo->id}}" name="filters[]" <?php echo in_array($taxo->id, $filters) ? 'checked' : ''  ?> type="checkbox">
                                                               {{ $value['single'] }}
                                                        </label>
                                                    </div>
                                                </div>
                                                @endforeach

                                            </div>
                                            
                                        </div>
                                       
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="control-label col-lg-2">Metas</label>
                                        <div id="meta_container" class="col-lg-10">

                                            @foreach($meta_key as $thekey => $theval)
                                                <?php echo newLine(view('arcana::admin.setting.entry_type.template.metas',[
                                                    'meta_key' => $theval,
                                                    'meta_name' => $meta_name[$thekey],
                                                    'meta_data_type' => $meta_data_type[$thekey],
                                                    'meta_type' => $meta_type[$thekey],
                                                    'meta_size' =>  @$meta_size[$thekey],
                                                ])->render()); ?>
                                            @endforeach

                                            @if(empty($meta_key))
                                                <?php echo newLine(view('arcana::admin.setting.entry_type.template.metas')) ?>
                                            @endif

                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-2 col-sm-offset-10 col-xs-12">
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <button class="btn btn-primary pull-right" type="submit" style="margin-left: 10px;">Save</button>
                                                    <a href="{{route('entry_type.index')}}" class="btn btn-white pull-right">Cancel </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <input type="hidden" name="id" value="{{$setting->id}}">
                                    {{csrf_field()}}
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <!-- iCheck -->
            <script src="{{asset('cms/js/plugins/iCheck/icheck.min.js')}}"></script>
            <script>
                $(document).ready(function () {
                    $('.i-checks').iCheck({
                        checkboxClass: 'icheckbox_square-green',
                        radioClass: 'iradio_square-green',
                    });
                });
            </script>

            <script type="text/javascript">

                var meta_template = '<?php echo newLine(view('arcana::admin.setting.entry_type.template.metas')) ?>';
                
                function initSelect2()
                {
                    // $('select').each(function() {

                    //     select_placeholder = $(this).attr('placeholder');
                    //     select_width = $(this).attr('width');
                    //     $(this).select2({
                    //         minimumResultsForSearch: "Infinity",
                    //         placeholder: select_placeholder,
                    //         width: select_width,

                    //     });

                    // });
                }
                
                $(document).ready(function(){
                    initPreventClose();
                    initSelect2();
                
                    $(document).on('click',".addmeta",function(e){
                        e.preventDefault();
                        $("#meta_container").append(meta_template);
                                    initSelect2();

                    });
                    $(document).on('click',".delete_meta",function(e){
                        e.preventDefault();
                        $(this).parent().parent().remove();
                    });


                    var status = "{{session('msg')}}";
                    if (status === 'Data Saved!') {
                        swal({
                            title: "SUCCESS",
                            text: "All changes has been saved successfuly",
                            confirmButtonColor: "#66BB6A",
                            type: "success"
                            });
                    }
                });
            </script>