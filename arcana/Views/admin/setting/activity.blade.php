
 @if(session('msg'))

    <div class="alert bg-success alert-styled-left">

        <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>

        <span class="text-semibold">{{session('msg')}}</span>

    </div>

@endif

            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10 col-sm-10 col-xs-12">
                    <h2>Settings - All Activity Logs</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a>Settings</a>
                        </li>
                        <li class="active">
                            <strong>All Activity Logs</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2 col-sm-2 col-xs-hidden">
                </div>
            </div>

        <div class="wrapper wrapper-content animated fadeInRight ecommerce" id="log">


            <div class="ibox-content m-b-sm border-bottom">
                <div class="row">
                    <div class="filter-table clearfix pull-left mr-5">
                        <div class="form-group pull-left  mr-5 no-margin-bottom">
                            <label style="height: 36px; line-height: 36px;margin-bottom: 0px;">Filter:&nbsp;</label>
                        </div>
                        <div class="form-group pull-left has-feedback  no-margin-bottom">
                            <input type="text" class="form-control" id="search_the_table" style="height:36px">
                            <div class="form-control-feedback" style="line-height:36px;">
                                    <i class="icon-search4" style="font-size:12px"></i>
                            </div>
                        </div>
                        
                    </div>

                    <?php $roles = \Role::get(); ?>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <select class="table-select2 filter-table-column" placeholder="Filter By Role">
                                <option data-col="1" value="0">All Roles</option>
                                @foreach($roles as $role)
                                <option data-col="1" value="{{$role->id}}">{{$role->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                </div>

            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">
                        <div class="ibox-content">

                            <table class="table entry-table table-striped">
                                <thead>
                                    <tr>
                                        <th width="50px" class="no-sort text-center">&nbsp;</th>
                                        <th class="td-date" width="150px">
                                            <span>Activity Date</span>
                                        </th>
                                        <th width="190px">User &amp; Role</th>
                                        <th>Activity</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                </tbody>
                            </table>


                        </div>
                    </div>
                </div>
            </div>

            <div class="modal inmodal" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                <div class="modal-content animated bounceInRight">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <i class="fa fa-eye modal-icon"></i>
                            <h4 class="modal-title">Information Detail</h4>
                            <small class="font-bold">Showing Client Information</small>
                        </div>
                        <div class="modal-body">
                            <p><strong>Lorem Ipsum is simply dummy</strong> text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown
                                printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting,
                                remaining essentially unchanged.</p>
                        </div>
                        <div class="modal-footer">
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <script type="text/javascript">
            var urlGetData = "{{route('arcana_admin_get_data_log')}}";
            
            $(document).ready(function(){
                $('#page-render .pagination').addClass('custom-pagination');

                $('.show-detail').click(function(e){
                    e.preventDefault();

                });
            });
        </script>

        <script>
        var entry_table = {};
            $(document).ready(function() {
                
                 entry_table = $('.entry-table').on( 'processing.dt', function ( e, settings, processing ) {
                        var block = $('.entry-table');
                         if(processing){
                              $('.dataTable, .dataTable tbody').css('position','static');
                                if(typeof entry_table.ajax != 'undefined')
                                {
                                    $(e.target).find('tbody').html('');
                                }
                                $(block).block({
                                    message: '<span class="text-semibold"><i class="fa fa-spinner spinner position-left"></i>&nbsp;Loading Data</span>',
                                    overlayCSS: {
                                        backgroundColor: '#fff',
                                        opacity: 0.8,
                                        cursor: 'wait'
                                    },
                                    css: {
                                        border: 0,
                                        padding: '10px 15px',
                                        color: '#333',
                                        width: 'auto',
                                       
                                    }
                                });
                         }else{
                            $('.dataTable, .dataTable tbody').css('position','relative');
                            $.unblockUI();
                            $(".blockUI").remove();

                         }
                         $( document ).trigger( "arcana--datatable-processing",[processing,$('.entry-table'),block,e]);
                }).DataTable({
                    "initComplete":function()
                    {
                       
                    },
                    "order": [],
                    "pageLength": 20,
                    "searching" : true,
                    "lengthChange": false,
                    "dom": 'rt<"datatable-footer"ilp><"clear">',
                    "processing": true,
                    "serverSide": true,
                    "oLanguage": {
                        "sProcessing":' ' 
                    },
                    "ajax": "{{ route('arcana_admin_get_data_log') }}",
                    "columnDefs": [{
                        "targets": 'no-sort',
                        "orderable": false,
                    }],
                    "columns": [

                        { "data": "created_at", "render": function(data, type, full, meta){
                            var html = full.html.split('|arcana--datatable-separator--|');
                            return html[0];
                        },
                        sClass:""
                        },

                        { "data": "activity", "render": function(data, type, full, meta){
                            var html = full.html.split('|arcana--datatable-separator--|');
                            return html[1];
                        },
                        sClass:"td-date"
                        },

                        { "data": "name", "render": function(data, type, full, meta){
                            var html = full.html.split('|arcana--datatable-separator--|');
                            return html[2];
                        },
                        sClass:""
                        },

                        { "data": "email", "render": function(data, type, full, meta){
                            var html = full.html.split('|arcana--datatable-separator--|');
                            return html[3];
                        },
                        sClass:""
                        }

                    ]
                });

                
                var filterby;
                
                // $('.filter-table-column').change(function(){
                //     var col = "";
                //     var val = "";
                //     thisfilter = $(this);
                //     thisfilter.find('option:selected').each(function(){
                //         val = $(this).val();
                //         col = $(this).attr('data-col');
                //     });
                    
                //     entry_table.columns(col).search(val).draw();
                    
                    
                // });
                
                $('#search_the_table').keyup(function(e){
                    if(e.which == 13)
                    {
                     entry_table.search($(this).val()).draw();
                    }
                });
                
                $('select').select2({
                    minimumResultsForSearch: Infinity,
                    width: 'auto'
                });
                 $('.table-select2').each(function() {

                    select_placeholder = $(this).attr('placeholder');
                    $(this).select2({
                        minimumResultsForSearch: Infinity,
                        placeholder: select_placeholder,
                        width: '220px',

                    });

                });
                
            });

        </script>