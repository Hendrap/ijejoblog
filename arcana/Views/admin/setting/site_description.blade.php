            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>GENERAL - Site Description</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a>General</a>
                        </li>
                        <li class="active">
                            <strong>Site Description</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>

            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h3>Site <small>- Site Description</small></h3>
                            </div>
                            <div class="ibox-content">

                                <form name="arcanaForm" id="form-site-description" method="POST" action="{{ route('arcana_admin_post_setting_general',['site_desc']) }}" class="form-horizontal">
                                    <div class="form-group"><label class="col-sm-2 control-label">Site Name</label>
                                        <div class="col-sm-10"><input type="text" name="site_name" class="form-control" id="site_name" value="{{ old('site_name',app('ArcanaSetting')->getSetting('site_name'))  }}" ></div>
                                    </div>

                                    <div class="form-group"><label class="col-sm-2 control-label">Site Tagline</label>
                                        <div class="col-sm-10"><input type="text" name="site_tagline" class="form-control" id="site_tagline" value="{{ old('site_tagline',app('ArcanaSetting')->getSetting('site_tagline')) }}"></div>
                                    </div>

                                    <div class="form-group"><label class="col-sm-2 control-label">Site Description</label>
                                        <div class="col-sm-10"><textarea name="site_desc" class="form-control" id="site_desc" value="">{{ old('site_desc',app('ArcanaSetting')->getSetting('site_desc')) }}</textarea></div>
                                    </div>

                                    <div class="hr-line-dashed"></div>

                                    <div class="form-group"><label class="col-sm-2 control-label">SEO Title</label>
                                        <div class="col-sm-10"><input type="text" name="seo_title" class="form-control" placeholder="Customize your page title for SEO purpose" id="seo_title" value="{{ app('ArcanaSetting')->getSetting('seo_title') }}"></div>
                                    </div>

                                    <div class="form-group"><label class="col-sm-2 control-label">SEO Description</label>
                                        <div class="col-sm-10"><textarea name="seo_desc" class="form-control" value="" id="seo_desc" placeholder="Default site description being used on your website">{{ app('ArcanaSetting')->getSetting('seo_desc') }}</textarea></div>
                                    </div>

                                    <div class="form-group"><label class="col-sm-2 control-label">Site Keywords</label>
                                        <div class="col-sm-10"><input type="text" name="seo_keywords" class="form-control" placeholder="Default SEO keywords being used on your website" id="seo_keywords" placeholder="" value="{{ app('ArcanaSetting')->getSetting('seo_keywords') }}"></div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-4 col-sm-offset-2">
                                            <button class="btn btn-white" type="submit">Cancel</button>
                                            <button class="btn btn-primary" type="submit">Save changes</button>
                                        </div>
                                    </div>
                                    {{ csrf_field() }}
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <script type="text/javascript">
                $(document).ready(function(){
                    initPreventClose();
                    


                    var status = "{{session('success')}}";
                    if (status === 'Data Saved!') {
                        swal({
                            title: "SUCCESS",
                            text: "All changes has been saved successfuly",
                            confirmButtonColor: "#66BB6A",
                            type: "success"
                            });
                    }
                });
            </script>