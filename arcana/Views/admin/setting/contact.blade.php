            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>General - Site Contact</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a>General</a>
                        </li>
                        <li class="active">
                            <strong>Site Contact</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>

            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h3>General <small>- Site Contact</small></h3>
                            </div>
                            <div class="ibox-content">
                                <p class="content-group-lg">Use the followings to define default contact information </p>
                                @if(count($errors) > 0)
                                @foreach($errors->all() as $key => $error)
                                @if($key == 0)
                                <div class="alert bg-danger alert-styled-left">
                                    <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
                                    <span class="text-semibold">{{ $error }}</span>
                                </div>
                                @endif
                                @endforeach
                                @endif

                                <form name="arcanaForm" id="" method="POST" action="{{ route('arcana_admin_post_setting_general',['contact']) }}" class="form-horizontal">
                                    
                                    {{ csrf_field() }}
                                    <fieldset class="content-group">
                                        <legend class="text-semibold">
                                           Physical Address
                                        </legend>
                                        <div class="form-group">
                                            <label class="control-label col-lg-2">Street Address</label>
                                            <div class="col-lg-4">
                                                 <textarea class="form-control" name="street_address" id="street_address">{{ app('ArcanaSetting')->getSetting('street_address') }}</textarea>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-lg-2">City</label>
                                            <div class="col-lg-4">
                                                 <input name="city" type="text" class="form-control" id="city" value="{{ app('ArcanaSetting')->getSetting('city') }}">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-lg-2">State/Province</label>
                                            <div class="col-lg-4">
                                                <input type="text" name="state" class="form-control" id="state" value="{{ app('ArcanaSetting')->getSetting('state') }}" >
                                            </div>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label class="control-label col-lg-2">Postal Code</label>
                                            <div class="col-lg-4">
                                                <input type="text" name="postal_code" class="form-control" id="postal_code" value="{{ app('ArcanaSetting')->getSetting('postal_code') }}">
                                            </div>
                                        </div>                    
                                        <div class="form-group">
                                            <label class="control-label col-lg-2">Country</label>
                                            <div class="col-lg-4">
                                                <input type="text" name="country" class="form-control" id="country" value="{{ app('ArcanaSetting')->getSetting('country') }}">                               
                                            </div>
                                        </div>
                                    </fieldset>

                                    <div class="form-group">
                                        <div class="col-sm-4 col-sm-offset-2">
                                            <button class="btn btn-white" type="submit">Cancel</button>
                                            <button class="btn btn-primary" type="submit">Save</button>
                                        </div>
                                    </div>
                                    {{csrf_field()}}
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <script type="text/javascript">
                $(document).ready(function(){
                    initPreventClose();
                    


                    var status = "{{session('success')}}";
                    if (status === 'Data Saved!') {
                        swal({
                            title: "SUCCESS",
                            text: "All changes has been saved successfuly",
                            confirmButtonColor: "#66BB6A",
                            type: "success"
                            });
                    }
                });
            </script>