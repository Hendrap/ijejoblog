<?php

    if(empty($role)){
        $role = new StdClass();
        $role->id = 0;
        $role->name = '';
        $role->rules = array();
    }

    $roleRules = [];
    $tmpRules = Rule::where('id','!=',1)->get();
    $rules = array();
    
    foreach ($tmpRules as $key => $value) {
        @$rules[$value->group][] = $value->id;
    }

    if(!empty($role)){
        foreach ($role->rules as $key => $value) {
            $roleRules[] = $value->id;
        }
    }

    $roleRules = old('rules',$roleRules);

?>

            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Accounts - {{ ($role->id == 0) ? 'New' : 'Edit' }} Role</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a>Home</a>
                        </li>
                        <li>
                            <a>Accounts</a>
                        </li>
                        <li class="active">
                            <strong>{{ ($role->id == 0) ? 'New' : 'Edit' }} Role</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>

<!-- /page header -->



<!-- Content area -->
<div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">

        <div class="col-xs-12">

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h3>
                        @if($role->id == 0)
                            Role
                        @else
                            {{ $role->name }}
                        @endif
                    </h3>
                    @if($role->id > 0)
                    <p class="content-group-lg">Created on {{date_format(date_create($role->created_at),app('ArcanaSetting')->getSetting('date_format'))}} at {{date_format(date_create($role->created_at),app('ArcanaSetting')->getSetting('time_format'))}} am by {{ generateName($role->user) }}, last modified on {{date_format(date_create($role->updated_at),app('ArcanaSetting')->getSetting('date_format'))}} at {{date_format(date_create($role->updated_at),app('ArcanaSetting')->getSetting('time_format'))}}</p>
                    @endif
                   @if(count($errors) > 0)
                    @foreach($errors->all() as $key => $error)
                    @if($key == 0)
                    <div class="alert bg-danger alert-styled-left">
                        <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
                        @if(Illuminate\Support\Str::contains($error,'required'))
                            <span class="text-semibold">Required field(s) error or missing</span>
                        @else
                            <span class="text-semibold">{{ str_replace('metas.','',$error) }}</span>
                        @endif
                    </div>
                    @endif
                    @endforeach
                    @endif
                </div>
                <div class="ibox-content">

                    
                    <div style="display: none" id="errorMsgRequired" class="alert bg-danger alert-styled-left">
                            <span class="text-semibold">Required field(s) error or missing</span>
                         </div>

                         <div style="display: none" id="errorMsgWrongFormat" class="alert bg-danger alert-styled-left">
                             <span class="text-semibold">Wrong format found</span>
                         </div>

                    <form name="arcanaForm" action="{{route('arcana_admin_save_roles')}}" method="POST" class="form-horizontal form-validate-jquery">
                        
                    <div class="fieldset">
                    <input type="hidden" name="id" value="{{$role->id}}">
                        
                        <div class="form-group">
                            <label class="control-label col-lg-1">Role</label>
                            <div class="col-lg-11">
                                <input id="name" type="text" name="name" class="form-control" value="{{old('name',$role->name)}}">
                            </div>
                        </div>

                        
                        <table class="table role-table table-one-line">
                        
                            <tr>
                                <th>Access</th>
                                <th width="50px">Write</th>
                                <th width="50px">Read</th>
                            </tr>
                            
                            @foreach($rules as $key => $rule)
                            <tr>
                                <td class="text-bold">
                                    {{$key}}
                                </td>
                                @foreach($rule as $r)
                                <td>
                                    <div class="i-checks">
                                        <label><input <?php echo in_array($r, $roleRules) ? 'checked' : ''; ?> value="{{$r}}" name="rules[]"  type="checkbox" class="styled">
                                        </label>
                                    </div>
                                </td>
                                @endforeach

                                @if(count($rule) < 2)
                                    <td></td>
                                @endif
                            </tr>
                            @endforeach
                            {{csrf_field()}}
                        </table>
                       
                    
                    </div>
                
                       <div class="text-right">
                            
                            <a href="{{route('arcana_admin_all_roles')}}" class="btn btn-white">Cancel</a>
                           <button type="submit" class="btn btn-primary ml-10" >Save</button>
                           
                        </div>
                    </form>

                </div>

            </div>

        </div>

    </div>

</div>

        <?php
            $err = count($errors);
            $errMsg = "";
        ?>
        {!! view('arcana::admin.error-script',[
                'errors'=>$errors,
                'fields' => [
                    'name' => 'required',
                ]

            ]); 
        !!}

    <script>
        
        $(document).ready(function() {

            // autosize($('textarea'));
            var status = "{{session('msg')}}";
            if (status === 'Data Saved!') {
                swal({
                    title: "SUCCESS",
                    text: "All changes has been saved successfuly",
                    confirmButtonColor: "#66BB6A",
                    type: "success"
                    });
            }

            var err = "{{$err}}";
            
            
            
        });

    </script>
    <script src="{{asset('cms/js/plugins/iCheck/icheck.min.js')}}"></script>
    <script>
        $(document).ready(function () {
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });
        });
    </script>
    <!-- Footer -->
    <!-- /footer -->

</div>
<script type="text/javascript">
    $(document).ready(function(){
        initPreventClose();
    })
</script>
