
 @if(session('msg'))

    <div class="alert bg-success alert-styled-left">

        <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>

        <span class="text-semibold">{{session('msg')}}</span>

    </div>

@endif

            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10 col-sm-10 col-xs-12">
                    <h2>Account - All Roles</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a>Accounts</a>
                        </li>
                        <li>
                            <a>Roles</a>
                        </li>
                        <li class="active">
                            <strong>All Roles</strong>
                        </li>
                    </ol>
                    <div class="visible-xs">
                            <a href="{{ route('arcana_admin_create_roles') }}" class="btn btn-primary"><b><i class="fa fa-file-o"></i></b> New Roles</a> 
                    </div>
                </div>
                <div class="col-lg-2 col-sm-2 col-xs-hidden">

                    <div class="heading-elements">

                        <a href="{{ route('arcana_admin_create_roles') }}" class="btn btn-primary"><b><i class="fa fa-file-o"></i></b> New Roles</a>

                    </div>
                </div>
            </div>

        <div class="wrapper wrapper-content animated fadeInRight ecommerce" id="role">


            <div class="ibox-content m-b-sm border-bottom">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="input-group">
                            
                            <input type="text" class="form-control input-sm m-b-xs" id="filter"
                                   placeholder="Search in table"> <span class="input-group-addon"><i class="fa fa-search"></i></span>
                        </div>
                    </div>
                </div>

            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">
                        <div class="ibox-content">

                            <div class="table-responsive">
                                <table class="footable table table-striped toggle-arrow-tiny table-vertical-middle" data-page-size="10" data-filter=#filter>
                                <thead>
                                <tr>

                                    <th data-toggle="true">Role</th>
                                    <th data-hide="phone,tablet">Created</th>
                                    <th data-hide="phone">Modified</th>
                                    <th class="text-right" data-sort-ignore="true"></th>

                                </tr>
                                </thead>
                                <tbody>
                                @foreach($roles as $key => $role)
                                <tr>
                                    <td><a href="{{ route('arcana_admin_edit_roles',[$role->id]) }}">{{$role->name}}</a></td>
                                    
                                    <td>
                                        @if(!empty($role->created_at))
                                            {{date_format($role->created_at,app('ArcanaSetting')->getSetting('date_format'))}}
                                            <br><span class="text-muted">{{date_format($role->created_at,app('ArcanaSetting')->getSetting('time_format'))}}</span>
                                        @endif
                                    </td>
                                    <td>
                                        @if(!empty($role->updated_at))
                                             {{date_format($role->updated_at,app('ArcanaSetting')->getSetting('date_format'))}}
                                            <br><span class="text-muted">{{date_format($role->updated_at,app('ArcanaSetting')->getSetting('time_format'))}}</span>
                                        @else
                                            Never
                                        @endif
                                    </td> 
                                    <td class="text-right">
                                        <div class="btn-group">
                                            <button data-toggle="dropdown" class="btn btn-default dropdown-toggle" aria-expanded="true"><i class="fa fa-bars"></i> <span class="caret"></span></button>
                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li><a href="{{ route('arcana_admin_edit_roles',[$role->id]) }}"><i class="fa fa-edit"></i> Edit</a></li>
                                                <li class="divider"></li>
                                                <li><a data-id="{{$role->id}}" href="#" class="confirm"><i class="fa fa-trash-o"></i> Delete</a></li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach                                
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td colspan="6">    
                                        <ul class="pagination pull-right"></ul>
                                    </td>
                                </tr>
                                </tfoot>
                            </table>

                            <form style="display:none" id="form_delete" action="{{route('arcana_admin_delete_roles',[0])}}" method="POST">
                                {{csrf_field()}}
                                <input type="hidden" name="id" value="0" >
                                {{ method_field('DELETE') }}
                            </form>


                        </div>
                    </div>
                </div>
            </div>

        </div>
        <script type="text/javascript">

            $(document).on('click',".confirm",function(e){
                e.preventDefault();
                    $("#form_delete").attr('action',$("#form_delete").attr('action').replace('/' + 0,'/' + $(this).data('id')));
                    $("#form_delete").find("[name=id]").val($(this).data('id'));

                swal({
                    title: "Delete ?",
                    text: "You will not be able to recover deleted items, are you sure want to delete ?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: false },
                    function(isConfirm){
                    if(isConfirm){
                        $("#form_delete").submit();
                    }

                });

            });

        </script>