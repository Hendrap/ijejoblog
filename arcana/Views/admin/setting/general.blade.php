            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Setting - General</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a>Settings</a>
                        </li>
                        <li class="active">
                            <strong>General</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>

            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h3>Settings <small>- General Settings</small></h3>
                            </div>
                            <div class="ibox-content">

                                <form name="arcanaForm" id="" method="POST" action="{{ route('arcana_admin_post_setting_general') }}" class="form-horizontal">
                                    <div class="form-group"><label class="col-sm-2 control-label">Domain Name</label>
                                        <div class="col-lg-4"><input type="text" id="domain_name" name="domain_name" class="form-control" value="{{ app('ArcanaSetting')->getSetting('domain_name') }}" placeholder="http://www.domain.com"></div>
                                    </div>

                                    <div class="form-group"><label class="col-sm-2 control-label">Path URL</label>
                                        <div class="col-lg-4"><input type="text" id="path_url" name="path_url" class="form-control" value="{{ app('ArcanaSetting')->getSetting('path_url') }}" placeholder="http://www.domain.com/path_url"></div>
                                    </div>

                                    <div class="form-group"><label class="col-sm-2 control-label">Date Format</label>
                                        <div class="col-lg-4">
                                           <select name="date_format" class="form-control">
                                                <option <?php if(app('ArcanaSetting')->getSetting('date_format') == 'd M Y'){echo 'selected';}?> value="d M Y">{{ date('d M Y') }}</option>
                                                <option <?php if(app('ArcanaSetting')->getSetting('date_format') == 'Y/m/d'){echo 'selected';}?> value="Y/m/d">{{ date('Y/m/d') }}</option>
                                                <option <?php if(app('ArcanaSetting')->getSetting('date_format') == 'd/m/Y'){echo 'selected';}?> value="d/m/Y">{{ date('d/m/Y') }}</option>
                                                <option <?php if(app('ArcanaSetting')->getSetting('date_format') == 'm/d/Y'){echo 'selected';}?> value="m/d/Y">{{ date('m/d/Y') }}</option>
                                                <option <?php if(app('ArcanaSetting')->getSetting('date_format') == 'd-m-Y'){echo 'selected';}?> value="d-m-Y">{{ date('d-m-Y') }}</option>
                                           </select>
                                        </div>
                                    </div>

                                    <div class="form-group"><label class="col-sm-2 control-label">Time Format</label>
                                        <div class="col-lg-4">
                                            <select name="time_format" class="form-control">
                                                <option <?php if(app('ArcanaSetting')->getSetting('time_format') == 'h:i a'){echo 'selected';}?> value="h:i a">{{ date('h:i a') }}</option>
                                                <option <?php if(app('ArcanaSetting')->getSetting('time_format') == 'H:i'){echo 'selected';}?> value="H:i">{{ date('H:i') }}</option>
                                           </select>
                                        </div>
                                    </div>

                                    <div class="form-group"><label class="col-sm-2 control-label">Weight</label>
                                        <div class="col-lg-3">
                                            <select name="weight_format" placeholder="" class="form-control">
                                                <option <?php if(app('ArcanaSetting')->getSetting('weight_format') == 'KG'){echo 'selected';}?> value="KG">Kilograms</option>
                                                <option <?php if(app('ArcanaSetting')->getSetting('weight_format') == 'G'){echo 'selected';}?> value="G">Grams</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group"><label class="col-sm-2 control-label">Distance</label>
                                        <div class="col-lg-3">
                                            <select name="distance_format" placeholder="" class="form-control">
                                                <option <?php if(app('ArcanaSetting')->getSetting('distance_format') == 'meters'){echo 'selected';}?> value="meters">Meters</option>
                                                <option <?php if(app('ArcanaSetting')->getSetting('distance_format') == 'kilometers'){echo 'selected';}?> value="kilometers">Kilometers</option>
                                                <option <?php if(app('ArcanaSetting')->getSetting('distance_format') == 'miles'){echo 'selected';}?> value="miles">Miles</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group"><label class="col-sm-2 control-label">Size</label>
                                        <div class="col-lg-3">
                                            <select name="size_format" placeholder="" class="form-control">
                                                <option <?php if(app('ArcanaSetting')->getSetting('size_format') == 'cm'){echo 'selected';}?> value="cm">Centimeters</option>
                                                <option <?php if(app('ArcanaSetting')->getSetting('size_format') == 'm'){echo 'selected';}?> value="m">Meters</option>
                                                <option <?php if(app('ArcanaSetting')->getSetting('size_format') == 'inc'){echo 'selected';}?> value="inc">Inches</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group"><label class="col-sm-2 control-label">Currency</label>
                                        <div class="col-lg-3">
                                            <select name="currency_format" placeholder="" class="form-control">
                                                <option <?php if(app('ArcanaSetting')->getSetting('currency_format') == 'IDR'){echo 'selected';}?> value="IDR">Indonesian Rupiah</option>
                                                <option <?php if(app('ArcanaSetting')->getSetting('currency_format') == 'USD'){echo 'selected';}?> value="USD">US Dollar</option>
                                            </select>
                                        </div>
                                    </div>

                                    

                                    <div class="form-group">
                                        <div class="col-sm-4 col-sm-offset-2">
                                            <button class="btn btn-white" type="submit">Cancel</button>
                                            <button class="btn btn-primary" type="submit">Save changes</button>
                                        </div>
                                    </div>
                                    {{csrf_field()}}
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <script type="text/javascript">
                $(document).ready(function(){
                    initPreventClose();
                    


                    var status = "{{session('success')}}";
                    if (status === 'Data Saved!') {
                        swal({
                            title: "SUCCESS",
                            text: "All changes has been saved successfuly",
                            confirmButtonColor: "#66BB6A",
                            type: "success"
                            });
                    }
                });
            </script>