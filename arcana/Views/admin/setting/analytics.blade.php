            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>General - Analytics</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a>General</a>
                        </li>
                        <li class="active">
                            <strong>Analytics</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>

            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h3>General <small>- Analytics Settings</small></h3>
                            </div>
                            <div class="ibox-content">
                                <p class="content-group-lg">Implement tracking codes to measure your web performance.</p>
                                @if(count($errors) > 0)
                                @foreach($errors->all() as $key => $error)
                                @if($key == 0)
                                <div class="alert bg-danger alert-styled-left">
                                    <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
                                    <span class="text-semibold">{{ $error }}</span>
                                </div>
                                @endif
                                @endforeach
                                @endif

                                <form name="arcanaForm" id="" method="POST" action="{{ route('arcana_admin_post_setting_general',['analytics']) }}" class="form-horizontal">
                                    
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="content-group-lg">
                                                <h6 class="text-semibold">Google Analytics</h6>
                                                <p class="content-group-sm">Insert your Google Analytics code, and the tracking will be automatically active.</p>
                                                <input style="width: 50%" name="ga_code" type="text" class="form-control" id="ga_code" value="{{ app('ArcanaSetting')->getSetting('ga_code') }}">
                                                <span class="help-block">Your Google Analytics code will be something like UA-XXXXXX </span>
                                            </div>
                                            <div class="content-group-lg">
                                                <input style="width: 50%" name="ga_view_id" type="text" class="form-control" id="ga_view_id" value="{{ app('ArcanaSetting')->getSetting('ga_view_id') }}">
                                                <span class="help-block">Google Analytics View Id like XXXXXX</span>
                                            </div>
                                            <div class="content-group-lg">
                                                <input style="width: 50%" type="file" class="form-control" name="ga_account_json">
                                                <span class="help-block">Google Analytics Service Account Key(JSON)</span>
                                            </div>
                                             <div class="content-group-lg">
                                                <input style="width: 50%" type="file" class="form-control" name="ga_account_p12">
                                                <span class="help-block">Google Analytics Service Account Key(P12)</span>
                                            </div>
                                        </div>

                                        <div class="col-lg-6">
                                            <div class="content-group-lg">
                                                <h6 class="text-semibold">Facebook Pixel</h6>
                                                <p class="content-group-sm">Insert your Facebook Pixel Id, and the tracking will be automatically active.</p>
                                                <input style="width: 50%" name="fb_pixel_id" type="text" class="form-control" id="fb_pixel_id" value="{{ app('ArcanaSetting')->getSetting('fb_pixel_id') }}">
                                                <span class="help-block">Your Facebook Pixel Id will be something like 00000000000001 </span>
                                            </div>
                                        </div>
                                    
                                        <div class="col-lg-6">
                                            <div class="content-group-lg">
                                                <h6 class="text-semibold">Extra Head Space</h6>
                                                <p class="content-group-sm">{{ "Insert additional code within the <head> area, in all pages. You can use this for site verification purposes, or any other custom needs." }}</p>
                                                <textarea class="form-control" id="extra_head_space" name="extra_head_space">{!! app('ArcanaSetting')->getSetting('extra_head_space') !!}</textarea>
                                            </div>
                                        </div>
                                      

                                        <div class="col-lg-6">
                                            <div class="content-group-lg">
                                                <h6 class="text-semibold">Extra Leg Room</h6>
                                                <p class="content-group-sm">{{ "Similar to head space, but these lines of code will be available just before the closing </html> tag at the bottom of every page." }}</p>
                                                <textarea class="form-control" id="extra_leg_room" name="extra_leg_room">{!! app('ArcanaSetting')->getSetting('extra_leg_room') !!}</textarea>
                                            </div>
                                        </div>

                                        
                                         
                                        
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-4 col-sm-offset-2">
                                            <button class="btn btn-white" type="submit">Cancel</button>
                                            <button class="btn btn-primary" type="submit">Save</button>
                                        </div>
                                    </div>
                                    {{csrf_field()}}
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <script type="text/javascript">
                $(document).ready(function(){
                    initPreventClose();
                    


                    var status = "{{session('success')}}";
                    if (status === 'Data Saved!') {
                        swal({
                            title: "SUCCESS",
                            text: "All changes has been saved successfuly",
                            confirmButtonColor: "#66BB6A",
                            type: "success"
                            });
                    }
                });
            </script>