<?php
    $name = old('name');
    $width = old('width');
    $height = old('height');
    $crop = old('crop');
    $aspectratio = old('aspectratio');
    if(!empty($setting)){
        $value = unserialize($setting->setting_value);
        $name = old('name',@$value['name']);
        $width = old('width',@$value['width']);
        $height = old('height',@$value['height']);
        $crop = old('crop',@$value['crop']);
        $aspectratio = old('aspectratio',@$value['aspectratio']);
    }
    if(empty($setting)){
        $setting = new StdClass();
        $setting->id = 0;
    }

 ?>
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Setting - Image Handling</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a>Settings</a>
                        </li>
                        <li class="active">
                            <strong><?php echo ($setting->id == 0) ? 'Add a new' : 'Edit' ?> Image Handling</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>

            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h3>
                                    @if($setting->id == 0)
                                        Image Handling
                                    @else
                                        {{ ucfirst($name) }}
                                    @endif
                                </h3>
                                @if($setting->id != 0)
                                    <p class="content-group-lg">Created on {{date_format(date_create($setting->created_at),app('ArcanaSetting')->getSetting('date_format'))}} at {{date_format(date_create($setting->created_at),app('ArcanaSetting')->getSetting('time_format'))}} by {{ generateName($setting->user) }}, last modified on {{date_format(date_create($setting->updated_at),app('ArcanaSetting')->getSetting('date_format'))}} at {{date_format(date_create($setting->updated_at),app('ArcanaSetting')->getSetting('time_format'))}}</p>                    
                                @endif

                                @if(count($errors) > 0)
                                @foreach($errors->all() as $key => $error)
                                @if($key == 0)
                                <div class="alert bg-danger alert-styled-left">
                                    <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
                                    @if(Illuminate\Support\Str::contains($error,'required'))
                                        <span class="text-semibold">Required field(s) error or missing</span>
                                    @else
                                        <span class="text-semibold">{{ str_replace('metas.','',$error) }}</span>
                                    @endif
                                </div>
                                @endif
                                @endforeach
                                @endif

                                <div style="display: none" id="errorMsgRequired" class="alert bg-danger alert-styled-left">
                                    <span class="text-semibold">Required field(s) error or missing</span>
                                </div>

                                <div style="display: none" id="errorMsgWrongFormat" class="alert bg-danger alert-styled-left">
                                    <span class="text-semibold">Wrong format found</span>
                                </div>

                            </div>
                            <div class="ibox-content">

                                <form name="arcanaForm" id="" method="POST" action="{{route('image.store')}}" class="form-horizontal">
                                    <div class="form-group"><label class="col-sm-2 control-label">Name</label>
                                        <div class="col-lg-4">
                                            <input id="name" name="name" type="text" class="form-control" value="{{$name}}">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-lg-2">Width</label>
                                        <div class="col-lg-4">
                                            <input id="width" type="text" name="width" class="form-control" value="{{$width}}" >
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-lg-2">Height</label>
                                        <div class="col-lg-4">
                                            <input id="height" type="text" name="height" class="form-control" value="{{$height}}" >
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-lg-2">Cropable</label>
                                        <div class="col-lg-1">
                                            <select id="crop" name="crop" class="form-control">
                                                <?php $options = ['yes','no']?>
                                                @foreach($options as $v)
                                                <option {{ ($v == $crop ) ? 'selected' : '' }} value="{{$v}}">{{ucfirst($v)}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-lg-2">Aspect Ratio</label>
                                        <div class="col-lg-1">

                                            <select id='aspectratio' name="aspectratio" class="form-control">
                                                <?php $options = ['yes','no']?>
                                                @foreach($options as $v)
                                                <option {{ ($v == $aspectratio ) ? 'selected' : '' }} value="{{$v}}">{{ucfirst($v)}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-2 col-sm-offset-10 col-xs-12">
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <button class="btn btn-primary pull-right" type="submit" style="margin-left: 10px;">Save</button>
                                                    <a href="{{route('image.index')}}" class="btn btn-white pull-right">Cancel </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <input type="hidden" name="id" value="{{$setting->id}}">
                                    {{csrf_field()}}
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            

<?php
    $err = count($errors);
    $errMsg = "";
?>

<script type="text/javascript">
    $(document).ready(function(){
        initPreventClose();
        var status = "{{session('msg')}}";
        var err = "{{$err}}";
        
        if (status === 'Data Saved!') {
            swal({
                title: "SUCCESS",
                text: "All changes has been saved successfuly",
                confirmButtonColor: "#66BB6A",
                type: "success"
                });
        }

    })
</script>