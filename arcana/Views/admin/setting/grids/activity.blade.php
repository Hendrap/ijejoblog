                                    
                                    <td>
                                        <a href="#" class="show-detail" data-toggle="modal" data-target="#myModal"><i class="fa fa-eye"></i></a>
                                    </td>
                                    |arcana--datatable-separator--|
                                    <td>
                                        {{ date(app('ArcanaSetting')->getSetting('date_format'),strtotime($log->created_at)) }}
                                        </br><span class="text-muted">{{ date(app('ArcanaSetting')->getSetting('time_format'),strtotime($log->created_at)) }}</span>
                                    </td>
                                    |arcana--datatable-separator--|
                                    <td>
                                        <strong>
                                            @if(!empty($log->user->metas))
                                                {{ getEntryMetaFromArray($log->user->metas,'first_name').' '.getEntryMetaFromArray($log->user->metas,'last_name') }}
                                            @else
                                                {{$log->email}}
                                            @endif
                                        </strong>
                                        <br><span class="text-muted">{{@$log->user->roles[0]->name}}</span>
                                    </td>
                                    |arcana--datatable-separator--|
                                    <td>
                                        {{$log->description}}
                                    </td>