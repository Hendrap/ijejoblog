<?php

    
    
    $slug = old('slug');
    $single = old('single');
    $plural = old('plural');
    $show_ui = old('show_ui');
    if(!empty($setting)){
        $value = unserialize($setting->setting_value);
        $slug = old('slug',@$value['slug']);
        $single = old('single',@$value['single']);
        $plural = old('plural',@$value['plural']);
        $show_ui = old('show_ui',@$value['show_ui']);
    }
    if(empty($setting)){
        $setting = new StdClass();
        $setting->id = 0;
    }

 ?>

             <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Setting - Taxonomy Type</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a>Settings</a>
                        </li>
                        <li class="active">
                            <strong><?php echo ($setting->id == 0) ? 'Add a new' : 'Edit' ?> Taxonomy Type</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>

            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h3>
                                    @if($setting->id == 0)
                                        Taxonomy Type
                                    @else
                                        {{ $single }}
                                    @endif
                                </h3>
                                @if($setting->id != 0)
                                    <p class="content-group-lg">Created on {{date_format(date_create($setting->created_at),app('ArcanaSetting')->getSetting('date_format'))}} at {{date_format(date_create($setting->created_at),app('ArcanaSetting')->getSetting('time_format'))}} by {{ generateName($setting->user) }}, last modified on {{date_format(date_create($setting->updated_at),app('ArcanaSetting')->getSetting('date_format'))}} at {{date_format(date_create($setting->updated_at),app('ArcanaSetting')->getSetting('time_format'))}}</p>                    
                                @endif

                                @if(count($errors) > 0)
                                @foreach($errors->all() as $key => $error)
                                @if($key == 0)
                                <div class="alert bg-danger alert-styled-left">
                                    <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
                                    @if(Illuminate\Support\Str::contains($error,'required'))
                                        <span class="text-semibold">Required field(s) error or missing</span>
                                    @else
                                        <span class="text-semibold">{{ str_replace('metas.','',$error) }}</span>
                                    @endif
                                </div>
                                @endif
                                @endforeach
                                @endif

                                <div style="display: none" id="errorMsgRequired" class="alert bg-danger alert-styled-left">
                                    <span class="text-semibold">Required field(s) error or missing</span>
                                </div>

                                <div style="display: none" id="errorMsgWrongFormat" class="alert bg-danger alert-styled-left">
                                    <span class="text-semibold">Wrong format found</span>
                                </div>

                            </div>
                            <div class="ibox-content">

                                <form name="arcanaForm" id="" method="POST" action="{{route('taxonomy_type.store')}}" class="form-horizontal">
                                    <div class="form-group"><label class="col-sm-2 control-label">Slug</label>
                                        <div class="col-lg-4">
                                            <input id="slug" name="slug" type="text" class="form-control" value="{{$slug}}">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-lg-2">Label Single</label>
                                        <div class="col-lg-4">
                                            <input id="single" type="text" name="single" class="form-control" value="{{$single}}" >
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-lg-2">Label Plural</label>
                                        <div class="col-lg-4">
                                            <input id="plural" type="text" name="plural" class="form-control" value="{{$plural}}" >
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-lg-2">Show UI</label>
                                        <div class="col-lg-1">

                                            <select id="show_ui" name="show_ui" class="form-control">
                                                <?php $options = ['yes','no']?>
                                                @foreach($options as $v)
                                                <option {{ ($v == $show_ui ) ? 'selected' : '' }} value="{{$v}}">{{ucfirst($v)}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-2 col-sm-offset-10 col-xs-12">
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <button class="btn btn-primary pull-right" type="submit" style="margin-left: 10px;">Save</button>
                                                    <a href="{{route('taxonomy_type.index')}}" class="btn btn-white pull-right">Cancel </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <input type="hidden" name="id" value="{{$setting->id}}">
                                    {{csrf_field()}}
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            