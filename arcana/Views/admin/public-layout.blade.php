<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>{{ $title or 'Arcana CMS' }}</title>
    <link rel="shortcut icon" type="image/x-icon" href="{{asset ('cms/img/arcana/favicon.ico') }}" />

    <link href="{{ asset('cms/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('cms/font-awesome/css/font-awesome.css') }}" rel="stylesheet">

    <link href="{{ asset('cms/css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('cms/css/style.css') }}" rel="stylesheet">

</head>

<body class="gray-bg">

    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            <?php if(!empty($content)) echo $content; ?>
            <p class="m-t"> <small>Copyright @ Arcana Studio</small> </p>
        </div>
    </div>

    <!-- Mainly scripts -->
    <script src="{{ asset('cms/js/jquery-2.1.1.js') }} "></script>
    <script src="{{ asset('cms/js/bootstrap.min.js') }} "></script>

</body>

</html>
