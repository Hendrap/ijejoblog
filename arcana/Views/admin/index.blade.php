<div class="content">
    <div class="dashboard-wrapper">
        <div class="row">


           

                <div class="panel panel-flat align-height">

                    <div class="panel-heading">
                        <div class="dashboard-panel-icon">
                            
                            <i class="icon-user"></i>
                            
                        </div>
                    </div>
                    <div class="panel-body">

                        <div class="common-info-top">

                            <span>{{ number_format($user) }}</span>
                            <h6>User Accounts</h6>

                        </div>

                       

                        <div class="common-info-bottom">

                            <ul class="common-info-ul">
                                @foreach($userByRole as $userRole)
                                <li class="clearfix">
                                    <span class="text-muted pull-right">{{ number_format($userRole->magnitude) }}</span>
                                    <h6>{{ $userRole->legendLabel }}</h6>

                                </li>
                                @endforeach
                            </ul>

                        </div>

                    </div>

                </div>

            

           

                <div class="panel panel-flat align-height">

                    <div class="panel-heading">
                        <div class="dashboard-panel-icon">
                            
                            <i class="icon-pencil6"></i>
                            
                        </div>
                    </div>
                    <div class="panel-body">

                        <div class="common-info-top">

                            <span>{{ number_format($totalContent) }}</span>
                            <h6>Contents</h6>

                        </div>

                       

                        <div class="common-info-bottom">

                            <ul class="common-info-ul">
                                @foreach($entries as $key =>  $entry)
                                <li class="clearfix">
                                    <span class="text-muted pull-right">{{ number_format($entry) }}</span>
                                    <h6>{{$key}}</h6>
                                </li>
                               @endforeach
                                
                            </ul>

                        </div>

                    </div>

                </div>

            

          

                <div class="panel panel-flat align-height">

                    <div class="panel-heading">
                        <div class="dashboard-panel-icon">
                            
                            <i class="icon-image2"></i>
                            
                        </div>
                    </div>
                    <div class="panel-body">

                        <div class="common-info-top">

                            <span>{{ number_format($totalMedia) }}</span>
                            <h6>Media</h6>

                        </div>

                       

                        <div class="common-info-bottom">

                            <ul class="common-info-ul">
                                @foreach($medias as $key => $value)
                                <li class="clearfix">
                                    <span class="text-muted pull-right">{{ number_format($value) }}</span>
                                    <h6>{{$key}}</h6>
                                </li>
                                @endforeach
                                
                            </ul>

                        </div>

                    </div>

                </div>

           

            
            
                @if($isGAEnabled == true)
                <div class="panel panel-flat align-height">

                    <div class="panel-heading">
                        <h5 class="panel-title text-center">Site Visit</h5>
                    </div>
                    <div class="panel-body">

                        <div class="site-visit-top">

                            <span>{{number_format($siteVisitToday)}}</span>
                            <h6>Today</h6>

                        </div>

                        <div id="site-visit" class="dashboard-chart"></div>

                        <div class="site-visit-bottom">

                            <div class="row">
                                <div class="col-sm-4">
                                    <span>{{ number_format($siteVisitAverage) }}</span>
                                    <h6>Average</h6>
                                </div>
                                <div class="col-sm-4">
                                    <span>{{ number_format($siteVisitThisWeek) }}</span>
                                    <h6>This Week</h6>
                                </div>
                                <div class="col-sm-4">
                                    <span>{{ number_format($siteVisitThisMonth) }}</span>
                                    <h6>This Month</h6>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
                <div class="panel panel-flat align-height">

                    <div class="panel-heading">
                        <h5 class="panel-title text-center">Page Views</h5>
                    </div>
                    <div class="panel-body">


                        <div class="page-view-top">

                            <span>{{ number_format($pageViewsToday) }}</span>
                            <h6>Today</h6>

                        </div>

                        <div id="page-views" class="dashboard-chart"></div>

                        <div class="page-view-bottom">

                            <div class="row">
                                <div class="col-sm-4">
                                    <span>{{ number_format($pageViewsAverage) }}</span>
                                    <h6>Average</h6>
                                </div>
                                <div class="col-sm-4">
                                    <span>{{ number_format($pageViewsThisWeek) }}</span>
                                    <h6>This Week</h6>
                                </div>
                                <div class="col-sm-4">
                                    <span>{{ number_format($pageViewsThisMonth) }}</span>
                                    <h6>This Month</h6>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
                <div class="panel panel-flat align-height">

                    <div class="panel-heading">
                        <h5 class="panel-title text-center">Top Browser</h5>
                    </div>
                    <div class="panel-body">

                        <div class="chart-container" style="overflow-x:auto">

                            <div id="browser-status" style="margin-bottom:20px;">


                                <div class="ct-chart ct-perfect-fourth"></div>


                            </div>
                            
                            
                            <ul class="common-info-ul color">
                                <?php 
                                    
                                    

                                 ?>
                                @foreach($originalTopBrowser as $key => $value)
                                <li class="clearfix" style="color:#{{$browserColors[$key]}}!important;">
                                    <span class="text-muted pull-right" style="color:#{{$browserColors[$key]}}!important;">{{ number_format($value['sessions']) }}</span>
                                    <h6 style="color:#{{$browserColors[$key]}}!important;"><span style="background-color:#{{$browserColors[$key]}}!important;"></span> {{$value['browser']}}</h6>
                                </li>
                                @endforeach
                                

                            </ul>
                            
                            

                        </div>

                    </div>
                </div>
                <div class="panel panel-flat align-height">

                    <div class="panel-heading">
                        <h5 class="panel-title text-center">Top Visit</h5>
                    </div>
                    <div class="panel-body">


                       <ul class="top-visit-ul">
                            @foreach($topVisit as $key => $value)
                            <li class="clearfix">
                                <span class="text-muted pull-right">{{ number_format($value['pageViews']) }}</span>
                                <h6><a href="#">{{ $value['url'] }}</a></h6>
                            </li>
                            @endforeach
                            
                        </ul>
                    </div>
                </div>
                @endif

           
                <div class="grid-sizer"></div>
                <div class="gutter-sizer" style="width: 4%;"></div>
       

         </div>
    </div>
    @if(!$isGAEnabled)
        <div class="no-ga-container" style="background:url('{{ asset('backend/assets/images/dashimageoverlay.png')}}')">
            <div class="no-ga-wrapper" style="background:url('{{ asset('backend/assets/images/dashimage.png')}}')">
                <div class="no-ga-wrapper-content">
                    <i class=" icon-statistics"></i>
                    <span>Get better insight from your audiences</span>
                    <a href="{{ route('arcana_admin_get_analytics') }}">Install google Analytics</a>
                </div>
            </div>
        </div>
    @endif
    

</div>
<!-- /content area -->

<script src="{{ asset('backend/assets/js/plugins/masonry/masonry.pkgd.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/assets/js/core/dashboard-layout.js') }}"></script>
@if($isGAEnabled)
<script>
     dashboardChart('#site-visit', 65, '#4CAF50',{!! json_encode($siteVisit) !!});
     dashboardChart('#page-views', 65, '#4CAF50',{!! json_encode($pageView) !!});
     chartDonut("#browser-status",{!! json_encode($topBrowser) !!});
</script>
@endif

    <script>
        $(document).ready(function() {
            setTimeout(function() {
                toastr.options = {
                    closeButton: true,
                    progressBar: true,
                    showMethod: 'slideDown',
                    timeOut: 4000
                };
                toastr.success("{{$toast['detail']}}", "{{$toast['title']}}");

            }, 1300);
        });
    </script>