{{ Auth::user() }}

    <script>
        $(document).ready(function() {
            setTimeout(function() {
                toastr.options = {
                    closeButton: true,
                    progressBar: true,
                    showMethod: 'slideDown',
                    timeOut: 4000
                };
                toastr.success("{{$toast['detail']}}", "{{$toast['title']}}");

            }, 1300);
        });
    </script>