var userTables = new Vue({
	el: '#user',
	data: {
		tableData: [],
		paramFilter: [{'search':''}],
    keywordTitle : '',
    keywordStatus : '',
    keywordRole : '',
	},
	methods: {

      getData : function(title = null, status = null, role = null){
        this.$http.post(urlGetData, {
                                      search:{'value':title},
                                      order:'',
                                      status:status,
                                      role:role,
                                    }).then( (response) => {
            this.tableData = response.body;
            console.log(response.body);
        });
      },

      filterData : function(e){
        if (e.keyCode === 13) {
          this.keywordTitle = e.target.value;
          this.getData(this.keywordTitle, this.keywordStatus, this.keywordRole);
        }
      },

      filterStatus : function(e){
        this.keywordStatus = e.target.value;
        if(this.keywordStatus == 'all')
          this.keywordStatus = '';
        this.getData(this.keywordTitle, this.keywordStatus, this.keywordRole);
      },

      filterRole : function(e){
        console.log(e.target.value);
        this.keywordRole = e.target.value;
        if(this.keywordRole == 'all')
          this.keywordRole = '';
        this.getData(this.keywordTitle, this.keywordStatus, this.keywordRole);
      },


	}

});
