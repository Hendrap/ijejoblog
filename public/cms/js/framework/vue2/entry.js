
// vue component ditambahkan ke dalam "shopping", masuk element #shopping-cart
Vue.component("entry-item",{
  template : "",
  props: ["item"],
  methods: {
    
  }
});

var entryTables = new Vue({
	el: '#entry',
	data: {
		tableData: [],
		paramFilter: [{'search':''}],
    keywordTitle : '',
    keywordStatus : '',
    keywordCategory : '',
	},
	methods: {

      getData : function(title = null, status = null, category = null){
        this.$http.post(urlGetData, {
                                      search:{'value':title},
                                      order:'',
                                      status:status,
                                      category:category,
                                    }).then( (response) => {
            this.tableData = response.body;
            console.log(response.body);
        });
      },

      filterData : function(e){
        if (e.keyCode === 13) {
          this.keywordTitle = e.target.value;
          this.getData(this.keywordTitle, this.keywordStatus, this.keywordCategory);
        }
      },

      filterStatus : function(e){
        this.keywordStatus = e.target.value;
        if(this.keywordStatus == 'all')
          this.keywordStatus = '';
        this.getData(this.keywordTitle, this.keywordStatus, this.keywordCategory);
      },

      filterCategory : function(e){
        this.keywordCategory = e.target.value;
        if(this.keywordCategory == 'all')
          this.keywordCategory = '';
        this.getData(this.keywordTitle, this.keywordStatus, this.keywordCategory);
      },


	}

});
