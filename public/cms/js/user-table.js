                        var entry_table = {};
                        $(document).ready(function() {
                              entry_table = $('.entry-table').on( 'processing.dt', function ( e, settings, processing ) {
                                    var block = $('.entry-table');
                                     if(processing){
                                         $('.dataTable, .dataTable tbody').css('position','static');
                                            if(typeof entry_table.ajax != 'undefined')
                                            {
                                                $(e.target).find('tbody').html('');
                                            }

                                            $(block).block({
                                                message: '<span class="text-semibold"><i class="fa fa-spinner spinner position-left"></i>&nbsp;Loading Data</span>',
                                                overlayCSS: {
                                                    backgroundColor: '#fff',
                                                    opacity: 0.8,
                                                    cursor: 'wait'
                                                },
                                                css: {
                                                    border: 0,
                                                    padding: '10px 15px',
                                                    color: '#333',
                                                    width: 'auto',
                                                   
                                                }
                                            });
                                     }else{
                                        $.unblockUI();
                                        $('.dataTable, .dataTable tbody').css('position','relative');
                                        checktablestate(entry_table);
                                        $(".blockUI").remove();
                                     }
                                     $( document ).trigger( "arcana--datatable-processing",[processing,$('.entry-table'),block,e]);
                            }).DataTable({
                                "order": [],
                                "searching" : true,
                                "lengthChange": false,
                                "dom": 'rt<"datatable-footer"ilp><"clear">',
                                "processing": true,
                                "oLanguage": {
                                    "sProcessing":' ' 
                                },
                                "serverSide": true,
                                "pageLength": 20,
                                "ajax": {
                                    "url": userGetData,
                                    "type": "POST"
                                },
                                "columnDefs": [{
                                    "targets": 'no-sort',
                                    "orderable": false,
                                }],
                                 "columns": [
                                    { "data": "username", "render": function(data, type, full, meta){
                                        var html = full.html.split('|arcana--datatable-separator--|');
                                        return html[0];
                                    },
                                    sClass:"text-bold"
                                    },


                                    { "data": "role", "render": function(data, type, full, meta){
                                        var html = full.html.split('|arcana--datatable-separator--|');
                                        return html[1];
                                    },
                                    sClass:""
                                    },

                                    { "data": "created_at", "render": function(data, type, full, meta){
                                        var html = full.html.split('|arcana--datatable-separator--|');
                                        return html[2];
                                    },
                                    sClass:"td-date"
                                    },

                                    { "data": "last_login", "render": function(data, type, full, meta){
                                        var html = full.html.split('|arcana--datatable-separator--|');
                                        return html[3];
                                    },
                                    sClass:"td-date"
                                    },

                                    { "data": "status", "render": function(data, type, full, meta){
                                        var html = full.html.split('|arcana--datatable-separator--|');
                                        return html[4];
                                    },
                                    sClass:""
                                    },

                                    { "data": "action", "render": function(data, type, full, meta){
                                        var html = full.html.split('|arcana--datatable-separator--|');
                                        return html[5];
                                    },
                                    sClass:"text-right table-actions"
                                    }

                                ],
                            });
                           
                            var filterby;
                            
                            $('.filter-table-column').change(function(){
                                var col = "";
                                var val = "";
                                thisfilter = $(this);
                                thisfilter.find('option:selected').each(function(){
                                    val = $(this).val();
                                    col = $(this).attr('data-col');
                                });
                                
                                entry_table.columns(col).search(val).draw();
                                
                                
                            });
                            
                            $('#search_the_table').keyup(function(e){
                                if(e.which == 13)
                                {
                                 entry_table.search($(this).val()).draw();
                                }
                            });
                            
                            $('select').select2({
                                minimumResultsForSearch: Infinity,
                                width: 'auto'
                            });
                             $('.table-select2').each(function() {

                                select_placeholder = $(this).attr('placeholder');
                                $(this).select2({
                                    minimumResultsForSearch: Infinity,
                                    placeholder: select_placeholder,
                                    width: '100%',

                                });

                            });
                        });