CKEDITOR.plugins.add('addslideshow', {
   
    init: function (editor) {
        editor.addCommand('addslideshow', {
            exec: function (editor) {

                $('#modal_add_slide').modal();

                $('.add-slider-to-editor').click(function (e) {
                    if (!$('input[name="radio_slide"]:checked').val()) {
                        new PNotify({

                            text: 'No slideshow selected.',
                            icon: 'icon-blocked',
                            type: 'error'
                        });
                    } else {
                       
                        var addedslide =  insertslide();
                        editor.insertHtml(addedslide);
                        $('#modal_add_slide').modal('hide')
                    }

                });

                

            }
        });
        editor.ui.addButton('addslideshow', {
            label: 'Add Slideshow',
            command: 'addslideshow',
            toolbar: 'insert',
            icons: '../icons/addslideshow.gif',
        });
    }
});

function insertslide(arraydata) {

                    var slidelist = $('input[name="radio_slide"]:checked').data('slides');
                    var imagetoparse = '';
                    var imagetemp;
                    $.each(slidelist, function (index, value) {
                        imagetemp = '<div class="swiper-slide"><img src="assets/images/' + value + '" alt="' + value + '"></div>';
                        imagetoparse = imagetoparse + imagetemp;
                    });
                    var slideindetifier = $('input[name="radio_slide"]:checked').attr('id');
                    var slidelayout = '<div class="summernote-slider' + slideindetifier + '"><div class="swiper-wrapper">' + imagetoparse + '</div></div>';

                    return slidelayout;


};
