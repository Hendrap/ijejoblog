<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/laravel', function () {
    return view('welcome');
});

Route::group(['middleware' => ['web','multilang'],'namespace'=>'App\Http\Controllers'], function () {
	//multi-language config
	include base_path('app/Http/Routes/multilang.php');
	//default routing
	include base_path('app/Http/Routes/default.php');
	
	//index
	// App Routes Goes Here
	Route::get('/','FrontController@index');
	Route::get('/topic/{slug}','ArticleController@entriesDetail');
});

// Sudah di load lewat providers
// include base_path('/arcana/Routes/admin.php');

